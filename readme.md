# Student Digital Idea System

Pastikan Git, Gitflow, Node Js + NPM, Composer sudah terinstall

## Kebutuhan
1. PHP 7.2 ++
2. Web Server + DB Server
3. Node js + NPM
4. Composer ( PHP Packager)
5. git, git flow
6. Cara menggunakan Git Flow

## Installasi
1. Clone Repo ini , Klik Tombol Clone diatas
2. masuk ke Direktori folder
3. Jalankan `composer update`
4. Lalu `npm install`
5. compile asset frontend `npm run dev`
6. buat nama database
7. Buat file dengan nama `.env` atau ganti nama file `.env.example` jadi `.env`
8. jalankan `php artisan key:generate`
