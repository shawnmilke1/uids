import EventStore from './modules/EventStore';
import TicketStore from './modules/TicketStore';
import NavigationStore from './modules/NavigationStore';
import AudienceStore from './modules/AudienceStore';
import MemberStore from './modules/MemberStore';
import {action} from 'mobx';

export default class RootStore {
    constructor(){
        this.eventStore = new EventStore(this);
        this.ticketStore = new TicketStore(this);
        this.navigationStore = new NavigationStore(this);
        this.audienceStore = new AudienceStore(this);
        this.memberStore = new MemberStore(this);
    }
    
    @action is_active = (e) => {
        let lastPath = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
        console.log(window.location.href);
    }

    @action logOut = (e) => {
        e.preventDefault();
        axios.get('/api/logout').then(res => {
            console.log(res);
        }).catch(err => console.log(err));
    }
    
}