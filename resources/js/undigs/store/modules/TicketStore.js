import { observable,action} from "mobx";
import uuidv4 from 'uuid/v4';

export default class TicketStore {

    @observable name = '';
    @observable qty = '';
    @observable price = '';
    @observable uuid = '';
    @observable event_id = '';
    @observable description = '';
    @observable refundable = false;
    @observable cancel_policy = '';
    @observable tickets = [];
    @observable ticket = {};
    @observable errors = {};
    @observable ticket_limit = 5;

    //for form handling
    @observable forms = [];
    @observable isEditing = false;

    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    @action setTicket = (id) => {
        axios.get('/api/tickets/'+id).then(res => {
            this.ticket = res.data;
            this.event_id = res.data.event.id;
        })
    }

    @action updateEventId = (id) => {
        this.event_id = id;
    }

    @action fetchTickets = (id) => {
        axios.get(`/api/events/${id}/tickets`).then(res => {
            this.tickets = res.data;
        })
    }

    @action fetchTicket = (ticket) => {
        this.handleEdit(ticket)
    }
    
    
    @action createFreeTicket = () => {
        const data = {
            name: '',
            qty: '',
            price: 'free',
        }
        this.price = 'free';
        this.name = '';
        this.qty = '';
        this.forms.push(data);
        this.isEditing = true;
    }

    @action createTicket = () => {
        const data  = {
            name: '',
            qty:'',
            price:'',
        }
        this.forms.push(data);
        this.name = '';
        this.price = 0;
        this.qty = '';
        this.isEditing = true;
    }

    @action cancelCreating = () => {
        this.isEditing = false;
        this.price = '';
        this.forms = [];
    }

    @action handleSubmit = (id) => (e) => {
        e.preventDefault();
        const data = {
            name: this.name,
            qty: this.qty,
            price: this.price,
            uuid: uuidv4(),
            event_id: id,
            description:this.description,
            refundable:this.refundable,
            cancel_policy:this.cancel_policy,
            ticket_sell:0,
            ticket_limit:this.ticket_limit
        }
        axios.post(`/api/events/${id}/tickets`, data).then(res => {
            this.tickets.push(res.data);
            this.isEditing = false;
            this.clearData();
        }).catch(err => {
            console.log(err.response);
            this.handleError(err.response.data.errors);
        });
    }
    @action clearData() {
        this.forms = [];
        this.name = '';
        this.qty = '';
        this.refundable =false;
        this.description = '';
        this.cancel_policy = '';
        this.errors = {};
    }

    @action handleUpdate = (id) => (e) => {
        e.preventDefault();
        const data = {
            name:this.name,
            qty:this.qty,
            price:this.price,
            uuid:uuidv4(),
            ticket_limit:this.ticket_limit,
            cancel_policy:this.cancel_policy,
            description:this.description,
            refundable:this.refundable,
            
        }
        axios.post(`/api/tickets/${id}`,data).then(res => {
            if(res.status == 201){
                this.tickets = this.tickets.map(item => item.id === res.data.id ? res.data : item);
                UIkit.modal(`#ticket-${id}`).hide()
                UIkit.notification('Updated')
                this.errors = {}
            }
        }).catch(err => {
            this.errors = err.response.data.errors;
        })
    }

    @action handleDelete = (id) => {
        var r = confirm('Are You Sure ?');
        if(r === true){
            axios.delete(`/api/tickets/${id}`).then(res => {
                _.remove(this.tickets,(data) => {
                    return data.id == id
                })
                UIkit.notification('Ticket Deleted!');
            })
        }
    }

    @action handleChange = (e) => {
        this[e.target.name] = e.target.value;
    }

    @action handleCheck = (e) => {
        this.refundable = !this.refundable;
    }

    @action handleEdit = (ticket) => {
        this.name = ticket.name;
        this.qty = ticket.qty;
        this.price = ticket.price;
        this.refundable = ticket.refundable;
        this.description = ticket.description == null ? '' : ticket.description
        this.ticket_limit = ticket.ticket_limit;
        this.cancel_policy = ticket.cancel_policy == null ? '' :ticket.cancel_policy
    }

    @action handleError = (errors) => {
        this.errors = errors;
    }

}