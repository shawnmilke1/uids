import {observable,action} from 'mobx';

export default class MemberStore {

    @observable members = [];
    @observable memberId = '';
    @observable email = [];
    @observable error = {};
    @observable roles = [];
    @observable organizer_role_id = '';
    @observable isLoading = false;
    @observable totalMembers = 0;

    constructor(rootStore){
        this.rootStore = rootStore;
    }

    @action fetchMembers(id){
        axios.get(`/api/organizers/${id}/members`).then(res => {
            this.members = res.data;
        });

        axios.get('/api/organizer-roles').then(res => {
            this.roles = res.data;
        })
    }

    @action getTotalMembers(id){
        axios.get(`/api/organizers/${id}/members`).then(res => this.totalMembers = res.data.length);
    }

    @action handleChange = (e) => {
        this.error = {};
        this[e.target.name] = e.target.value;
    }

    @action handleUpdate = (member) => {
        this.memberId = member.id;
        this.organizer_role_id = member.organizer_role_id;
    }

    @action changeRole = (e) => {
        e.preventDefault();
        axios.post(`/api/members/${this.memberId}`,{
            organizer_role_id:this.organizer_role_id
        }).then(res => {
            this.members = this.members.map(member => member.id === res.data.id ? res.data : member);
            UIkit.notification({
                message:'Role Updated'
            })
            UIkit.modal('#modal-example').hide()
        })
    }

    @action addMember = (id) => (e) => {
        this.isLoading = true;
        e.preventDefault();
        const data = {
            email:this.email
        }
        axios.post(`/api/organizers/${id}/members`,data).then(res => {
            UIkit.notification({
                message: 'Menambahkan anggota sukses',
                status: 'primary',
                pos: 'top-right',
                timeout: 5000
            });
            this.error = {};
            this.members.push(res.data);
        }).catch(err => {
            this.error = err.response.data.errors
            this.isLoading = false;
        })
    }

    @action handleDelete = (id) => {
        var r = confirm('Yakin mau hapus anggota ini?');
        if (r === true) {
            axios.delete(`/api/members/${id}`).then(res => {
                let members = this.members.filter(member => member.id !== id);
                this.members.replace(members);
                UIkit.notification('Delete Member!');
            })
        }
    }

}