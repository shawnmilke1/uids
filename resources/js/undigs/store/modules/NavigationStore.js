import { observable, action} from 'mobx';
import {BrowserRouter as Router} from 'react-router-dom'

export default class NavigationStore {

    @observable navigation = new Router().history
    constructor(rootStore) {
        this.rootStore = rootStore
    }
    
}