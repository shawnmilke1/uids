import {observable,action} from 'mobx';

export default class EventStore {
    
    @observable events = [];
    @observable status = 'pending';
    @observable ticketSell = 0;
    @observable ticketCreated = 0;
    @observable incomes = 0;
    @observable visitors = 0;
    @observable sellRate = 0;

    constructor(rootStore){
        this.rootStore = rootStore;
    }
    
    @action fetchCollection(){
        axios.get(`/api/o/${window.App.org.slug}/collection`).then(res => {
            this.ticketSell = res.data.sell;
            this.ticketCreated = res.data.tickets;
            this.incomes = res.data.incomes;
            this.visitors = res.data.visitors;
            this.sellRate = res.data.sellRate;
        })
    }
    
    @action
    fetchEvents(){
        axios.get(`/api/o/${window.App.org.slug}/events`).then(res => {
            this.events = res.data
            this.status = 'done'
        }).catch(err => console.log('Whoops Something When Wrong, Please Contact Developer'))
    }

    @action changeStatus = (e) => {
        axios.get(`/api/o/${window.App.org.slug}/events?status=${e.target.value}`).then(res => {
            this.events = res.data;
        })
    }
    
    @action updateEvent = (event) => (e) => {
        e.preventDefault();
        UIkit.modal.confirm('Anda Serius mau mempublish Acara ini?').then(() => {
            axios.put(`/api/events/${event.id}`).then(() => {
                window.location.href = `/o/${window.App.org.slug}/event/${event.id}/dashboard`
            })
        }),function(){
            console.log('reject')
        }
    }

}