import {observable,action} from 'mobx';

export default class AudienceStore {

    @observable audiences = [];

    @observable checkLength = 0 ;

    constructor(rootStore){
        this.rootStore = rootStore;
    }

    @action
    fetchAuds(id) {
        axios.get(`/api/tickets/${id}/audiences`).then(res => {
            this.audiences = res.data;
        })
    }

    @action setCheckIn = (res,isCheck) => {
        this.checkLength = isCheck ? this.checkLength + 1 : this.checkLength - 1;
        this.audiences = this.audiences.map(item => item.id === res.data.id ? res.data : item)
    }

    @action
    allAuds(id) {
        axios.get(`/api/${id}/allauds`).then(res => {
            this.audiences = res.data;
            this.checkLength = _.filter(res.data,['check_in',1]).length;
            
        }).then(() => {
            $(document).ready(()=> {
                let rows_selected = [];
                let table = $('#auds-table').DataTable({
                    "data": this.audiences,
                    responsive:true,
                    columns: [
                        {
                            data:'id',
                        },
                        {
                            data: 'full_name',
                        },
                        {
                            data: 'email',
                        },
                        {
                            data: 'phone'
                        },
                        {
                            "data": "ticket.name"
                        },
                        {
                            data:"status",
                            render: function(data,type,row){
                                var status = [
                                    {
                                        name:'paid'
                                    },
                                    {
                                        name:'unpaid'
                                    }
                                ]
                                var option = status.map((aud) => (
                                    `<option value=${aud.name} ${row.status == aud.name ? 'selected' : ''}>${aud.name}</option>`
                                ))
                                if(type === 'display'){
                                    return `
                                    <select class="uk-select select-status" name="status" id=${row.uuid}>
                                     ${option}
                                    </select>
                                    `
                                }
                                return data
                            }
                        },
                        {
                            "data": "check_in",
                            render: function(data,type,row){
                                if(type === 'display'){
                                    return `<input id=${row.id} class='uk-checkbox editor-active' type='checkbox'>`
                                }
                                return data
                            }
                        }
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                        {
                            extend:'excel',
                            title:'Data Peserta',
                            className:'btn btn-primary'
                        },
                        {
                            extend:'pdf',
                            title:'Data Peserta',
                            className:'btn btn-outline-info'
                        },
                        {
                            extend:'print',
                            title:'Data Peserta',
                            className:'btn btn-info'
                        }
                    ],
                    rowCallback: function(row, data){
                        $('input.editor-active',row).prop('checked',data.check_in == 1);
                        let rowId = data.id;
                        if($.inArray(rowId,rows_selected) !== -1){
                            $(row).find('input[type="checkbox"]').prop('checked', true);
                        
                        }
                    },
                });
                
                table.buttons().container().appendTo('.aud-buttons')
                $('#auds-table_filter').appendTo('.aud-search')
                $('#auds-table_filter input').addClass('form-control')

                const checkIn = this.setCheckIn
                $('#auds-table').on('change','input.editor-active',function(e){
                    
                    axios.post(`/api/audiences/${$(this).attr('id')}/check`).then(res => {
                        let isCheck = $(this).prop('checked');
                        checkIn(res,isCheck);
                        let $row = $(this).closest('tr')
                        let data = table.row($row).data();
                        let rowId = data.id;
                        let index = $.inArray(rowId, rows_selected);
                        if (this.checked && index === -1) {
                            rows_selected.push(rowId);
                           

                        } else if (!this.checked && index !== -1) {
                            rows_selected.splice(index, 1);
                           
                        }
                        e.stopPropagation();
                        
                    })
                    
                });
                $('#auds-table').on('change','.uk-select.select-status',function(e){
                    const data = {
                        status:$(this).val()
                    };
                    axios.post(`/api/audiences/${$(this).attr('id')}/setAudStatus`,data)
                });
            })
        })
    }
}