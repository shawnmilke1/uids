import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Dashboard from './views/Dashboard';
const NotFound = () => (
    <div>
        Not Found
    </div>
)
//event
import IndexEvent from './views/events/IndexEvent';
import ShowEvent from './views/events/ShowEvent';
import Stats from './views/events/Stats';
//ticket
import Ticket from './views/tickets/Ticket';
//peserta
import IndexAud from './views/audiences/IndexAud';
//managements
import Member from './views/manages/Member';
import General from './views/manages/General';

let username = window.App.org.slug;

const AppRouter = () => (
    <Switch>
        <Route path={`/o/${username}`} exact component={Dashboard} key="dashboard" />
        <Route path={`/o/${username}/events`} component={IndexEvent} key="event.index" />
        <Route path={`/o/${username}/event/:id/tickets`} component={Ticket} key="ticket.create" />
        <Route path={`/o/${username}/event/:id/dashboard`} component={ShowEvent} key="event.show" />
        <Route path={`/o/${username}/event/:id/audiences`} component={IndexAud} key="audiences" />
        <Route path={`/o/${username}/event/:id/stats`} component={Stats} key='stats' />
        <Route path={`/o/${username}/settings/members`} component={Member} key="members" />
        <Route path={`/o/${username}/settings/generals`} component={General} key="general" />
        <Route path={`/o/${username}/settings`} render={() => <Redirect to={`/o/${username}/settings/generals`}/> } />
        <Route path="*" component={NotFound}/>
    </Switch>
);
export default AppRouter;