import React from 'react'
import { observer } from 'mobx-react';
import TicketModal from './TicketModal';

@observer export default class TicketList extends React.Component {
    constructor(props){
        super(props);
        this.props.rootStore.ticketStore.handleDelete = this.props.rootStore.ticketStore.handleDelete.bind(this);
    }
    render(){
        const {tickets} = this.props;
        const {handleDelete,handleEdit} = this.props.rootStore.ticketStore;
        return (
            tickets.length > 0 ?
                tickets.map(ticket => (
                    <div key={ticket.id} className="uk-margin-medium-bottom">
                        <div className="uk-card uk-card-default uk-border-rounded">
                            <div className="uk-card-header">
                                <div data-uk-grid className="uk-child-width-1-2">
                                    <div>
                                        <h4 className="uk-text-bold">
                                            {ticket.name}
                                        </h4>
                                    </div>
                                    {
                                        this.props.event.status == "DRAFT" && 
                                        <div className="uk-text-right">
                                            <ul className="uk-iconnav uk-flex-right">
                                                <li>
                                                    <button type="button" onClick={() => handleEdit(ticket)} id={'button' + ticket.id} data-uk-toggle={`target: #ticket-${ticket.id}`} className="uk-icon-link" data-uk-icon="icon: file-edit">
                                                    </button>
                                                </li>
                                                <li>
                                                    <button data-uk-tooltip="Hapus Tiket Ini?" className="uk-icon-link" onClick={() => handleDelete(ticket.id)} data-uk-icon="icon: trash">
                                                    </button>
                                                </li>
                                            </ul>
                                        </div>
                                    }
                                </div>
                            </div>
                            <div className="uk-card-body uk-flex-middle">
                                <div className="uk-child-width-1-2" data-uk-grid>
                                    <div>
                                        <div className="uk-text-large">Jumlah Tiket : {ticket.qty - ticket.ticket_sell} </div>
                                    </div>
                                    <div className="uk-text-right">
                                        <div className="text-read uk-text-large uk-text-bold">
                                            {
                                                ticket.price == "free" ? "Free" : `Rp. ${ticket.price}`
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <TicketModal ticket={ticket} rootStore={this.props.rootStore} />
                    </div>
                ))
                :

                <div>
                    <div className="uk-text-center">
						<img src="/images/icons/tickets.svg" width={100}/>
						<p>
							Tidak Ada Tiket
						</p>
                    </div>
                </div>
        )
    }
}