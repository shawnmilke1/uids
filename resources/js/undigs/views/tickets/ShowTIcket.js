import React, { Component } from 'react'
import { observer,inject } from 'mobx-react';

@inject('rootStore')
@observer
export default class ShowTicket extends Component {
    constructor(props){
        super(props);
    }
    componentDidMount = () => {
        this.props.rootStore.ticketStore.setTicket(this.props.match.params.id)
        this.props.rootStore.audienceStore.fetchAuds(this.props.match.params.id);
    }
    
    render(){
        const { ticket} = this.props.rootStore.ticketStore;
        const {audiences} = this.props.rootStore.audienceStore;
        return(
            <section className="pt-8 pb-5">
                <div className="container-fluid">
                    <div>
                        <h1>
                            Tiket {ticket.name}
                        </h1>
                        <div data-uk-grid className="uk-grid-small">
                            <div className="uk-width-2-3@m">
                                <div className="uk-card uk-card-small uk-border-rounded uk-card-default box-shadow-default">
                                    <div className="uk-card-header">
                                        <div className="uk-text-bold">Analisa</div>
                                    </div>
                                </div>
                            </div>
                            <div className="uk-width-1-3@m">
                                <div className="uk-card uk-card-small uk-border-rounded uk-card-default box-shadow-default">
                                    <div className="uk-card-header">
                                        <div className="uk-text-bold">Tiket Detail</div>
                                    </div>
                                    <div className="uk-card-body">
                                        <div data-uk-grid className="uk-flex-middle uk-grid-divider">
                                            <div className="uk-width-1-4">
                                                <div>Terjual</div>
                                            </div>
                                            <div className="uk-width-3-4">
                                                <div>{audiences.length} / {audiences.length + ticket.qty}</div>
                                            </div>
                                        </div>
                                        <hr />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}