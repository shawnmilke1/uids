import React, { Component } from 'react'
import {observer} from 'mobx-react'

@observer
export default class Form extends Component {
    constructor(props){
        super(props);
        this.props.rootStore.ticketStore.handleChange = this.props.rootStore.ticketStore.handleChange.bind(this);
        this.props.rootStore.ticketStore.handleCheck = this.props.rootStore.ticketStore.handleCheck.bind(this);
        this.props.rootStore.ticketStore.createFreeTicket = this.props.rootStore.ticketStore.createFreeTicket.bind(this);
        this.props.rootStore.ticketStore.createTicket = this.props.rootStore.ticketStore.createTicket.bind(this);
        this.props.rootStore.ticketStore.handleSubmit = this.props.rootStore.ticketStore.handleSubmit.bind(this);
    }
    render(){
        const limits = [1,2,3,4,5];
        const {handleChange, name,refundable,cancel_policy,ticket_limit, handleCheck, qty, price, createFreeTicket,cancelCreating, forms, isEditing, errors,handleSubmit, createTicket ,description} = this.props.rootStore.ticketStore
        return (
            <div>
                {
                    isEditing == false && 
                    <div className="uk-text-center">
                        <button className="uk-button uk-button-primary uk-margin-small-right" type="button" onClick={createFreeTicket} >Tiket Gratis</button>
                        <button className="uk-button uk-button-default uk-margin-small-right" type="button" onClick={createTicket} >Tiket Berbayar</button>
                    </div>
                }
                {
                    forms.map((form,index) => (
                        <div key={index} className="uk-animation-fade">
                            <div className="uk-card uk-card-body uk-card-default uk-border-rounded">
                                <form onSubmit={handleSubmit(this.props.id)} >
                                    <div data-uk-grid className="uk-grid-small">
                                        <div className="uk-width-1-2@m">
                                            <label htmlFor="name">Nama Tiket</label>
                                            <input placeholder="VIP, Free" onChange={handleChange} value={name} type="text" name="name" className="uk-input"/>
                                            { errors.name && <span className="text-red">{errors.name[0]}</span> }
                                        </div>
                                        <div className="uk-width-1-4@m">
                                            <label htmlFor="price" className="uk-display-block">Harga</label>
                                            <div className="uk-inline">
                                                <span className="uk-form-icon">Rp</span>
                                                <input type="text" name="price" onChange={handleChange} value={price} className="uk-input" disabled={form.price == "free"} />
                                            </div>
                                            {errors.price && <span className="text-red">{errors.price[0]}</span>}
                                        </div>
                                        <div className="uk-width-1-4@m">
                                            <label htmlFor="qty">Jumlah</label>
                                            <input type="text" name="qty" onChange={handleChange} value={qty} placeholder="100" className="uk-input" />
                                            {errors.qty && <span className="text-red">{errors.qty[0]}</span>}
                                        </div>
                                    </div>
                                    <div className="uk-margin">
                                        <ul data-uk-accordion>
                                            <li>
                                                <a className="uk-accordion-title" href="#">Additional Information</a>
                                                <div className="uk-accordion-content">
                                                    <div className="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                                                        <label>
                                                            <input className="uk-checkbox" name="refundable" type="checkbox" onChange={handleCheck} checked={refundable}/> Tiket dapat dikembalikan ?</label>
                                                         
                                                    </div>
                                                    <div className="uk-margin">
                                                        <label htmlFor="ticket_limit" className="uk-form-label">Tiket Limit</label>
                                                        <input type="number" name="ticket_limit" onChange={handleChange} value={ticket_limit} className="uk-input" max={5} min={1}/>
                                                        <span className="uk-text-small">Pembelian tiket dalam sekali registrasi, Maksimal 5, Minimal 1</span>
                                                    </div>
                                                    <div className="uk-margin">
                                                        <label htmlFor="cancel_policy" className="uk-form-label">Kebijakan Pembatalan</label>
                                                        <textarea name="cancel_policy" placeholder="Deskripsikan Bagaimana jika peserta anda membatalkan tiket yang sudah ia pesan" onChange={handleChange} value={cancel_policy} cols="30" rows="5" className="uk-textarea"></textarea>
                                                    </div>
                                                    <div className="uk-margin">
                                                        <label className="uk-form-label" htmlFor="description">Deskripsi Tiket</label>
                                                        <textarea name="description" value={description} onChange={handleChange} id="desc" cols="30" rows="5" className="uk-textarea"></textarea>    
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="uk-margin">
                                        <a className="uk-button uk-button-default uk-margin-small-right" onClick={cancelCreating.bind(this)} >Cancel</a>
                                        <button type="submit" className="uk-button uk-button-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    ))
                }
            </div>
        )
    }
}