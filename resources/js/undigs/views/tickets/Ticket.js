import React, { Component } from 'react'
import Form from './Form';
import {inject, observer} from 'mobx-react'
import TicketList from './TicketList';

@inject('rootStore')
@observer
export default class Ticket extends Component {
    constructor(props){
        super(props);
        this.state = {
            event:{}
        }
    }
    componentDidMount = () => {
        this.props.rootStore.ticketStore.fetchTickets(this.props.match.params.id)
        axios.get(`/api/events/${this.props.match.params.id}`).then(res => this.setState({event:res.data}))
        .catch(err => {
            if (err.response.status == 403) {
                window.location.href = '/myevents';
            }
        })
        this.props.rootStore.ticketStore.updateEventId(this.props.match.params.id)
    }
    componentWillUnmount(){
        this.props.rootStore.ticketStore.isEditing = false;
        this.props.rootStore.ticketStore.forms = [];
    }
    
    render(){
        const {tickets} = this.props.rootStore.ticketStore
        const {event} = this.state
        return (
            <section className="pt-8 pb-5">
                <div className="container-fluid">
                    <div>
                        <h2 className="uk-text-center">Buat Tiket</h2>
                        {
                            tickets.length < 2 & window.App.role != 'member' ?
                                <Form id={this.props.match.params.id} rootStore={this.props.rootStore} />
                                :
                                <div className="uk-text-center uk-alert">
                                    <p className="text-red">
                                        {
                                            window.App.role == 'member' ?
                                                'Anda tidak memiliki izin untuk membuat tiket' :
                                                'Limit Pembuatan Ticket Hanya 2 Saja :)'
                                        }
                                    </p>
                                </div>
                        }
                        <hr />
                        <div className="uk-margin-medium-top">
                            <h2 className="uk-text-center">Tiket untuk {event.title}</h2>
                            <TicketList tickets={tickets} event={event} rootStore={this.props.rootStore} />
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}