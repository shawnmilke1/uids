import React, { Component } from 'react'
import {observer} from 'mobx-react';

@observer export default class TicketModal extends Component {
    constructor(props){
        super(props);
    }
    
    render(){
        const {ticket} = this.props;
        const {name,qty,price,handleChange,handleUpdate,errors,ticket_limit,cancel_policy,refundable,description,handleCheck} = this.props.rootStore.ticketStore;
        return(
            <div id={"ticket-" + ticket.id} data-uk-modal>
                <div className="uk-modal-dialog uk-modal-body">
                    <h2 className="uk-modal-title">Edit Ticket</h2>

                    <form onSubmit={handleUpdate(ticket.id)}>
                        <div className="uk-margin">
                            <label htmlFor="name">Ticket Name</label>
                            <input type="text" name="name" value={name}  onChange={handleChange} className="uk-input"/>
                            {errors.name && <span>{errors.name[0]}</span>}
                        </div>
                        <div className="uk-margin">
                            <label htmlFor="qty">Quantity</label>
                            <input type="text" name="qty"  value={qty} onChange={handleChange} className="uk-input"/>
                            {errors.qty && <span>{errors.qty[0]}</span>}
                        </div>
                        <div className="uk-margin">
                            <ul data-uk-accordion>
                                <li>
                                    <a className="uk-accordion-title" href="#">Additional Information</a>
                                    <div className="uk-accordion-content">
                                        <div className="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                                            <label>
                                                <input className="uk-checkbox" name="refundable" type="checkbox" onChange={handleCheck} checked={refundable} /> Tiket dapat dikembalikan ?</label>

                                        </div>
                                        <div className="uk-margin">
                                            <label htmlFor="ticket_limit" className="uk-form-label">Tiket Limit</label>
                                            <input type="number" name="ticket_limit" onChange={handleChange} value={ticket_limit} className="uk-input" max={5} min={1} />
                                            <span className="uk-text-small">Pembelian tiket dalam sekali registrasi, Maksimal 5, Minimal 1</span>
                                        </div>
                                        <div className="uk-margin">
                                            <label htmlFor="cancel_policy" className="uk-form-label">Kebijakan Pembatalan</label>
                                            <textarea name="cancel_policy" placeholder="Deskripsikan Bagaimana jika peserta anda membatalkan tiket yang sudah ia pesan" onChange={handleChange} value={cancel_policy} cols="30" rows="5" className="uk-textarea"/>
                                        </div>
                                        <div className="uk-margin">
                                            <label className="uk-form-label" htmlFor="description">Deskripsi Tiket</label>
                                            <textarea name="description"  value={description} onChange={handleChange} id="desc" cols="30" rows="5" className="uk-textarea"/>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        {
                        ticket.price !== "free" &&
                        <div className="uk-margin">
                            <label htmlFor="price">Price</label>
                            <input type="text" name="price" value={price} onChange={handleChange} className="uk-input"/>
                            {errors.price && <span>{errors.price[0]}</span>}
                        </div>
                        }
                        <p className="uk-text-right">
                            <button className="uk-button uk-button-default uk-margin-small-right uk-modal-close" type="button">Cancel</button>
                            <button className="uk-button uk-button-primary" type="submit">Save</button>
                        </p>
                    </form>
                </div>
            </div>
        )
    }
}