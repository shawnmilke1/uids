import React, { Component } from 'react';
import Layout from './Layout';
import {inject, observer} from 'mobx-react';

@inject('rootStore')
@observer
export default class Member extends Component {
    constructor(props){
        super(props);
        this.props.rootStore.memberStore.addMember = this.props.rootStore.memberStore.addMember.bind(this);
        this.props.rootStore.memberStore.handleDelete = this.props.rootStore.memberStore.handleDelete.bind(this);
    }
    componentDidMount = () => {
        this.props.rootStore.memberStore.fetchMembers(window.App.org.id);
    }
    
    render(){
        const {members,error} = this.props.rootStore.memberStore
        const {handleChange, addMember,email,handleDelete,isLoading, handleUpdate,roles,changeRole,organizer_role_id} = this.props.rootStore.memberStore
        return(
            <Layout>
                {
                    window.App.org.user_id == window.App.user.id && 
                    <form onSubmit={addMember(window.App.org.id)} >
                        <div className="uk-margin" data-uk-margin>
                            <div >
                                <label htmlFor="member">Tambah Member</label>
                                <input onChange={handleChange} name="email" value={email} className="uk-input uk-margin-small-right" type="email" placeholder="type user email" />
                                {
                                    error.email &&
                                    <span className="text-red">{error.email[0]}</span>
                                }
                            </div>
                            <button className="uk-button uk-button-default" disabled={isLoading} type="submit">
                                {
                                    isLoading ? 
                                    <div data-uk-spinner={''}></div>
                                    :
                                    'Tambah Anggota'
                                }
                            </button>
                        </div>
                    </form>
                }
                <div className="uk-child-width-1-1@m" data-uk-grid>
                    {members.length > 0 && members.map((member) => (
                        <div key={member.id}>
                            <div className="uk-card uk-card-small box-shadow-default uk-card-default">
                                <div className="uk-card-header">
                                    <h3 className="uk-card-title">{`${member.user.first_name} ${member.user.last_name}`} <span className="uk-badge">{member.role.name}</span></h3>
                                </div>
                                <div className="uk-card-body">
                                    <div className="uk-flex-middle" data-uk-grid>
                                        <div className="uk-width-3-4">
                                            <div className="uk-text-bold">{member.user.email}</div>
                                        </div>
                                        {
                                            window.App.org.user_id == window.App.user.id  && 
                                            window.App.org.user_id != member.user_id && 
                                            <div className="uk-width-1-4 uk-text-right">
                                               <p data-uk-margin>
                                                    <button data-uk-tooltip="Change Role" onClick={() => handleUpdate(member)} className="uk-icon-button" type="button" data-uk-toggle="target: #modal-example" data-uk-icon={'pencil'}></button>
                                                    <button onClick={() => handleDelete(member.id)} className="uk-icon-button" data-uk-tooltip="Hapus Member" data-uk-icon={'trash'}></button>
                                               </p>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
                <div className="uk-margin-medium-top">
                    <hr/>
                    <div data-uk-alert>
                        <a className="uk-alert-close" data-uk-close></a>
                        <h3>Notice</h3>
                        <p>Setiap role (Badge berwarna Biru) memiliki perannya masing, apa yang bisa/tidak bisa dilakukan oleh masing-masing role</p>
                        <h4>
                            Owner
                        </h4>
                        <ul>
                            <li>Membuat, mengubah, dan mem-publish Acara</li>
                            <li>Membuat, mengubah, menambahkan dan mengurangi tiket</li>
                            <li>Memperbaharui daftar peserta</li>
                            <li>Menambahkan dan mengganti role Member</li>
                        </ul>
                        <h4>
                            Manager
                        </h4>
                        <ul>
                            <li>Membuat, mengubah dan menambahkan tiket</li>
                            <li>Memperbaharui daftar peserta</li>
                        </ul>
                        <h4>Member</h4>
                        <ul>
                            <li>
                                Melihat Acara
                            </li>
                            <li>Melihat Tiket</li>
                        </ul>
                    </div>
                </div>
                <div id="modal-example" data-uk-modal>
                    <div className="uk-modal-dialog uk-modal-body">
                        <h2 className="uk-modal-title">Headline</h2>
                        <form onSubmit={changeRole} >
                            <div className="uk-margin">
                                <label htmlFor="role" className="uk-form-label">Role</label>
                                <select name="organizer_role_id" value={organizer_role_id} onChange={handleChange} className="uk-select">
                                    {
                                        roles.map(role => (
                                            <option value={role.id} key={role.id}>{role.name}</option>
                                        ))
                                    }
                                </select>
                            </div>
                            <p className="uk-text-right" data-uk-margin>
                                <button className="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                                <button className="uk-button uk-button-primary" type="submit">Save</button>
                            </p>
                        </form>
                    </div>
                </div>
            </Layout>
        )
    }
}