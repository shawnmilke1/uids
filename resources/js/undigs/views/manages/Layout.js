import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';

export default class Layout extends Component {
    render(){
        return(
            <section className="pt-5 pb-5">
                <div className="uk-grid-divider" data-uk-grid>
                    <div className="uk-width-1-4@m">
                        <ul className="uk-nav uk-nav-default ui-management-nav">
                            <li className="uk-nav-header uk-text-bold">Organizations</li>
                            <li>
                                <NavLink to={`/o/${window.App.org.slug}/settings/generals`}>General Settings</NavLink>
                            </li>
                            <li>
                                <NavLink to={`/o/${window.App.org.slug}/settings/members`}>Members</NavLink>
                            </li>
                        </ul>
                    </div>
                    <div className="uk-width-3-4@m">
                        {this.props.children}
                    </div>
                </div>
            </section>
        )
    }
}