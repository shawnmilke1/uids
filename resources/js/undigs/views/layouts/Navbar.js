import React from 'react';

export default() => (
    <nav className="navbar navbar-expand-lg main-navbar bg-primary">  
        <div className="mr-auto">
            <div className="navbar-nav mr-3">
                <li>
                    <a href="#" data-toggle="sidebar" className="nav-link nav-link-lg">
                        <i className="fas fa-bars"></i>
                    </a>
                </li>
            </div>
        </div>  
        <ul className="navbar-nav navbar-right">
            <li className="dropdown">
                <a href="#" data-toggle="dropdown" className="nav-link dropdown-toggle nav-link-lg nav-link-user">
                    <img src="/vendor/admin/img/avatar/avatar-1.png" alt="Avatar" className="rounded-circle mr-1"/>
                </a>
                <div className="dropdown-menu dropdown-menu-right">
                    <a href="/" className="dropdown-item has-icon">
                        <i className="far fa-user"></i> Home Page
                    </a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item has-icon text-danger">
                        <i className="fas fa-sign-out-alt"></i> Logout
                    </a>
                </div>
            </li>
        </ul>
  </nav>
)