import React, { Component } from 'react'
import {NavLink} from 'react-router-dom';
import Event from './Event';

export default class Sidebar extends Component {
    render(){
        String.prototype.startsWith = function (str) { return (this.match("^" + str) == str) };
        return(
            <div className="main-sidebar sidebar-style-2">
                <aside id="sidebar-wrapper">
                    <div className="sidebar-brand">
                        <a href="/">UNSTAGS</a>
                    </div>
                    <div className="sidebar-brand sidebar-brand-sm">
                        <a href="/">
                            US
                        </a>
                    </div>
                    <ul className="sidebar-menu">
                        <li className="menu-header">Dashboard</li>
                        <li>
                            <NavLink exact to={`/o/${window.App.org.slug}`} className="nav-link">
                                <i className="fas fa-fire"></i> <span>Dashboard</span>
                            </NavLink>
                        </li>
                        <li>
                            <NavLink exact className="nav-link" to={`/o/${window.App.org.slug}/events`}>
                                <i className="fas fa-calendar"></i> <span>Events</span></NavLink>
                        </li>
                        {
                            window.location.pathname.startsWith(`/o/${window.App.org.slug}/event/`) &&
                            <Event />
                        }
                        <hr/>
                        <li>
                            <NavLink className="nav-link" to={`/o/${window.App.org.slug}/settings`}>
                                <i className="fas fa-cog"></i> <span>Settings</span></NavLink>
                        </li> 
                        
                    </ul>
                </aside>
            </div>
        )
    }
}