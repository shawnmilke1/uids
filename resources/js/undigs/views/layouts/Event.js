import React, { Component } from 'react'
import { NavLink, withRouter } from 'react-router-dom';
import { observer,inject } from 'mobx-react';

@withRouter
@inject('rootStore')
@observer
export default class Event extends Component {
    render() {
        const {event_id} = this.props.rootStore.ticketStore;
        return (
            <>
                <hr/>
                <li>
                    <NavLink className="nav-link" to={`/o/${window.App.org.slug}/event/${event_id}/dashboard`} >
                        <i className="fas fa-calendar"></i> <span>Event Dashboard</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink className="nav-link" to={`/o/${window.App.org.slug}/event/${event_id}/tickets`}>
                        <i className="fas fa-ticket-alt"></i> <span>Ticket</span>
                    </NavLink>
                </li> 
                <li>
                    <NavLink  className="nav-link" to={`/o/${window.App.org.slug}/event/${event_id}/audiences`}>
                        <i className="fas fa-users"></i> <span>Data Peserta</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink className="nav-link"  to={`/o/${window.App.org.slug}/event/${event_id}/stats`}>
                        <i className="fas fa-chart-bar"></i> <span>Stats</span>
                    </NavLink>
                </li>
            </>
        )
    }
}