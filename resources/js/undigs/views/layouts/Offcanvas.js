import React from 'react';
import { NavLink } from 'react-router-dom';
import Event from './Event';

export default() => (
    <div id="offcanvas-slide" data-uk-offcanvas="overlay: true">
        <div className="uk-offcanvas-bar dash-sidebar">

            <button className="uk-offcanvas-close" type="button" data-uk-close></button>
            <ul data-uk-nav="multiple:true" className="uk-nav-parent-icon uk-margin-medium-top">
                <li className="uk-parent uk-margin-small-bottom">
                    <a href="#" className="uk-active">{window.App.org.slug}</a>
                    <ul className="uk-nav-sub">
                        <li>
                            <a href="/choose">Ganti Organisasi</a>
                        </li>
                        <li>
                            <a onClick={(e) => {
                                e.preventDefault();
                                document.getElementById('form-logout').submit();
                            }}>Log Out</a>
                        </li>
                    </ul>
                </li>
                <li key="dashboard">
                    <NavLink exact activeClassName="uk-active" to={`/o/${window.App.org.slug}`}><span className="uk-margin-small-right" uk-icon="icon: table"></span> Dashboard</NavLink>
                </li>
                <li key="events">
                    <NavLink activeClassName="uk-active" to={`/o/${window.App.org.slug}/events`} ><span className="uk-margin-small-right" data-uk-icon="icon: thumbnails"></span> Events</NavLink>
                </li>

            </ul>
            {
                window.location.pathname.startsWith(`/o/${window.App.org.slug}/event/`) &&
                <ul id="sidebar-bottom" className="uk-nav-parent-icon uk-margin-small-top" data-uk-nav>
                    <Event />
                </ul>
            }

            <ul id="setting" className="uk-nav-parent-icon uk-margin-small-top" data-uk-nav>
                <li>
                    <NavLink activeClassName="uk-active" to={`/o/${window.App.org.slug}/settings/generals`} ><span className="uk-margin-small-right" data-uk-icon="icon: cog"></span>Managements</NavLink>
                </li>
            </ul>

        </div>
    </div>
)