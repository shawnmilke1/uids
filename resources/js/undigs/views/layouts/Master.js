import React, { Component } from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import AppRouter from '../../AppRouter';
import Navbar from './Navbar';
import Sidebar from './Sidebar';

export default class Master extends Component {
    render(){
        return (
            <Router>
                <div className="main-wrapper main-wrapper-1">
                    <Navbar />
                    <Sidebar/>
                    <div className="main-content">
                        <AppRouter/>
                    </div>
                </div>
            </Router>
        )
    }
}