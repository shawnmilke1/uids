import React, { Component } from 'react'

export default class Loading extends Component {
    render(){
        return(
            <div className="uk-text-center">
                <span data-uk-spinner={"ratio: 4.5"}></span>
            </div>
        )
    }
}