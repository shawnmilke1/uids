import React, { Component } from 'react'
import { observer, inject } from 'mobx-react';
import {Link} from 'react-router-dom';

@inject('rootStore')
@observer
export default class Dashboard extends Component {
    componentDidMount = () => {
      this.props.rootStore.eventStore.fetchEvents();
      this.props.rootStore.eventStore.fetchCollection();
      this.props.rootStore.memberStore.getTotalMembers(window.App.org.id);
    }
    
    render(){
        const {events,ticketSell,ticketCreated,visitors,sellRate} = this.props.rootStore.eventStore;
        const {totalMembers} = this.props.rootStore.memberStore
        const {slug} = window.App.org;
        return(
            <section className="section pt-5">
                <div className="header-body">
                    <div className="uk-child-width-1-4@l uk-flex-center" data-uk-grid data-uk-height-match="target: > div > .uk-card > .uk-card-body">
                        <div>
                            <div className="card card-primary">
                                <div className="card-header">
                                    <h4>Events</h4>
                                    <div className="card-header-action">
                                        <i className="fas fa-calendar"></i>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <span className="h2 font-weight-bold">
                                        {events.length}
                                    </span>
                                    <p className="mt-3 mb-0 text-muted text-sm">
                                        <Link to={`/o/${slug}/events`} >Details</Link>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="card card-primary">
                                <div className="card-header">
                                    <h4>Tiket Dibuat</h4>
                                    <div className="card-header-action">
                                        <i className="fas fa-ticket-alt text-red"></i>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <span className="h2 font-weight-bold">
                                        {ticketCreated}
                                    </span>
                                    <p className="mt-3 mb-0 text-muted text-sm">
                                        Dari semua acara
                                            </p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="card card-primary">
                                <div className="card-header">
                                    <h4>Tiket Terjual</h4>
                                    <div className="card-header-action">
                                        <i className="fas fa-ticket-alt text-blue"></i>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <span className="h2 font-weight-bold">
                                        {ticketSell}
                                    </span>
                                    <p className="mt-3 mb-0 text-muted text-sm">
                                        Dari semua acara
                                            </p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="card card-primary">
                                <div className="card-header">
                                    <h4>Pengunjung</h4>
                                    <div className="card-header-action">
                                        <i className="fas fa-chart-bar text-red"></i>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <span className="h2 font-weight-bold">
                                        {visitors}
                                    </span>
                                    <p className="mt-3 mb-0 text-muted text-sm">
                                        Dari semua acara
                                            </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-md-8">
                        <div className="card profile-widget">
                            <div className="profile-widget-header">
                                <img src={window.App.org.image ? `/storage/${window.App.org.image}` : `/vendor/admin/img/avatar/avatar-1.png`} className="rounded-circle profile-widget-picture" alt={window.App.org.name} />
                                <div className="profile-widget-items">
                                    <div className="profile-widget-item">
                                        <div className="profile-widget-item-label">Anggota</div>
                                        <div className="profile-widget-item-value">{totalMembers}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="profile-widget-description">
                                <div className="profile-widget-name">
                                    {window.App.org.name}
                                </div>
                                <p>
                                    {window.App.org.description}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card mt-4">
                            <div className="card-header">
                                <h4>Ticket Sell Rate</h4>
                            </div>
                            <div className="card-body text-center">
                                <div className="display-4">
                                    {sellRate}%
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}