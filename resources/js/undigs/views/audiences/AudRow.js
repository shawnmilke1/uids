import React, { Component } from 'react'
require('datatables.net');

export default class AudRow extends Component { 
    render(){
        return(
            <div className="card">
                <div className="card-body">
                    <div className="table-responsive">
                        <div className="row align-items-center">
                            <div className="col-md-8 aud-buttons"></div>
                            <div className="col-md-4 aud-search text-right">

                            </div>
                        </div>
                        <table id="auds-table" className="table table-striped">
                            <thead className="thead-light">
                                <tr>
                                    <th scope="col">#ID</th>
                                    <th scope="col">Nama Lengkap</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Tiket</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Check</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}