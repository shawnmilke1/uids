import React, { Component } from 'react'
import { inject, observer } from 'mobx-react';
import AudRow from './AudRow';
import {computed} from 'mobx';

@inject('rootStore')
@observer
export default class AudIndex extends Component {
    constructor(props){
        super(props);
    }
    componentDidMount = () => {
        this.props.rootStore.audienceStore.allAuds(this.props.match.params.id);
        this.props.rootStore.ticketStore.updateEventId(this.props.match.params.id);
    }
    render(){
        const {audiences,checkLength} = this.props.rootStore.audienceStore;
        return(
            <div className="pt-8 pb-5">
                <div className="container-fluid">
                    <div className="uk-child-width-1-2@m" data-uk-grid>
                        <div>
                            <div className="uk-card uk-card-small uk-card-default box-shadow-default uk-margin-medium-bottom uk-border-rounded">
                                <div className="uk-card-header bg-blue-dark text-white">
                                    <div id="testing" className="uk-text-bold">Attention</div>
                                </div>
                                <div className="uk-card-body">
                                    <p>
                                        Disini anda dapat melihat daftar peserta yang sudah mendaftar pada acara yang anda buat,
                                        berikut dengan status pembayarannya. Jika acara anda sudah memasuki hari H, fitur "Check In" akan aktif
                                        yang berguna untuk melakukan pencatatan jika peserta telah berada pada area lokasi acara anda.
                                </p>
                                    <p className="uk-text-bold">
                                        Perhatian untuk Memperbaharui status, update status hanya dilakukan ketika peserta telah membayar tiket yang anda buat ( jika tiket bertipe berbayar )
                                </p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="uk-card uk-card-small uk-card-default box-shadow-default uk-margin-medium-bottom uk-border-rounded">
                                <div className="uk-card-header bg-blue-dark text-white">
                                    <div className="uk-text-bold">Peserta</div>
                                </div>
                                <div className="uk-card-body">
                                    <div className="uk-text-large">
                                        Total : {audiences.length}
                                    </div>
                                    <hr />
                                    <div className="uk-text-large">
                                        Peserta Masuk : {checkLength}
                                    </div>
                                    <div>
                                        {Math.floor((checkLength / audiences.length) * 100)}%
                                </div>
                                    <div>
                                        <progress id="js-progressbar" className="uk-progress" value={checkLength} max={audiences.length}></progress>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {
                        window.App.role != 'member' &&
                        <AudRow rootStore={this.props.rootStore} id={this.props.match.params.id} auds={audiences} />
                    }
                </div>
            </div>
        )
    }
}