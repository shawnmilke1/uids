import React, { Component } from 'react'
import {Link} from 'react-router-dom';
import {inject,observer} from 'mobx-react';

@inject('rootStore')
@observer
export default class ShowEvent extends Component {
    constructor(props){
        super(props);
        this.state = {
            event:{},
            totalAuds:''
        }
    }
    componentDidMount = () => {
        axios.get(`/api/events/${this.props.match.params.id}`).then(res => this.setState({event:res.data}))
        .catch(err => {
            if(err.response.status == 403){
                window.location.href = '/myevents';
            }
        });
        ;
        this.props.rootStore.ticketStore.updateEventId(this.props.match.params.id);
        axios.get(`/api/events/${this.props.match.params.id}/get-total-auds`).then(res => this.setState({ totalAuds: res.data }));
    }
    copyClipboard(){
        let copy = document.getElementById('eventUrl');
        copy.select();
        document.execCommand('copy');
        UIkit.notification({
            message:'Copy to clipboard',
            status:'primary',
            pos:'bottom-center',
            timeout:5000
        })
    }
    
    render(){
        const {event} = this.state;
        const {updateEvent} = this.props.rootStore.eventStore;
        const ticketsSell = _.sumBy(event.tickets,'ticket_sell');
        const ticketLeft = _.sumBy(event.tickets,'qty');
        const percent = Math.floor((ticketsSell / ticketLeft) * 100);
        const {tickets} = event;
        return(
            <section className="pt-5 pb-5">
                <div>
                    <div className="uk-text-center">
                        <h2>{event.title}</h2>
                    </div>
                    <div className="uk-text-center">
                        {
                            event.status == "DRAFT" &&
                            <div>
                                <a href={`/events/${this.props.match.params.id}/edit`} className="btn btn-primary uk-margin-small-right">Edit</a>
                                <a href={`/events/preview/${event.slug}`} className="btn btn-info uk-margin-small-right">Preview</a>
                                <form onSubmit={updateEvent(event)} className="uk-display-inline">
                                    <button type="submit" className="btn btn-danger">Publish</button>
                                </form>
                            </div>
                        }

                    </div>
                </div>
                <hr />
                <div className="uk-child-width-1-2@m uk-grid-small uk-grid-match" data-uk-grid>
                    <div>
                        <div className="uk-card uk-card-body uk-card-small uk-card-default uk-border-rounded">
                            <div className="uk-grid-small uk-flex-top" data-uk-grid>
                                <div className="uk-width-auto">
                                    <i className="fas fa-ticket-alt"></i>
                                </div>
                                <div className="uk-width-expand">
                                    <div className="uk-margin-remove-bottom uk-text-bold">Tiket Terjual</div>
                                    <p className="uk-margin-remove-top uk-text-small">
                                        {event.tickets && event.tickets.length > 0 ? `${_.sumBy(event.tickets, 'ticket_sell')} tiket terjual / ${_.sumBy(event.tickets,'qty')} ` : 'gaada'}
                                    </p>
                                   {
                                       event.tickets && event.tickets.length > 0 && 
                                        <div className="progress">
                                            <div className="progress-bar"
                                                role="progressbar"
                                                data-width={`${percent}%`}
                                                aria-valuenow={ticketsSell}
                                                aria-valuemin="0"
                                                aria-valuemax={ticketLeft}
                                                style={{width:`${percent}%`}}
                                            >{`${percent}%`}</div>
                                        </div>
                                   }
                                </div>
                                <div className="uk-width-auto">
                                   <Link className="btn btn-primary btn-sm" to={`/o/${window.App.org.slug}/event/${event.id}/tickets`}>View More</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="uk-card uk-card-body uk-card-small uk-card-default uk-border-rounded">
                            <div className="uk-grid-small uk-flex-top" data-uk-grid>
                                <div className="uk-width-auto">
                                    <i className="fas fa-users"></i>
                                </div>
                                <div className="uk-width-expand">
                                    <div className="uk-margin-remove-bottom uk-text-bold">Peserta</div>
                                    <p className="uk-margin-remove-top uk-text-small">{this.state.totalAuds}</p>
                                    <Link to={`/o/${window.App.org.slug}/event/${event.id}/audiences`}>Lihat Peserta</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-5">
                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-header">
                                    <h4>Tickets</h4>
                                    <div className="card-header-action">
                                        <Link to={`/o/${window.App.org.slug}/event/${event.id}/tickets`} className="btn btn-primary">Create New</Link>
                                    </div>
                                </div>
                                <div className="card-body p-0">
                                    <div className="table-responsive">
                                        <table className="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Nama</th>
                                                    <th>Jumlah Tiket</th>
                                                    <th>Tiket Tersisa</th>
                                                    <th>Limit Per-Order</th>
                                                    <th>Harga Tiket</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                {
                                                    tickets && tickets.length > 0 &&
                                                    tickets.map(ticket => (
                                                        <tr key={ticket.uuid}>
                                                            <td>{ticket.name}</td>
                                                            <td>{ticket.qty}</td>
                                                            <td>
                                                                {ticket.qty - ticket.ticket_sell}
                                                            </td>
                                                            <td>{ticket.ticket_limit}</td>
                                                            <td>
                                                                {ticket.price == "free" ? "Free" : `Rp ${ticket.price}`}
                                                            </td>
                                                            <td>
                                                                <a href="#" className="btn btn-primary">Show</a>
                                                            </td>
                                                        </tr>
                                                    ))

                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="uk-margin">
                    <label htmlFor="links">Your Event Url</label>
                    <div className="uk-inline uk-width-1-1">
                        <button onClick={() => this.copyClipboard()} className="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: copy" data-uk-tooltip="Copy Link"></button>
                        <input className="uk-input" readOnly value={`${window.location.host}/events/${event.slug}`} id="eventUrl" />
                    </div>
                </div>
                <div className="tips uk-margin-medium-top">
                    <div className="uk-card uk-card-body uk-card-default box-shadow-default">
                        <h3 className="uk-text-bold">Tips</h3>
                        <h4 className="uk-text-bold">Buat Minimal Satu Tiket</h4>
                        <ul className="uk-list">
                            <li>Tiket bukan berarti anda harus menarifkan acara yang anda buat, ini berguna untuk mengetahui kapasitas ruangan acara anda,</li>
                        </ul>
                        <h4 className="uk-text-bold">Be True</h4>
                        <ul className="uk-list">
                            <li>Jangan sekali-kali anda berbohong dengan acara yang anda dan organisasi anda buat</li>
                            <li>Jelaskan dengan rinci dan jujur pada saat anda membuat acara di kolom deskripsi acara</li>
                            <li>Jangan anda publish acara anda sebelum selesai, hati-hati menekan tombol "Publish" Diatas</li>
                        </ul>
                    </div>
                </div>
            </section>
        )
    }
}