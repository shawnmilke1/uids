import React, { Component } from 'react'
import { inject, observer } from 'mobx-react';
import {Link} from 'react-router-dom';

@inject('rootStore')
@observer
export default class IndexEvent extends Component {
    componentDidMount = () => {
        this.props.rootStore.eventStore.fetchEvents();
    }

    render(){
        const {events,changeStatus} = this.props.rootStore.eventStore
        return(
            <section className="pt-5 pb-5">
                <div>
                    <div className="row">
                        <div className="col">
                            <div className="card shadow">
                                <div className="card-header border-0">
                                    <h4 className="mb-0">Events</h4>
                                    <div className="card-header-action">
                                        <div className="mb-0">
                                            <select onChange={changeStatus} className="form-control">
                                                <option value="">All Events</option>
                                                <option value="DRAFT">Draft Event</option>
                                                <option value="PUBLISHED">Published Event</option>
                                                <option value="PASSED">Pass Event</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div className="table-responsive">
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Judul</th>
                             
                                                <th scope="col">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                events.length > 0 ?
                                                    events.map(event => (
                                                        <tr key={event.id}>
                                                            <th scope="row">
                                                                <Link to={`/o/${window.App.org.slug}/event/${event.id}/dashboard`}>
                                                                    <span className="text-sm">
                                                                        {event.title}
                                                                    </span>
                                                                </Link>
                                                            </th>
                                                
                                                            <td>
                                                                {event.status}
                                                            </td>
                                                        </tr>
                                                    ))
                                                    :
                                                    <tr>
                                                        <td colSpan="3">
                                                            <div className="uk-text-center">
                                                                <h4 className="uk-text-muted">
                                                                    Tidak Ada Acara pada List Ini
                                                            </h4>
                                                                <a href="/events/create" className="uk-button uk-button-primary uk-border-rounded">Buat Acara</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
    
}