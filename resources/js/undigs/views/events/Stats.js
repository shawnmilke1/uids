import React from 'react';
import Chart from 'chart.js';
import {observer , inject} from 'mobx-react';

@inject('rootStore')
@observer
export default class Stats extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            dates:[],
            data:[]
        }
    }
    componentDidMount = () => {
        this.props.rootStore.ticketStore.updateEventId(this.props.match.params.id);
        axios.get(`/api/events/${this.props.match.params.id}/stats`).then(res => {
            let dates = new Array();
            let data = new Array();
            _.forEach(res.data, function(date ,key){
                dates.push(key);
                data.push(date.length)
            });
            this.setState({
                dates:dates,
                data:data
            })
            let ctx = document.getElementById('stats').getContext('2d');
            new Chart(ctx, {
                type: 'line',
                data: {
                    labels:dates,
                    datasets:[{
                        label: 'Pengunjung',
                        data:data,
                        borderWidth: 2,
                        backgroundColor: '#6777ef',
                        borderColor: '#6777ef',
                        borderWidth: 2.5,
                        pointBackgroundColor: '#ffffff',
                        pointRadius: 4
                    }],
                },
                options: {
                    legend:{
                        display:false
                    },
                    scales: {
                        yAxes: [{
                            gridLines:{
                                drawBorder:false,
                                color:'#f2f2f2'
                            },
                            ticks:{
                                beginAtZero:true,
                                stepSize:50
                            }
                        }]
                    }
                },
                
            })
        })
    }

    render(){
        return(
            <div className="pt-5 pb-5">
                <div className="container-fluid">
                    <h2>
                        Event Stats
                    </h2>
                    <p>
                        Data dibawah adalah hasil dari analisa halaman acara anda, halaman ini mencatat jumlah pengunjung pada saat acara anda sudah berstatus "PUBLISHED" hingga akhir acara dimulai.
                    </p>
                    <div className="card">
                        <div className="card-body">
                            <canvas id="stats"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}