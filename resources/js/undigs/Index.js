import React, { Component } from 'react'
import {Provider} from 'mobx-react';
import ReactDOM from 'react-dom';
import Master from './views/layouts/Master';
import RootStore from './store/RootStore';

export default class Index extends Component {    
    render(){
        return(
            <Provider rootStore={new RootStore()} >
                <Master/>
            </Provider>
        )
    }
}

ReactDOM.render(<Index/>,document.getElementById('undigs'));