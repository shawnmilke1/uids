importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js');

if (workbox) {
    workbox.setConfig({ debug: false });
    workbox.precaching.precacheAndRoute([]);

} else {
    console.log(`Boo! Workbox didn't load 😬`);
}