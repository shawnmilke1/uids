import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
ClassicEditor
    .create(document.querySelector('#editor'), {
        toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
        heading: {
            options: [
                { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                { model: 'heading2', view: 'h2', title: 'Heading', class: 'ck-heading_heading2' }
            ]
        }
    })
    .catch(error => {
        console.log(error);
    });

$('#input-image').change(function () {
    var file = this.files[0];
    var imgimg = document.getElementById('imgimg');
    var reader = new FileReader();
    reader.addEventListener("load", function () {
        imgimg.src = reader.result;
    }, false);
    if (file) {
        reader.readAsDataURL(file);
    }
    var container = document.getElementById('img-container');
    container.removeAttribute('hidden');
})


let upload = document.getElementById('uploader')

$('#input-image').change(function () {
    upload.setAttribute('hidden', 'true');
});
if (window.location.pathname != '/events/create') {
    upload.setAttribute('hidden', 'true')
}


$('#remove-button').click(function () {
    upload.removeAttribute('hidden');
    $('#img-container').attr('hidden', 'true');
})

//date and time picker
let starts_date = $('#starts_date');
let ends_date = $("#ends_date");
let end_id = document.getElementById('ends_time');
let start_id = document.getElementById('starts_time');
let date = new Date();

const ends_time = flatpickr(end_id, {
    enableTime: true,
    noCalendar: true,
    format: "H:i",
    time_24hr: true
});


const starts_time = flatpickr(start_id, {
    enableTime: true,
    noCalendar: true,
    format: 'H:i',
    time_24hr: true,
});


starts_date.flatpickr({
    dateFormat: "Y-m-d",
    minDate: new Date().fp_incr(4),
    onChange: function (selectedDates, dateStr, instance) {
        ends_date.removeAttr('disabled');
        const dateStart = dateStr;
        let ends = ends_date.flatpickr({
            dateFormat: 'Y-m-d',
            minDate: dateStr,
            onChange: function (selectedDates, dateStr) {
                if (dateStart == dateStr) {
                    starts_time.set({
                        minTime: `${date.getHours()}:${date.getMinutes()}`,
                        defaultHour: date.getHours(),
                        defaultMinute: date.getMinutes(),
                    })
                    ends_time.set({
                        minTime: `${date.getHours() + 1}:${date.getMinutes()}`,
                        defaultHour: date.getHours() + 1,
                        defaultMinute: date.getMinutes(),
                    })
                } else {
                    starts_time.set({
                        minTime: '00:00',
                    })
                    ends_time.set({
                        minTime: '00:00',
                    })
                }
            }
        });
        ends_date.val(dateStr);
        if (starts_date.val() == ends_date.val()) {
            starts_time.set({
                minTime: `${date.getHours()}:${date.getMinutes()}`,
                defaultHour: date.getHours(),
                defaultMinute: date.getMinutes()
            })
            ends_time.set({
                minTime: `${date.getHours() + 1}:${date.getMinutes()}`,
                defaultHour: date.getHours() + 1,
                defaultMinute: date.getMinutes()
            })
        } else {
            starts_time.set({
                minTime: '00:00',
            })
            ends_time.set({
                minTime: '00:00',
            })
        }
    }
});
if (window.location.pathname != '/events/create') {
    ends_date.flatpickr({
        dateFormat: 'Y-m-d',
        minDate: starts_date.val(),
        onChange: function (selectedDates, dateStr) {
            if (starts_date.val() == dateStr) {
                starts_time.set({
                    minTime: `${date.getHours()}:${date.getMinutes()}`,
                    defaultHour: date.getHours(),
                    defaultMinute: date.getMinutes(),
                })
                ends_time.set({
                    minTime: `${date.getHours() + 1}:${date.getMinutes()}`,
                    defaultHour: date.getHours() + 1,
                    defaultMinute: date.getMinutes(),
                })
            } else {
                starts_time.set({
                    minTime: '00:00',
                })
                ends_time.set({
                    minTime: '00:00',
                })
            }
        }
    });
}

$(document).ready(function(){
    $('#city-select').select2({
        ajax: {
            url: '/api/cities',
            dataType:'json',
            type:"GET",
            delay:250,
            data: function(params){
                    return {
                        s: params.term, // search term
                        page: params.page
                    };
            },
            processResults: function (data) {
                return {
                results:  $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            },
            cache: true,
        },
        placeholder: "Pilih Kota",
        minimumInputLength: 3,
    })
});