@extends('layouts.app')
@section('title','Temukan Acara Keren yang dibuat mahasiswa atau buat Acara Anda sendiri')
@section('desc','Temukan Acara keren yang dibuat mahasiswa atau buat acara anda sendiri di unstags')
@section('ogtitle','Temukan Acara Keren yang dibuat mahasiswa atau buat Acara Anda sendiri')
@section('ogdesc','Temukan Acara keren yang dibuat mahasiswa atau buat acara anda sendiri di unstags')
@section('keywords','Unstags, Unkindled Idea, Student Events, Student Activity, University Events, Gigs, Acara Mahasiswa, Event Mahasiswa, Aktivitas Kampus, Acara Kampus')
@section('content')
    <section class="uk-section bg-blue-lightest">
        <div class="uk-container">
            <div class="uk-grid uk-flex-middle">
                <div class="uk-width-1-3@m uk-text-left@m uk-text-center">
                    <h2>Make Something Students Want</h2>
                    <p class="uk-margin-medium-bottom text-300">
                        Create or attend events made by students.
                    </p>
                    <p uk-margin>
                        <a href="/overview" class="uk-button btn-up btn-shadow uk-button-danger">What is Unstags</a>
                        <a href="{{route('help.index')}}" class="uk-button ui-btn-white btn-shadow btn-up">Help Center</a>
                    </p>
                </div>
                <div class="uk-width-2-3@m uk-text-center uk-text-right@m">
                    <div class="uk-padding-small">
                        <img src="/images/icons/header.svg" width="600" alt="Welcome Cover">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="uk-section bg-grey-lightest">
        <div class="uk-container">
            <div class="uk-child-width-1-3@m" uk-grid>
            @forelse ($events as $event)
            <div>
                <a href="{{route('event.show',$event->slug)}}" class="ui-card-link">
                    <div class="uk-card uk-card-default uk-card-small">
                        <div class="uk-card-media-top uk-cover-container">
                            <img data-src="{{asset('storage/'.$event->image)}}" alt="{{$event->title}}" uk-img uk-cover>
                            <canvas width="500" height="250"></canvas>
                        </div>
                        <div class="uk-card-body">
                            <div class="uk-card-badge uk-label">
                                <span class="uk-text-bold uk-text-small">
                                    {{parseDate($event->starts_date)->format('d M')}}
                                </span>
                            </div>
                            <h3 class="size-16 uk-margin-remove-top">{{$event->title}}</h3>
                            <p class="uk-text-small">{{$event->city->name}}</p>
                            <div class="uk-child-width-1-2@m uk-flex-middle uk-grid-small" uk-grid>
                                <div class="uk-text-left@m uk-text-center">
                                    <span class="text-red uk-text-bold">
                                        @if($event->tickets()->where('price','free')->get()->count() > 0)
                                        Free
                                        @elseif(!$event->tickets()->exists())
                                        Tanpa Tiket
                                        @else
                                        No Free
                                        @endif
                                    </span>
                                </div>
                                <div class="uk-text-right@m uk-text-center">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @empty
                <div class="uk-width-expand uk-flex uk-flex-center uk-text-center">
                    <div>
                        <div class="uk-text-bold uk-text-large uk-margin-small-bottom">Tidak ada acara yang ditemukan</div>
                        <a href="{{route('event.create')}}" class="uk-button uk-button-primary uk-border-rounded">Buat Acara</a>
                    </div>
                </div>
            @endforelse
            </div>
            @if(count($events) > 0)
            <div class="uk-text-center uk-margin-medium-top">
                <a href="{{route('event.index')}}" class="uk-button uk-button-primary border-80">More Events</a>
            </div>
            @endif
        </div>
    </section>
    <section class="uk-section all-h3-bold uk-text-center">
        <div class="uk-container">
            <div class="uk-child-width-1-3@m uk-grid-small" uk-grid>
                <div>
                    <div class="uk-card uk-card-body uk-card-small">
                        <img src="{{asset('/images/icons/event.svg')}}" width="150" alt="Buat Acara Sebanyak Mungkin">
                        <h3>Buat Acara Sebanyak Mungkin</h3>
                        <p>
                            Buat acara sebanyak mungkin di unstags dengan konfigurasi acara standar dan premium
                        </p>
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-body uk-card-small">
                        <img src="{{asset('/images/icons/network.svg')}}" width="150" alt="Buat Acara Sebanyak Mungkin">
                        <h3>Integrasi Sosial Media</h3>
                        <p>
                            Integrasikan acara anda dengan sosial media seperti facebook event dan lain-lain untuk menjangkau sebanyak mungkin peserta acara
                        </p>
                    </div>
                </div>
                <div>
                    <div class="uk-card uk-card-body uk-card-small">
                        <img src="{{asset('/images/icons/message.svg')}}" width="150" alt="Buat Acara Sebanyak Mungkin">
                        <h3>It's All UnPaper</h3>
                        <p>
                            Sudah tidak ingin menggunakan metode lama mencatat peserta acara ? gunakan tiket online untuk mengatur daftar peserta acara anda.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection