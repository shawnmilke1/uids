@extends('auth.settings.layout')
@section('title','Ticket List')
@section('setting-title','Ticket List')
@section('setting-filter')
    <div class="uk-margin-small-bottom">
        <div class="uk-child-width-1-4@s uk-grid-small" uk-grid>
            <div>
                <a class="uk-text-bold uk-button uk-button-small uk-width-1-1 bg-blue-light text-white" href="?tanggal={{now()->format('Y-m-d')}}">
                    Now
                </a>
            </div>
            <div>
                <a class="uk-text-bold uk-button uk-button-small uk-width-1-1 bg-blue-light text-white" href="?tanggal={{now()->format('m')}}">
                    Bulan Ini
                </a>
            </div>
            <div>
                <a href="/@/purchase" class="uk-button uk-button-small uk-width-1-1 bg-red-light text-white">Reset Filter</a>
            </div>
        </div>
    </div>
@endsection
@section('setting-content')
    <div>
        <div class="uk-child-width-1-1@m" uk-grid>
            @forelse ($auds as $aud)
            <div>
                <div class="uk-card box-shadow-default uk-card-small border-default uk-border-rounded ">
                    <div class="uk-card-header">
                        <div class="uk-child-width-1-2" uk-grid>
                            <div>
                                <div>Ticket Id <strong>{{$aud->id}}</strong></div>
                            </div>
                            <div class="uk-text-right">
                                <div class="uk-text-bold">
                                    @if($aud->ticket->price !== "free")
                                        Rp {{number_format($aud->ticket->price,0)}}
                                    @else
                                        Free
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-card-body bg-grey-lighter">
                        <div class="uk-text-bold">
                            {{$aud->event->title}}
                        </div>
                    </div>
                    <div class="uk-card-footer">
                        <div class="uk-child-width-1-2" uk-grid>
                            <div>
                                <div class="uk-text-bold">{{title_case($aud->status)}}</div>
                            </div>
                            <div class="uk-text-right">
                                <a href="{{route('audience.show',$aud->id)}}" class="uk-text-bold uk-margin-small-right">Details</a>
                                <button type="button" class="uk-icon-link uk-margin-small-left" uk-icon="more"></button>
                                <div uk-dropdown="mode:click;">
                                    <ul class="uk-nav uk-dropdown-nav uk-text-left">
                                        <li><a href="#">Item</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @empty
                <div class="uk-width-expand uk-text-center">
                    <div>
                        <img src="{{asset('images/icons/tickets.svg')}}" width="100">
                        <h2>No Ticket Found</h2>
                        <a href="{{route('event.index')}}" class="uk-button uk-button-primary uk-button-small">Browse Events</a>
                    </div>
                </div>
            @endforelse
            {{$auds->links('vendor.pagination.default')}}
        </div>
    </div>
@endsection