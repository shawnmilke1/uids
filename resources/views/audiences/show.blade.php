@extends('auth.settings.layout')
@section('title','Ticket List')
@section('setting-title',title_case($aud->status))
@section('setting-content')
    <div class="uk-child-width-1-3@m" uk-grid>
        <div>
            <div class="uk-text-muted">BOOKED ON</div>
            <div class="uk-text-bold">{{parseDate($aud->created_at)->format('l, d M Y, H:i')}}</div>
        </div>
        <div>
            <div class="uk-text-muted">PAYMENT METHOD</div>
            <div class="uk-text-bold">
                {{title_case($aud->payment->method)}}
            </div>
        </div>
    </div>
@endsection
@section('add_content')
    <div class="uk-card uk-card-small uk-card-default box-shadow-default uk-margin-small-top">
        <div class="uk-card-header">
            <ul uk-accordion>
                <li>
                    <a class="uk-accordion-title uk-text-bold" href="#">Ticket ID #{{$aud->id}} ({{$aud->ticket->price == "free" ? "Free" : "Rp ".number_format($aud->ticket->price,0)}})</a>
                    <div class="uk-accordion-content">
                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-expand" uk-leader="fill: .">Cost</div>
                            <div>
                                {{$aud->ticket->price == "free" ? "Free" : "Rp ".number_format($aud->ticket->price,0)}}
                            </div>
                        </div>
                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-expand" uk-leader="fill: .">Discount</div>
                            <div>
                                Rp 0
                            </div>
                        </div>
                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-expand uk-text-bold" uk-leader="fill: .">Total</div>
                            <div class="uk-text-bold">
                                {{$aud->ticket->price == "free" ? "Free" : "Rp ".number_format($aud->ticket->price,0)}}
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="uk-card-body">
            <ul class="uk-list uk-list-striped purchase-list m-n-20">
                <li>
                    <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
                        <div>
                            <div>
                                Booked By
                            </div>
                        </div>
                        <div>
                            <div class="uk-text-bold">
                                {{$aud->full_name}}
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
                        <div>
                            <div>
                                Email
                            </div>
                        </div>
                        <div>
                            <div class="uk-text-bold">
                                {{$aud->email}}
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
                        <div>
                            <div>
                                Event Title
                            </div>
                        </div>
                        <div>
                            <div class="uk-text-bold">
                                {{$aud->event->title}}
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
                        <div>
                            <div>
                                Destination
                            </div>
                        </div>
                        <div>
                            <div class="uk-text-bold">
                                {{$aud->event->city->name}}
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
                        <div>
                            <div>
                                Start Date
                            </div>
                        </div>
                        <div>
                            <div class="uk-text-bold">
                                {{parseDate($aud->event->starts_date)->format('l, d M Y')}} - {{parseDate($aud->event->starts_time)->format('H:i')}}
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-child-width-1-2@m uk-grid-small" uk-grid>
                        <div>
                            <div>
                                End Date
                            </div>
                        </div>
                        <div>
                            <div class="uk-text-bold">
                                {{parseDate($aud->event->ends_date)->format('l, d M Y')}} - {{parseDate($aud->event->ends_time)->format('H:i')}}
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="uk-child-width-1-2@m uk-grid-small uk-flex-middle" uk-grid>
                        <div>
                            <div>
                                Ticket Usage Info
                            </div>
                        </div>
                        <div>
                            <div class="uk-text-bold uk-text-small">
                                Tunjukan List Tiket Ini atau tiket yang dikirimkan ke email anda kepada panitia acara.
                            </div>
                        </div>
                    </div>
                </li>
                
            </ul>
        </div>
    </div>
    <div class="uk-card uk-card-small uk-card-default uk-margin-small-top box-shadow-default uk-card-body">
        <p>
            If there is a problem with the application, please <a href="{{route('contact.index')}}">Contact Us</a>
        </p>
    </div>
@endsection