@extends('layouts.app')
@section('title',$ticket->event->title)
@section('ogtitle',$ticket->event->title)
@section('ogdesc',str_limit(strip_tags($ticket->event->description),200))
@section('desc')
    {{str_limit(strip_tags($ticket->event->description),200)}}
@endsection
@section('ogimage',asset('storage/'.$ticket->event->image))
@section('keywords')
Acara tentang {{$ticket->event->type->name}}
@endsection
@section('content')
    <section class="uk-section bg-grey-lighter">
        <div class="uk-container uk-container-small">
            <h2>Buy Ticket</h2>
            <span class="uk-text-muted">Isi Form Dibawah Ini Dengan Benar</span>
            <div class="uk-margin-small-top" uk-grid>
                <div class="uk-width-2-3@m">
                    @foreach ($errors->all() as $item)
                        <h2>{{$item}}</h2>
                    @endforeach
                   <form action="{{route('aud.store',$ticket->uuid)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @for ($i = 0; $i < $qty; $i++) 
                            <div class="uk-card uk-card-default uk-card-small box-shadow-default">
                                <div class="uk-card-header">
                                    <div class="uk-text-bold">Tiket {{$i+1}}</div>
                                </div>
                                <div class="uk-card-body">
                                    <div class="uk-margin">
                                        <label class="uk-form-label" for="full_name">Nama Lengkap</label>
                                        <input type="text" name="auds[{{$i}}][full_name]" value="{{old("auds.$i.full_name")}}" placeholder="Seperti Nama di KTP / Kartu Tanda Mahasiswa, tanpa karakter spesial"
                                            class="uk-input" required />
                                    </div>
                                    <div class="uk-margin">
                                        <label class="uk-form-label" for="uk-form-label">Email</label>
                                        <input type="email" name="auds[{{$i}}][email]" value="{{old("auds.$i.email")}}" placeholder="UnPaper / E-Ticket akan di kirimkan ke email ini"
                                            class="uk-input" required>
                                    </div>
                                    <div class="uk-margin">
                                        <label class="uk-form-label" for="phone">Phone</label>
                                        <input type="text" name="auds[{{$i}}][phone]" value="{{old("auds.$i.phone")}}" class="uk-input" placeholder="Anda Pasti Punya Handphone kan ? :)">
                                    </div>
                                </div>
                            </div>
                            <hr>
                        @endfor
                        <div class="uk-text-right">
                            <button type="submit" class="uk-button uk-button-primary">Continue Buy Ticket</button>
                        </div>
                    </form>
                </div>
                <div class="uk-width-1-3@m">
                    <div class="uk-card uk-card-default box-shadow-default uk-card-small">
                        <div class="uk-card-header">
                            <div class="uk-text-bold">Event Detail</div>
                        </div>
                        <div class="uk-card-body">
                            <dl class="uk-description-list uk-description-list-divider">
                                <dt>Tanggal Mulai</dt>
                                <dd>{{parseDate($ticket->event->starts_date)->format('l, d F , Y')}}</dd>
                                <dt>Jam Mulai</dt>
                                <dd>{{parseDate($ticket->event->starts_time)->format('H:i')}}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection