@extends('auth.settings.layout')
@section('title', 'Password')
@section('setting-title','Change Password')
@section('setting-content')
    <div>
        <form action="{{route('usetting.post_password')}}" method="post">
            @csrf
            @if(Session::has('error'))
              
                <div class="uk-margin">
                    <div class="uk-alert">
                       {{Session::get('error')}}
                    </div>
                </div>
            @endif
            @if($user->password != null)
            <div class="uk-margin">
                <label for="password" class="uk-form-label">Old Password</label>
                <input type="password" name="password" placeholder="Masukkan Password Lama" class="uk-input">
                {!! if_error($errors,'password','text-red') !!}
            </div>
            @endif
            <div class="uk-margin">
                <label for="new_password" class="uk-form-label">New Password</label>
                <input type="password" name="new_password" class="uk-input">
                {!! if_error($errors,'new_password','text-red') !!}
            </div>
            <div class="uk-margin">
                <label for="new_password_confirmation" class="uk-form-label">New Password Confirmation</label>
                <input type="password" name="new_password_confirmation" class="uk-input">
                {!! if_error($errors,'new_password_confirmation','text-red') !!}
            </div>
            <div class="uk-margin">
                <button type="submit" class="uk-button uk-button-primary uk-border-rounded">Update Password</button>
            </div>
        </form>
    </div>
@endsection