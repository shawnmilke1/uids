@extends('layouts.app')
@section('content')
    <section class="uk-section bg-grey-lighter">
        <div class="uk-container">
            <div uk-grid>
                <div class="uk-width-1-4@m">
                    <div class="uk-card uk-card-body uk-card-small uk-card-default box-shadow-default">
                        @include('auth.settings.nav')
                    </div>
                </div>
                <div class="uk-width-3-4@m">
                    @yield('setting-filter')
                    <div class="uk-card uk-card-small uk-card-default box-shadow-default">
                        <div class="uk-card-header">
                            <div class="uk-text-bold">@yield('setting-title')</div>
                        </div>
                        <div class="uk-card-body">
                            @yield('setting-content')
                        </div>
                    </div>
                    @yield('add_content')
                </div>
            </div>
        </div>
    </section>
@endsection