@extends('auth.settings.layout')
@section('setting-title','Basic Setting')
@section('title','Basic Setting')
@section('setting-content')
<div>
    <form action="{{route('usetting.store')}}" method="post">
        @csrf
        <div class="uk-child-width-1-2@m" uk-grid>
            <div>
                <label for="first_name" class="uk-form-label">First Name</label>
                <input type="text" name="first_name" value="{{old('first_name',$user->first_name)}}" class="uk-input">
                {!! if_error($errors,'first_name','text-red') !!}
            </div>
            <div>
                <label for="last_name" class="uk-form-label">Last Name</label>
                <input type="text" name="last_name" value="{{old('last_name',$user->last_name)}}" class="uk-input">
                {!! if_error($errors,'last_name','text-red') !!}
            </div>
        </div>
        <div class="uk-margin">
            <label for="username" class="uk-form-label">Username</label>
            <input type="text" name="username" value="{{old('username',$user->username)}}" class="uk-input">
            {!! if_error($errors,'username','text-red') !!}
        </div>
        <div class="uk-margin">
            <button type="submit" class="uk-button uk-button-primary uk-border-rounded">Update</button>
        </div>
    </form>
</div>
@endsection