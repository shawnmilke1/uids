<ul class="uk-nav uk-nav-default">
    <li class="uk-nav-header">Personal Account</li>
    <li {{is_active('@/basic')}}>
        <a href="{{route('usetting.index')}}">Basic</a>
    </li>
    <li {{is_active('@/password')}} >
        <a href="{{route('usetting.password')}}">Password</a>
    </li>
    <li {{is_active( '@/connections')}}>
        <a href="{{route('social.index')}}">Connections</a>
    </li>
    <li class="uk-nav-divider"></li>
    <li class="uk-nav-header">Organizer Account</li>
    <li {{is_active('@/organizers')}}>
        <a href="{{route('organizer.index')}}">Organizer</a>
    </li>
    <li class="uk-nav-divider"></li>
    <li class="uk-nav-header">Purchase</li>
    <li {{is_active('@/purchase')}} >
        <a href="{{route('audience.index')}}">Tickets</a>
    </li>
</ul>