@extends('layouts.app')
@section('title','Password Reset')
@section('content')
<section class="uk-section bg-grey-lightest">
    <div class="uk-container">
        <div class="uk-child-width-1-2@m uk-flex-center uk-flex" uk-grid>
            <div>
                <div class="uk-card uk-card-default uk-border-rounded">
                    <div class="uk-card-header">{{ __('Reset Password') }}</div>
    
                    <div class="uk-card-body">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
    
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
    
                            <div class="uk-margin">
                                <div>
                                    <input id="email" type="email" placeholder="Your Email" class="uk-input bottom-line{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                                        required> @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span> @endif
                                </div>
                            </div>
    
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="uk-button uk-button-primary">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
