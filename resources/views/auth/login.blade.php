@extends('layouts.app')
@section('title','Login')
@section('content')
<section class="uk-section uk-section-small bg-grey-lightest">
    <div class="uk-container">
        <div class="uk-child-width-1-2@m uk-flex-center uk-grid">
            <div>
                <div class="uk-card uk-card-default uk-border-rounded">
                    @if(Session::has('unverified'))
                    <div uk-alert class="uk-alert-danger uk-margin-remove-bottom">
                        <a class="uk-alert-close" uk-close></a>
                        <h3>Unverified</h3>
                        <p>
                            Akun Ini Belum terverifikasi, silahkan verifikasi terlebih dahulu akun anda yang kami kirimkan ke email akun ini.
                        </p>
                    </div>
                    @endif
                    @if(Session::has('verified'))
                    <div uk-alert class="uk-alert-info uk-margin-remove-bottom">
                        <a class="uk-alert-close" uk-close></a>
                        <h3>Verified</h3>
                        <p>
                           {{Session::get('verified')}}
                        </p>
                    </div>
                    @endif
                    <div class="uk-card-body">
                        <div class="uk-text-center">
                            <img src="/images/icons/student.png" width="100" class="uk-border-circle border-default">
                        </div>
                        <form action="{{route('login')}}" method="POST">
                            @csrf
                            <div class="uk-margin">
                                <input required type="email" name="email" id="email" placeholder="Email" class="uk-input bottom-line">
                                @if($errors->has('email'))
                                    <span class="text-red">{{$errors->first('email')}}</span>
                                @endif
                            </div>
                            <div class="uk-margin">
                                <input required type="password" name="password" placeholder="*******" class="uk-input bottom-line">
                                @if($errors->has('password'))
                                    <span class="text-red">{{$errors->first('password')}}</span>
                                @endif
                            </div>
                            <p>
                                <a href="/password/reset">Forget Password?</a>
                            </p>
                            <button type="submit" class="uk-button uk-button-primary uk-border-rounded text-300 uk-width-1-1">Login</button>
                        </form>
                        @include('auth.social')
                    </div>
                    <div class="uk-card-footer">
                        <a href="/register">Create New Account</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
