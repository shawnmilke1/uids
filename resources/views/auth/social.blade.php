@section('fonts')
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
@endsection
<div class="uk-text-center uk-margin">
    <div class="uk-text-small">Or Sign in With</div>
    <p uk-margin>
        <a href="/provider/google" class="uk-button google uk-border-rounded box-shadow-default">
            <span class="uk-icon uk-icon-image" style="background-image: url('https://assets.gitlab-static.net/assets/auth_buttons/google_64-9ab7462cd2115e11f80171018d8c39bd493fc375e83202fbb6d37a487ad01908.png');width:18px;height:18px;"></span>
            Google</a>
            <a href="/provider/facebook" class="uk-button google uk-border-rounded box-shadow-default">
                <span uk-icon="facebook"></span>
                facebook
            </a>
                   
    </p>
  

</div>