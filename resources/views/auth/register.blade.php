@extends('layouts.app')
@section('title','Register new Account')
@section('content')
<section class="uk-section uk-section-small bg-grey-lightest">
    <div class="uk-container">
        <div class="uk-child-width-1-2@m uk-flex-center uk-grid">
            <div>
                <div class="uk-card uk-card-large uk-card-default uk-border-rounded">
                    @if(Session::has('registered'))
                    <div uk-alert class="uk-margin-remove-bottom">
                        <a class="uk-alert-close" uk-close></a>
                        <h3>Sukses</h3>
                        <p>
                            Pendaftaran Berhasil, namun akun anda belum dapat digunakan, silahkan cek email yang anda daftarkan dan lakukan konfirmasi
                            email yang kami kirimkan. Saat ini email akan masuk sekitar 1 sampai 2 jam, kami harap anda dapat menunggu konfirmasi email tersebut.
                        </p>
                    </div>
                    @endif
                    <div class="uk-card-body">
                        <h3 class="uk-text-center">Create Account</h3>
                        <form action="{{route('register')}}" method="POST">
                            @csrf
                            <div class="uk-child-width-1-2@m uk-margin uk-grid">
                                <div>
                                    <input type="text" name="first_name" required value="{{old('first_name')}}" placeholder="Your First Name" class="uk-input bottom-line">
                                    {!! if_error($errors,'first_name','text-red') !!}
                                </div>
                                <div>
                                    <input type="text" name="last_name" required placeholder="Your Last Name" value="{{old('last_name')}}" class="uk-input bottom-line">
                                    {!! if_error($errors,'last_name','text-red') !!}
                                </div>
                            </div>
                            <div class="uk-margin">
                                <input type="text" required name="username" id="name" value="{{old('username')}}" placeholder="Your UserName" class="uk-input bottom-line">
                                {!! if_error($errors,'username','text-red') !!}
                            </div>
                            <div class="uk-margin">
                                <input required type="email" name="email" id="email" value="{{old('email')}}" placeholder="Email" class="uk-input bottom-line">
                                {!! if_error($errors,'email','text-red') !!}
                            </div>
                            <div class="uk-child-width-1-2@m uk-margin uk-grid">
                                <div>
                                    <input required type="password" name="password" placeholder="Password" class="uk-input bottom-line">
                                    {!! if_error($errors,'password','text-red') !!}
                                </div>
                                <div>
                                    <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password" class="uk-input bottom-line">
                                </div>
                            </div>
                            <div class="uk-margin">
                                <a href="{{route('password.request')}}">Forget Password?</a>
                            </div>
                            <button type="submit" class="uk-button uk-button-primary uk-border-rounded text-300 uk-width-1-1">Register</button>
                        </form>
                        <div class="uk-text-center uk-margin">
                            @include('auth.social')
                        </div>
                    </div>
                    <div class="uk-card-footer">
                        <a href="/login">Already Have an Account?</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection
