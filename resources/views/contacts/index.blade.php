@extends('layouts.app')
@section('title','Hubungi Kami, Unstags')
@section('ogtitle','Hubungi Kami, Unstags')
@section('desc','Hubungi kami terkait dengan hal-hal yang tidak terdapat pada pusat bantuan')
@section('ogdesc','Hubungi kami terkait dengan hal-hal yang tidak terdapat pada pusat bantuan')
@section('keywords','Contact Us, Hubunngi kami, Unstags Help Center')
@section('content')
    <div class="uk-child-width-1-2@m uk-grid-collapse" uk-grid>
        <div class="uk-tile uk-padding uk-text-left@m uk-text-center">
            <h2 class="uk-text-bold ">Hubungi Kami</h2>
            <div class="uk-margin">
                <div class="uk-text-muted">Email</div>
                <div class="uk-text-bold">support@unstags.com</div>
            </div>
            <div class="uk-margin">
                <div class="uk-text-muted">Phone (whats app only)</div>
                <div class="uk-text-bold"><a href="https://wa.me/6283811251175?text=Hallo%20Michael">+6283811251175</a> (Michael Lazuardy)</div>
                <div class="uk-text-bold"><a href="https://wa.me/6289681414890?text=Hallo%20Arin">+6289681414890</a> (Arin Dwi W)</div>
            </div>
            <div class="uk-margin">
                <div class="uk-text-muted">Alamat</div>
                <div>Nomaden</div>
            </div>
        </div>
        <div class="uk-tile uk-padding uk-visible@m bg-blue-dark uk-text-center">
            <img src="/images/icons/social-media.svg" width="300" alt="Contact Us">
        </div>
    </div>
    <section class="uk-section bg-grey-lighter">
        <div class="uk-container uk-container-small">
            <div class="uk-card uk-border-rounded uk-card-body uk-card-default box-shadow-default border-dark-left">
                <form action="{{route('contact.store')}}" method="POST">
                    @csrf
                    <div class="uk-child-width-1-2@m" uk-grid>
                        <div>
                            <label for="first_name" class="uk-form-label">Nama Depan</label>
                            <input type="text" name="first_name" value="{{old('first_name')}}" class="uk-input grey uk-border-rounded">
                            {!! if_error($errors,'first_name','text-red') !!}
                        </div>
                        <div>
                            <label for="first_name" class="uk-form-label">Nama Belakang</label>
                            <input type="text" name="last_name" value="{{old('last_name')}}" class="uk-input grey uk-border-rounded">
                            {!! if_error($errors,'last_name','text-red') !!}
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label for="email" class="uk-form-label">Email</label>
                        <input type="email" name="email" value="{{old('email')}}" class="uk-input grey uk-border-rounded">
                        {!! if_error($errors,'email','text-red') !!}
                    </div>
                    <div class="uk-margin">
                        <label for="subject" class="uk-form-label">Subyek</label>
                        <input type="text" name="subject" placeholder="Ringkasan masalah anda" value="{{old('subject')}}" class="uk-border-rounded uk-input grey">
                        {!! if_error($errors,'subject','text-red') !!}
                    </div>
                    <div class="uk-margin">
                        <label for="message" class="uk-form-label">Pesan</label>
                        <textarea name="message" placeholder="Sebutkan dengan jelas pesan anda" cols="30" rows="10" class="uk-border-rounded uk-textarea grey">{{old('message')}}</textarea>
                        {!! if_error($errors,'message','text-red') !!}
                    </div>
                    <div class="uk-margin">
                        <button type="submit" class="uk-button uk-button-primary border-80">Submit Pesan</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <section class="uk-section uk-section-xsmall">
        <div class="uk-container">
            
        </div>
    </section>
@endsection