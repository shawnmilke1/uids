@extends('layouts.app') 
@section('title',$org->name.' Members') 
@section('ogtitle',$org->name. ' Members') 
@section('desc',$org->description) 
@section('ogdesc',$org->description)
@section('content')
@include('organizers.header')
@include('organizers.tab')
<section class="uk-section bg-grey-lighter">
    <div class="uk-container uk-container-small">
        <h3 class="border-bottom-title uk-text-center">Members</h3>
        <div class="uk-margin-medium-top">
            @foreach ($members as $member)
                <div class="uk-card uk-card-default uk-card-body uk-margin-small-bottom box-shadow-default">
                    <div uk-grid>
                        <div class="uk-text-left@m uk-text-center uk-width-1-4@m">
                            <img class="uk-border-circle" alt="{{$member->user->fullName()}}" src="{{gravatar()->image($member->user->email)->size(150)}}">
                        </div>
                        <div class="uk-width-3-4@m uk-text-left@m uk-text-center">
                            <h4 class="uk-text-bold">{{$member->user->fullName()}} @if($member->user_id == $org->user_id) <span class="uk-badge">Leader</span>@endif</h4>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endsection