@php $create = Request::is('@/organizers/create'); 
@endphp
<div class="uk-text-center">
    <h2 class="uk-margin-remove">Event Organizer</h2>
    <p class="uk-margin-remove">{{Request::is('@/organizers/create') ? 'Create an Event Organizer Profile' : 'Edit Event Organizer' }}</p>
</div>
@if(Session::has('need_eo'))
<div class="uk-alert-warning" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    <p>{{Session::get('need_eo')}}</p>
</div>
@endif @if(Session::has('success'))
<div class="uk-placeholder">
    <p> <span uk-icon="check"></span> Event Organizer Created, Continue <a href="{{route('event.create')}}">Create Event</a>        or add new Event Organizer below</p>
</div>
@endif
<form action="{{$create ? route('organizer.store') : route('organizer.update',$organizer->id)}}" method="post" enctype="multipart/form-data">
    @if(!$create) @method("PATCH") @endif @csrf
    <div uk-grid class="uk-margin-medium-top">
        <div class="uk-width-1-4@m uk-text-center">
            <div class="uk-card uk-card-small uk-card-default box-shadow-b-2">
                @if($organizer->image == null)
                <div class="uk-card-body">
                    <span uk-icon="icon:user;ratio:5;"></span>
                </div>
                @else
                <div class="uk-card-media-top">
                    <img src="{{asset('storage/'.$organizer->image)}}" alt="{{$organizer->organizer_name}}">
                </div>
                @endif
            </div>
            <p>
                jpeg, jpg, png, gif. Tidak Lebih dari 1MB
            </p>
            <div class="uk-margin">
                <div uk-form-custom="target: true">
                    <input type="file" name="image">
                    <input class="uk-input uk-form-width-large" type="text" placeholder="Select Image" disabled>
                </div>
            </div>
            @if ($errors->has('image'))
            <span class="uk-text-danger">{{$errors->first('image')}}</span> @endif
        </div>
        <div class="uk-width-3-4@m">
            <div>
                <div class="uk-card uk-card-body uk-card-default box-shadow-b-2">
                    <div class="uk-margin">
                        <label for="organizer_name">Organizer Name</label>
                        <input type="text" name="name" value="{{$create ? old('name') : old('name',$organizer->name)}}"
                            class="uk-input">
                        {!! if_error($errors,'name','text-red') !!}
                    </div>
                    <div class="uk-margin">
                        <label for="slug">Organizer Username</label>
                        <input type="text" name="slug" value="{{old('slug',$organizer->slug)}}" class="uk-input">
                        {!! if_error($errors,'slug','text-red') !!}
                        <span class="uk-text-muted uk-text-small uk-display-block">
                            Nama unik untuk mengidentifikasi organisasi anda, buat sesingkat mungkin.minimal 3 huruf.
                        </span>
                    </div>
                    <div class="uk-margin">
                        <label for="description">Organizer Description</label>
                        <textarea name="description" cols="30" rows="10" class="uk-textarea">{{$create ? old('description') : old('description',$organizer->description) }}</textarea>
                        {!! if_error($errors,'description','text-red') !!}
                    </div>
                    <div class="uk-margin">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{old('email',$organizer->email)}}" class="uk-input">
                        {!! if_error($errors,'email','text-red') !!}
                    </div>
                    <div class="uk-margin">
                        <h3>Social Network (*Optional)</h3>
                    </div>
                    <div class="uk-child-width-1-2@m" uk-grid>
                        <div>
                            <div class="uk-margin">
                                <label for="facebook">Facebook Page</label>
                                <div class="uk-inline uk-width-1-1">
                                    <span class="uk-form-icon" uk-icon="icon: facebook"></span>
                                    <input class="uk-input" value="{{$create ? old('facebook') : old('facebook',$organizer->facebook)}}" name="facebook" placeholder="shawn.milke or else">
                                    {!! if_error($errors,'facebook','text-red') !!}
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-margin">
                                <label for="twitter">Twitter Page</label>
                                <div class="uk-inline uk-width-1-1">
                                    <span class="uk-form-icon" uk-icon="icon: twitter"></span>
                                    <input class="uk-input" value="{{$create ? old('twitter') : old('twitter',$organizer->twitter)}}" name="twitter" placeholder="johndoe, do not use @">
                                    {!! if_error($errors,'twitter','text-red') !!}
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-margin">
                                <label for="instagram">Instagram Page</label>
                                <div class="uk-inline uk-width-1-1">
                                    <span class="uk-form-icon" uk-icon="icon: instagram"></span>
                                    <input class="uk-input" value="{{$create ? old('instagram') : old('instagram',$organizer->instagram)}}" name="instagram"
                                        placeholder="your_ig_username">
                                    {!! if_error($errors,'instagram','text-red') !!}
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="uk-margin">
                                <label for="website">Phone</label>
                                <div class="uk-inline uk-width-1-1">
                                    <span class="uk-form-icon" uk-icon="icon: phone"></span>
                                    <input class="uk-input" value="{{$create ? old('phone') : old('phone',$organizer->phone)}}" name="phone" placeholder="start with + or 0">
                                </div>
                                @if ($errors->has('phone'))
                                <span class="uk-text-warning">{{$errors->first('phone')}}</span> @endif
                            </div>
                        </div>
                    </div>
                    <div class="uk-margin">
                        <label for="website">Website</label>
                        <div class="uk-inline uk-width-1-1">
                            <span class="uk-form-icon" uk-icon="icon: world"></span>
                            <input class="uk-input" value="{{$create ? old('website') : old('website',$organizer->website)}}" name="website" placeholder="start with https">
                        </div>
                    </div>
                    <div class="uk-margin">
                        <button type="submit" class="uk-button uk-button-primary">{{$create ? 'Save' : 'Update' }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>