@extends('layouts.app')
@section('title','Create Organizer')
@section('content')
<div class="uk-section bg-grey-lighter">
    <div class="uk-container">
        @include('organizers.form')
    </div>
</div>
@endsection