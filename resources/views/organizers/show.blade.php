@extends('layouts.app')
@section('title',$org->name)
@section('ogtitle',$org->name)
@section('desc',$org->description)
@section('ogdesc',$org->description)
@section('content')
    @include('organizers.header')
    @include('organizers.tab')
    <section class="uk-section bg-grey-lighter">
        <div class="uk-container uk-container-small">
            <div class="uk-card uk-card-default box-shadow-default">
                <div class="uk-card-header">
                    <h3 class="uk-text-center uk-text-bold">Tentang {{$org->name}}</h3>
                </div>
                <div class="uk-card-body">
                    <p>
                        {{$org->description}}
                    </p>
                </div>
            </div>
            <div class="uk-card uk-margin-medium-top uk-card-default box-shadow-default">
                <div class="uk-card-header">
                    <h3 class="uk-text-center uk-text-bold">Acara Mendatang</h3>
                </div>
                <div class="uk-card-body">
                    @forelse ($events as $event)
                    <a href="{{route('event.show',$event->slug)}}" class="ui-card-link">
                        <div class="uk-card uk-card-default uk-card-small uk-grid-collapse uk-margin" uk-grid>
                            <div class="uk-card-media-left uk-width-1-3@m uk-cover-container">
                                <img src="{{asset('storage/'.$event->image)}}" alt="" uk-cover>
                                <canvas width="400" height="200"></canvas>
                            </div>
                            <div class="uk-width-2-3@m">
                                <div class="uk-card-body">
                                    <h3 class="uk-card-title">{{$event->title}}</h3>
                                    <div class="description">
                                        <p class="uk-text-muted uk-text-small">{{$event->city->name}}</p>
                                        <p>
                                            {{parseDate($event->starts_date)->format('d M, Y')}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @empty
                    <div class="uk-text-center">
                        <img src="/images/icons/event2.svg" alt="No Events" width="100">
                        <h3 class="uk-text-bold">Tidak Ada Acara</h3>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </section>
@endsection