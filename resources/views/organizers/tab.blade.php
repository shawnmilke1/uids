<ul class="uk-tab uk-flex-center organizer-tab uk-margin-remove-top uk-margin-remove-right uk-margin-remove-bottom" style="flex-wrap: nowrap;overflow:auto">
    <li {{isActive('organizers/'.$org->slug)}} >
        <a href="{{route('organizer.show',$org->slug)}}">Dashboard</a>
    </li>
    <li {{isActive('organizers/'.$org->slug.'/members')}}>
        <a href="{{route('organizer.member',$org->slug)}}">Members</a>
    </li>
</ul>