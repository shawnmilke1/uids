@extends('layouts.app')
@section('title','Choose Organizer')
@section('content')
    <section class="uk-section">
        <div class="uk-container">
            <div class="uk-text-center">
                <h3>Pilih Organisasi</h3>
                <div class="uk-position-relative uk-visible-toggle" tabindex="-1" uk-slider>
                
                    <ul class="uk-slider-items uk-child-width-1-4@m">
                       @foreach ($organizers as $organizer)
                           <li>
                                <img src="{{asset($organizer->image == null ? 'images/icons/student.png' : 'storage/'.$organizer->image)}}" alt="{{$organizer->name}}">
                                <div class="uk-position-center uk-panel">
                                    <a href="{{url('/o/'.$organizer->slug)}}" class="uk-button uk-button-primary">Go To Dashboard</a>
                                </div>
                                <h2>{{$organizer->name}}</h2>
                            </li>
                       @endforeach
                       
                    </ul>
                
                    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
                    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
                
                </div>
            </div>
        </div>
    </section>
@endsection