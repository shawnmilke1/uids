@extends('auth.settings.layout')
@section('setting-title','Organizers')
@section('title','Organizer Profile')
@section('setting-content')
    <div>
        <div class="uk-text-center uk-margin-small-bottom">
            <a href="{{route('organizer.create')}}" class="uk-button uk-button-primary">Create New Organizer</a>
        </div>
        <div class="uk-child-width-1-3@m" uk-grid>
            @forelse ($orgs as $organizer)
                <div>
                    <div class="uk-card uk-card-default">
                        <div class="uk-card-media-top">
                            <img src="{{$organizer->image == null ? '/images/icons/student.png' : asset('storage/'.$organizer->image) }}" alt="{{$organizer->name}}">
                        </div>
                        <div class="uk-card-body">
                            <h3 class="uk-card-title">{{$organizer->name}}</h3>
                        </div>
                        <div class="uk-card-footer">
                            @if($user->id == $organizer->user_id)
                            <a href="{{route('organizer.edit',$organizer->id)}}" class="uk-button uk-button-text">Edit</a>
                            @endif
                            <a href="{{route('organizer.show',$organizer->slug)}}" class="uk-button uk-button-text uk-align-right">Detail</a>
                        </div>
                    </div>
                </div>
            @empty
                <div class="uk-width-expand">
                    <div class="uk-text-center">
                        No Organizers Created
                    </div>
                </div>
            @endforelse
        </div>
    </div>
@endsection