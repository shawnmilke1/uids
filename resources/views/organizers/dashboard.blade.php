<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Unkindled Idea</title>
    {{-- Fonts --}}
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    {{-- Css --}}
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <link rel="stylesheet" href="/vendor/admin/modules/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/vendor/admin/modules/fontawesome/css/all.min.css">
    {{-- Template --}}
    <link rel="stylesheet" href="/vendor/admin/css/style.css">
    <link rel="stylesheet" href="/vendor/admin/css/components.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" href="/vendor/admin/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
    @php
        $user = Auth::user();
        $role = $org->members->where('user_id',$user->id)->first();
    @endphp
    <script>
        window.App = {!! json_encode([
                'csrfToken' => csrf_token(),
                'user' => Auth::user(),
                'signedIn' => Auth::check(),
                'org' => $org,
                'role' => $role->role->slug
            ]) !!};
    </script>
</head>

<body>
    <div id="undigs"></div>
    <form method="POST" id="form-logout" style="display: none;" action="{{route('logout')}}">
        @csrf
    </form>
    <!--  
    -----------------------
    What Are You Doing Here?
    -----------------------
    -->
    <script src="{{mix('js/undigs.js')}}"></script>
    <script src="{{mix('js/manifest.js')}}"></script>
    <script src="{{mix('js/vendor.js')}}"></script>
    <script src="/vendor/admin/modules/popper.js"></script>
    <script src="/vendor/admin/modules/tooltip.js"></script>
    <script src="/vendor/admin/modules/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/admin/modules/nicescroll/jquery.nicescroll.min.js""></script>
    <script src="/vendor/admin/js/stisla.js"></script>
    @env('production')
    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/js/uikit.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/js/uikit-icons.min.js"></script>
    @else
    <script src="{{asset('js/unstags.js')}}"></script>
    <script src="{{asset('js/unstags-icons.js')}}"></script>
    @endenv
    {{-- Custom --}}
    <script src="/vendor/admin/js/scripts.js"></script>
    <script src="/vendor/admin/js/custom.js"></script>
    <script src="/vendor/admin/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
</body>

</html>