@extends('auth.settings.layout')
@section('title','Edit Organizer Profile')
@section('setting-title','Edit Organizer Profile')
@section('setting-content')
    @include('organizers.form')
@endsection