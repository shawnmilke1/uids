<section class="uk-section uk-section-xsmall uk-light bg-blue-darker">
    <div class="uk-container uk-container-small">
        <div class="uk-flex-middle" uk-grid>
            <div class="uk-width-2-3@m">
                <h2>{{$org->name}}</h2>
            </div>
            <div class="uk-width-1-3@m">
                <img width="150" src="{{asset($org->image == null ? 'images/icons/student.png' : 'storage/'.$org->image)}}">
            </div>
        </div>
    </div>
</section>