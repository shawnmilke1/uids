@extends('layouts.help-type-layouts')
@section('title',"{$help->title} in Unstags")
@section('desc',str_limit(strip_tags($help->body)),200)

@section('help-header')
    <h1>{{$help->title}}</h1>
@endsection
@section('help-content')
    <div class="help-content">
        {!! $help->body !!}
    </div>
@endsection