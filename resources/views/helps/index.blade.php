@extends('layouts.app')
@section('title','Unstags Help Center')
@section('desc','Pelajari Lebih lanjut cara menggunakan website unstags untuk membuat acara organisasi anda')
@section('keywords','unstags help center, unstags getting started')
@section('content')
    <section class="uk-section bg-blue-darkest uk-light">
        <div class="uk-container">
            <h1 class="border-bottom-title uk-text-center">Unstags Help Center</h1>
        </div>
    </section>
    <section class="uk-section">
        <div class="uk-container">
            <h2 class="uk-text-center">How Unstags Can Help You ?</h2>
            <div class="help-card uk-margin-small-top">
                <div class="uk-child-width-1-3@m" uk-grid>
                    @foreach ($types as $type)
                        <div>
                            <a href="{{route('ht.show',$type->slug)}}" class="uk-card uk-card-small uk-card-hover uk-card-body uk-display-block ui-card-link border-default box-shadow-default uk-text-center">
                                <img src="{{asset('/images/icons/help/'.$type->icon)}}" alt="{{$type->name}}" width="100">
                                <h3 class="uk-margin-small-top">{{$type->name}}</h3>
                                <p>{{$type->description}}</p>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection