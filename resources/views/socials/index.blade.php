@extends('auth.settings.layout') 
@section('setting-title','Connections') 
@section('title','Connections') 
@section('setting-content')
<div>
    @if($user->password != null)
    <div class="uk-grid-small uk-flex-middle" uk-grid>
        <div class="uk-width-expand@m">
            <h3 class="uk-card-title uk-margin-remove-bottom uk-text-bold">Connect to Google</h3>
            <p class="uk-text-meta uk-margin-remove-top">
                Anda dapat menggunakan akun Google anda untuk Log in ke Unstags
            </p>
        </div>
        <div class="uk-width-auto@m">
            @if($socials->where('provider','google')->count() > 0)
                <form action="{{route('social.destroy',$socials->where('provider','google')->first()->id)}}" method="POST">
                    @method("DELETE")
                    @csrf
                    <button type="submit" class="uk-button uk-button-secondary uk-border-rounded">Disconnect</button>
                </form>
            @else
                <a href="/provider/google" class="uk-button uk-button-danger"><span uk-icon="google"></span> Connect to Google</a>
            @endif
        </div>
    </div>
    <hr>
    <div class="uk-grid-small uk-flex-middle" uk-grid>
        <div class="uk-width-expand@m">
            <h3 class="uk-card-title uk-margin-remove-bottom uk-text-bold">Connect to Facebook</h3>
            <p class="uk-text-meta uk-margin-remove-top">
                Anda dapat menggunakan akun Facebook anda untuk Log in ke Unstags
            </p>
        </div>
        <div class="uk-width-auto@m">
            @if($socials->where('provider','facebook')->count() > 0)
            <form action="{{route('social.destroy',$socials->where('provider','facebook')->first()->id)}}" method="POST">
                @method("DELETE")
                @csrf
                <button type="submit" class="uk-button uk-button-secondary uk-border-rounded">Disconnect</button>
            </form>
            @else
            <a href="/provider/facebook" class="uk-button uk-button-primary"><span uk-icon="facebook"></span>Connect to Facebook</a> @endif
        </div>
    </div>
    @else
        <div class="uk-text-center">
            <p>Tolong atur <a href="/@/password">Password</a> anda terlebih dahulu sebelum mengatur Koneksi Media Sosial.</p>
        </div>
    @endif
</div>
@endsection