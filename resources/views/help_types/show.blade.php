@extends('layouts.help-type-layouts')
@section('title',"{$type->name} in Unstags")
@section('desc',$type->description)

@section('help-header')
    <h1>{{$type->name}}</h1>
@endsection
@section('help-content')
    <div class="help-content">
        @foreach ($type->helps as $help)
        <ul class="uk-list uk-list-striped">
            <li>
                <a href="{{route('help.show',['type' => $help->type->slug,'slug' => $help->slug])}}" class="uk-display-block">
                {{$help->title}}
                </a>
            </li>
        </ul>
           
        @endforeach
    </div>
@endsection