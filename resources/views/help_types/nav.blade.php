<ul class="uk-nav uk-nav-default">
    @foreach ($types as $type)
        <li>
            <a href="{{route('ht.show',$type->slug)}}">{{$type->name}}</a>
        </li>
    @endforeach
</ul>