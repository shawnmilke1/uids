@component('mail::message')
## Dear {{$user->username}}

{{Illuminate\Mail\Markdown::parse($info->body)}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
