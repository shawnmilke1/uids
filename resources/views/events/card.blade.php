<section class="uk-section bg-grey-lighter">
    <div class="uk-container">
        <div class="uk-child-width-1-3@m" uk-grid>
            @forelse ($events as $event)
            <div>
                <a href="{{route('event.show',$event->slug)}}" class="ui-card-link">
                    <div class="uk-card uk-card-default uk-card-small">
                        <div class="uk-card-media-top uk-cover-container">
                            <img data-src="{{asset('storage/'.$event->image)}}" alt="{{$event->title}}" uk-img uk-cover>
                            <canvas width="500" height="250"></canvas>
                        </div>
                        <div class="uk-card-body">
                            <div class="uk-card-badge uk-label">
                                <span class="uk-text-bold uk-text-small">
                                    {{parseDate($event->starts_date)->format('d M')}}
                                </span>
                            </div>
                            <h3 class="size-16 uk-margin-remove-top">{{$event->title}}</h3>
                            <p class="uk-text-small">{{$event->city->name}}</p>
                            <div class="uk-child-width-1-2@m uk-flex-middle uk-grid-small" uk-grid>
                                <div class="uk-text-left@m uk-text-center">
                                    <span class="text-red uk-text-bold">
                                        @if($event->tickets()->where('price','free')->get()->count() > 0)
                                        Free
                                        @elseif(!$event->tickets()->exists())
                                        Tanpa Tiket
                                        @else
                                        No Free
                                        @endif
                                    </span>
                                </div>
                                <div class="uk-text-right@m uk-text-center">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            @empty
            <div class="uk-width-expand uk-flex uk-flex-center uk-text-center">
                <div>
                    <img src="/images/icons/event2.svg" width="100" alt="Tidak ada event ditemukan">
                    <div class="uk-text-bold uk-margin">Tidak ada acara</div>
                    <a href="{{route('event.create')}}" class="uk-button uk-button-primary border-80">Buat Acara</a>
                </div>
            </div>
            @endforelse
        </div>
        <div class="uk-text-center uk-margin-medium-top">
            {{$events->links('vendor.pagination.default')}}
        </div>
    </div>
</section>