@php
$org = $event->organizer;
@endphp
@if($org->website != null)
<a href="{{$org->website}}" target="_blank" class="uk-icon-button" uk-icon="world"></a>
@endif
@if($org->facebook != null)
<a href="https://facebook.com/{{$org->facebook}}" class="uk-icon-button" target="_blank" uk-icon="facebook"></a>
@endif
@if($org->instagram != null)
<a href="https://instagram.com/{{$org->instagram}}" class="uk-icon-button" target="_blank" uk-icon="instagram"></a>
@endif
@if($org->phone != null)
<a href="tel:{{$org->phone}}" class="uk-icon-button" target="_blank" uk-icon="receiver"></a>
@endif