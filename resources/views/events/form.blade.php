<div class="uk-container uk-container-small uk-margin-large-bottom">
    <h2 class="uk-text-bold uk-text-center">First Step Is Create Your Event</h2>
    <div class="uk-card uk-card-default border-default">
        @php $create = Request::is('events/create'); 
        @endphp
        @if($create)
        <form action="{{route('event.store')}}" method="POST" id="event-submit" enctype="multipart/form-data">
        @else
        <form action="{{route('event.update',$event->id)}}" method="POST" id="event-submit" enctype="multipart/form-data">
        @method("PATCH")
        @endif
            @csrf
            <div id="img-container" class="uk-card-media-top uk-cover-container card-img-rounded" {{Request::is( 'events/create') ?
                'hidden' : ''}}>
                <img id="imgimg" src="{{$create ?  '' : asset('storage/'.$event->image)}}" uk-cover>
                <canvas width="750" height="375"></canvas>
                <div class="uk-position-bottom-right uk-overlay uk-overlay-primary">
                    <a id="remove-button" uk-tooltip="Ingin Hapus dan Ganti Cover Photo?" class="uk-icon-button" uk-icon="trash"></a>
                </div>
            </div>
            <div class="uk-card-body">
                <div id="uploader" class="js-upload uk-placeholder uk-text-center">
                    <span uk-icon="icon: cloud-upload"></span>
                    <div uk-form-custom>
                        <input type="file" name="image" id="input-image">
                        <span class="uk-link">Pilih Gambar Ukuran 960 x 480px</span>
                        <span>Atau 2:1 , selain dari ukuran itu, gambar akan di crop :)</span>
                    </div>
                </div>
                {!! if_error($errors,'image','text-red') !!}
                <div class="uk-margin">
                    <label for="title" class="uk-form-label">Nama / Judul Acara</label>
                    <input type="text" name="title" placeholder="Create a good looking title" value="{{old('title',$event->title)}}" class="uk-input bottom-line uk-form-large">
                    {!! if_error($errors,'title','text-red') !!}
                </div>
                <div class="uk-child-width-1-2@m" uk-grid>
                    <div>
                        <label for="organizer">Organizer</label>
                        <select name="organizer_id" class="uk-select bottom-line">
                            @foreach ($user->organizers as $organizer)
                               @if(Request::is('events/create'))
                            <option value="{{$organizer->id}}" {{old( 'organizer_id') == $organizer->id ? 'selected' : '' }} >{{$organizer->name}}</option>
                            @else
                            <option value="{{$organizer->id}}" {{$event->organizer->id == $organizer->id || $organizer->id == old('organizer_id') ? 'selected' :''}} >{{$organizer->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <label for="event_type">Tipe Event</label>
                        <select name="event_type_id" id="event_type" uk-tooltip="Event Type membantu pengunjung mengetahui tipe acara yang kamu buat"
                            class="uk-select bottom-line">
                            @foreach ($types as $type)
                                @if(Request::is('events/create'))
                                <option value="{{$type->id}}" {{old('event_type_id') == $type->id ? 'selected' : '' }} >{{$type->name}}</option>
                                @else
                                <option value="{{$type->id}}" {{$event->event_type_id == $type->id || $type->id == old('event_type_id')  ? 'selected' :''}} >{{$type->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="uk-child-width-1-4@m uk-child-width-1-2@s" uk-grid>
                    <div>
                        <label for="starts_date">Tanggal Mulai <span uk-icon="icon:info;ratio:1" uk-tooltip="+4 day from now"></span></label>
                        <input name="starts_date" type="date" class="uk-input" value="{{$create ? old('starts_date',now()->format('Y-m-d')) : old('starts_date',$event->starts_date) }}"
                            id="starts_date"> 
                        {!! if_error($errors,'starts_date','text-red') !!}
                    </div>
                    <div>
                        <label for="ends_date">Tanggal Selesai</label>
                        <input name="ends_date" type="text" value="{{$create ? old('ends_date',now()->format('Y-m-d')) : old('ends_date',$event->ends_date) }}" class="uk-input"
                            id="ends_date" {{Request::is( 'event/create') ? 'disabled' : ''}}> 
                        {!! if_error($errors,'ends_date','text-red') !!}
                    </div>
                    <div>
                        <label for="starts_time">Jam Mulai</label>
                        <input name="starts_time" type="text" value="{{$create ? old('starts_time',parseDate(date('H:i'))->format('H:i'))  : old('starts_time',$event->starts_time) }}"
                            class="uk-input" id="starts_time"> 
                        {!! if_error($errors,'starts_time') !!}
                    </div>
                    <div>
                        <label for="ends">Jam Selesai</label>
                        <input name="ends_time" type="text" value="{{$create ? old('ends_time',parseDate(date('H:i'))->addHour()->format('H:i')) : old('ends_time',$event->ends_time) }}" class="uk-input"
                            id="ends_time"> 
                        @if($errors->has('ends_time'))
                        <span class="uk-text-danger">{{$errors->first('ends_time')}}</span> 
                        @endif
                    </div>
                </div>
                <div class="uk-child-width-1-2@m" uk-grid>
                    <div>
                        <label for="city" class="uk-display-block">Kota</label>
                        <select class="uk-input" name="city_id" id="city-select">
                            @if(!$create)
                                <option value="{{$event->city_id}}">{{$event->city->name}}</option>
                            @endif
                        </select>
                        {!! if_error($errors,'city_id','text-red') !!}
                    </div>
                    <div>
                        <label for="gmap_url">Google Map Url</label>
                        <input type="text" name="gmap_url" placeholder="Google Map Url" value="{{old('gmap_url',$event->gmap_url)}}" class="uk-input">
                        {!! if_error($errors,'gmap_url','text-red') !!}
                        <p class="uk-text-small">
                            Jika anda kesulitan atau tidak mengetahui apa itu gmap_url, silahkan kunjungi <a href="https://unstags.com/help/organizer/link-google-maps-pada-acara-anda">Cara Memasukkan Link Google Map</a>
                        </p>
                    </div>
                </div>
                <div class="uk-margin">
                    <label for="address">Alamat</label>
                    <textarea name="address" placeholder="Alamat Tempat acara anda dilaksanakan" cols="30" rows="5" class="uk-textarea">{{old('address',$event->address)}}</textarea>
                    {!! if_error($errors,'address','text-red') !!}
                </div>
                <div class="uk-margin">
                    <label for="description">Description</label>
                    <textarea id="editor" name="description" rows="30">{{old('description',$event->description)}}</textarea>
                    {!! if_error($errors,'description','text-red') !!}
                </div>
            </div>
        </form>
    </div>
</div>
<div class="fixed-submit-button bg-white">
    <div class="uk-container">
        <div class="uk-flex-middle uk-grid-stack" uk-grid>
            <div class="uk-width-1-2 uk-visible@m">
                <p>Harap Cek Kembali Form Anda Sebelum melakukan Submit</p>
            </div>
            <div class="uk-width-1-2@m uk-text-right">
                <a href="{{route($create ? 'event.store' : 'event.update',$event->id)}}" onclick="event.preventDefault();document.getElementById('event-submit').submit();" class="uk-button uk-button-primary uk-width-1-2@m">Submit Event</a>
            </div>
        </div>
    </div>
</div>