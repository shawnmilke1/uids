@extends('layouts.app')
@section('title',$event->title)
@section('desc',strip_tags(str_limit($event->description,250)))
@section('keywords',"{$event->type->name}, "."Acara di {$event->city->name}")
@section('ogtitle',$event->title)
@section('ogimage',asset('storage/'.$event->image))
@section('ogdesc',strip_tags(str_limit($event->description,250)))
@section('content')
<div class="uk-section bg-black uk-background-blend-overlay uk-background-cover" style="background-image:url({{asset('storage/'.$event->image)}});">
    <div class="uk-container">
        <div class="uk-flex-middle" uk-grid>
            <div class="uk-width-2-3@m">
                <h2 class="text-white uk-text-bold">{{$event->title}}</h2>
                <p class="text-white uk-text-large">{{$event->city->name}}</p>
            </div>
            <div class="uk-width-1-3@m">
                <div class="uk-card uk-card-default uk-card-small uk-border-rounded">
                    <div class="uk-card-body">
                        <p uk-margin class="uk-text-center">
                            <a href="https://www.facebook.com/sharer/sharer.php?app_id=2184112911838313&sdk=joey&u={{url()->current()}}&display=popup&ref=plugin&src=share_button"
                                onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')" class="btn-social-dark facebook" uk-icon="facebook">
                            </a>
                            <a class="btn-social-dark twitter" href="https://twitter.com/share?text={{$event->title}}&amp;url={{url()->current()}}"
                                onclick="window.open(this.href, 'share-twitter', 'width=550,height=235');return false;" uk-icon="twitter">
                            </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="uk-section bg-grey-lighter">
    <div class="uk-container">
        <div class="uk-card uk-card-body uk-card-default">
            <div uk-grid class="uk-grid-small uk-grid-divider">
                <div class="uk-width-2-3@m">
                    <div class="body-of-event">
                        <h3>Event Description</h3>
                        <div>
                            {!! $event->description !!}
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3@m">
                    <div class="dnt uk-margin-medium-bottom">
                        <div class="uk-text-bold">Tanggal dan Waktu</div>
                        <p class="uk-margin-remove-bottom">
                            {{parseDate($event->starts_date)->format('l, d F, Y')}}
                        </p>
                        <p class="uk-margin-remove-top">
                            {{parseDate($event->ends_time)->format('H:i')}}
                        </p>
                    </div>
                    <div class="location">
                        <div class="uk-text-bold">Lokasi</div>
                        <p>
                            {{$event->address}}
                        </p>
                    </div>
                    @if($event->gmap_url != null)
                    <div class="gmap">
                        <div class="uk-text-bold">Google Map</div>
                        <p>
                            <a href="{{$event->gmap_url}}">Get Location</a>
                        </p>
                    </div>
                    @endif
                </div>
                <p uk-margin>
                    <a href="{{route('type.show',$event->type->slug)}}" class="uk-button uk-button-secondary border-80">{{$event->type->name}}</a>
                    <a href="{{route('city.show',$event->city->slug)}}" class="uk-button uk-button-secondary border-80">{{$event->city->name}}</a>
                </p>
            </div>    
        </div>
        <h3 class="uk-text-center uk-text-bold">Tiket</h3>
        @forelse ($tickets as $ticket)
        <div id="ticket-list" class="uk-card uk-margin-small-bottom uk-margin-small-top uk-card-small uk-card-default">
            <div class="uk-card-body">
                <div class="uk-child-width-1-2@m uk-flex-middle" uk-grid>
                    <div class="uk-text-left@m uk-text-center">
                        <h5 class="uk-text-bold">{{$ticket->name}}</h5>
                        <ul class="uk-list">
                            <li>E-Ticket, UnPaper</li>
                            <li>
                                {{$ticket->refundable ? 'Tiket Dapat dikembalikan' : 'Tiket tidak dapat dikembalikan'}}
                            </li>
                            <li class="uk-text-bold">
                                {{$ticket->qty - $ticket->ticket_sell}} Tiket Tersisa
                            </li>
                        </ul>
                    </div>
                    <div class="uk-text-right@m uk-text-center">
                        <div class="uk-display-inline uk-margin-small-right">
                            <span class="uk-text-bold text-red">{{$ticket->price == "free"? "Free" : "Rp ".number_format($ticket->price,0)}}</span>
                        </div>
                        <a href="#ticket-{{$ticket->id}}" class="uk-button uk-button-primary border-80" uk-toggle>Buy Ticket</a>
                        {{-- This is Ticket Modal --}}
                        <div id="ticket-{{$ticket->id}}" uk-modal>
                            <div class="uk-modal-dialog uk-modal-body">
                                <h2 class="uk-modal-title">{{$ticket->name}}</h2>
                                <form action="{{route('aud.create',$ticket->uuid)}}" method="GET">
                                    <div class="uk-margign">
                                        <label for="qty" class="uk-form-label">Total Peserta</label>
                                        <select name="qty" class="uk-select">
                                            @for ($i = 0; $i < $ticket->ticket_limit; $i++)
                                                <option value="{{$i+1}}">{{$i+1}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <p class="uk-text-right">
                                        <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                                        <button class="uk-button uk-button-primary" type="submit">Book Now</button>
                                    </p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-card-footer">
                <ul uk-accordion>
                    <li>
                        <a class="uk-accordion-title uk-text-bold" href="#">Detail</a>
                        <div class="uk-accordion-content">
                            <ul uk-tab="animation: uk-animation-slide-left-medium, uk-animation-slide-right-medium" style="flex-wrap: nowrap;overflow:auto">
                                <li>
                                    <a href="#">Tiket Description</a>
                                </li>
                                <li>
                                    <a href="#">Kebijakan Pembatalan</a>
                                </li>
                                <li><a href="#">Check In Method</a></li>
                            </ul>
                            
                            <ul class="uk-switcher uk-margin">
                                <li>
                                    {{$ticket->description ?? "Tidak ada deskripsi tiket" }}
                                </li>
                                <li>
                                    {{$ticket->cancel_policy ?? "Tidak ada Kebijakan Pembatalan"}}
                                </li>
                                <li>
                                    Tunjukkan tiket yang anda pesan kepada panitia acara yang dikirimkan ke email anda, lalu perlihatkan QR Code pada email tersebut.
                                    Anda tidak perlu mencetak tiket yang anda pesan, panitia akan melakukan Scan terkait validasi tiket anda. Tolong atur kecerahan layar smartphone anda 
                                    pada saat memperlihatkan QR Code.
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        @empty
        <h3 class="uk-text-center">Tiket Tidak Tersedia</h3>
        @endforelse
        <div class="uk-card uk-card-body uk-card-default box-shadow-default uk-card-small">
            <div class="uk-text-center">
                <img class="uk-border-circle" width="150" src="{{asset($event->organizer->image == null ? 'images/icons/student.png' : 'storage/'.$event->organizer->image)}}">
                <div class="border-bottom-title uk-margin-small-top uk-margin-medium-bottom">{{$event->organizer->name}}</div>
                <p>Organizer of {{$event->title}}</p>
                <p class="uk-margin">
                    <a href="{{route('organizer.show',$event->organizer->slug)}}" class="uk-button uk-button-primary border-80">Visit Organizer</a>
                    <button class="uk-button uk-button-secondary border-80" uk-toggle="target: #org-contact" type="button">Contact</button>
                    <p id="org-contact" uk-margin hidden>
                        @include('events.contact')
                    </p>
                </p>
            </div>
            <div class="uk-child-width-1-2@m uk-flex-center" uk-grid>
                <div>
                    <p>
                        {{$event->organizer->description}}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fixed-submit-button bg-white">
    <div class="uk-container">
        <div class="uk-flex-middle uk-grid-stack uk-grid-small" uk-grid>
            <div class="uk-width-2-3@m">
                <div class="uk-visible@m">
                    <div class="uk-text-bold">{{$event->title}}</div>
                    <div class="uk-text-small">{{parseDate($event->starts_date)->format('D')}}, {{parseDate($event->starts_date)->format('d F, Y')}}</div>
                </div>
                <div class="uk-hidden@m uk-text-center">
                    <span class="text-red uk-text-bold">
                        @if($event->tickets()->where('price','free')->get()->count() > 0)
                        Free
                        @elseif(!$event->tickets()->exists())
                        Tanpa Tiket
                        @else
                        No Free
                        @endif
                    </span>
                </div>
            </div>
            <div class="uk-width-1-3@m uk-text-right">
                <a href="#ticket-list" class="uk-button uk-button-primary uk-width-1-2@m border-80">Get Tiket</a>
            </div>
        </div>
    </div>
</div>
@endsection