@extends('layouts.app')
@section('title','Events')
@section('desc','Cari Acara yang dibuat Oleh mahasiswa, atau buat acara anda sendiri di Unstags')
@section('keywords','Unstags Event, Buat Acara Mahasiswa, Cari Acara yang Dibuat Mahasiswa, Acara Mahasiswa')
@section('content')
    <section class="uk-section uk-section-xsmall">
        <div class="uk-container">
            <div class="uk-child-width-1-2@m uk-flex-middle" uk-grid>
                <div>
                    <div class="uk-text-bold uk-text-large uk-text-left@m uk-text-center">Event</div>
                </div>
                <div class="uk-text-right@m uk-text-center">
                    <a href="{{route('event.create')}}" class="uk-button uk-button-primary border-80">Buat Acara</a>
                </div>
            </div>
        </div>
        <hr class="border-top-grey">
        <div class="uk-container uk-container-xsmall">
            <div class="uk-margin">
                <div class="uk-card uk-card-body uk-card-small uk-card-default box-shadow-default">
                    @php
                        $search = request()->has('search');
                        $type = request()->has('type');
                    @endphp
                    @if($search ||  $type)
                       <div class="uk-margin">
                           <div class="uk-align-right">
                               <a href="/events" class="uk-button-secondary uk-button border-80 uk-button-small">Reset Filter <span uk-icon="icon:close;ratio:0.7"></span></a>
                           </div>
                       </div>
                    @endif
                    <form class="uk-search uk-search-navbar uk-width-1-1">
                        <div class="uk-child-width-1-2@m uk-grid-divider uk-grid-small" uk-grid>
                            <div>
                                <label for="search" class="uk-form-label">Nama Event</label>
                                <input class="uk-input bottom-line" value="{{request('search')}}" type="text" name="search" placeholder="Cari Event">
                            </div>
                            <div>
                                <label for="type" class="uk-form-label">Tipe</label>
                                <select name="type" class="uk-select bottom-line">
                                    <option value="all">Semua Tipe</option>
                                    @foreach (\App\EventType::get() as $type)
                                        <option value="{{$type->slug}}" {{$type->slug == request('type') ? 'selected' : ''}} >{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-margin-small-top">
                            <button type="submit" class="uk-button uk-button-primary uk-border-rounded uk-width-1-1" uk-icon="search"></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    {{-- Section Listing Card --}}
    @include('events.card')
@endsection