@extends('layouts.app')
@section('title','Terms of Use')
@section('description','Official Unstags Terms of use')
@section('keywords','Terms of use, unstags terms of use, syarat ketentuan unstags')
@section('content')
    <section class="uk-section bg-grey-lighter">
        <div class="uk-container">
            <div uk-grid>
                <div class="uk-width-1-4@m">
                    <div class="uk-card uk-card-default uk-card-body">
                        <ul class="uk-nav uk-nav-default">
                            <li><a href="/terms">Terms</a></li>
                            <li><a href="/privacy">Privacy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="uk-width-3-4@m">
                    <div class="uk-card uk-card-body uk-card-default">
                        <h1>Syarat Dan Ketentuan Unstags</h1>
                        <i>Diperbaharui Tanggal 24 Desember, 2018</i>
                        <div class="content">
                            <p>
                                Terima kasih sudah menggunakan Unstags sebagai media informasi acara untuk mahasiswa. 
                                Misi kami adalah untuk membuat media informasi tentang acara-acara yang dibuat oleh mahasiswa, 
                                baik himpunan,badan eksekutif mahasiwa, unit kegiatan mahasiwa dan lembaga-lembaga yang tersedia di universitas. 
                                Ketika anda mengunjungi, melihat, menggunakan ataupun mengakses website <a href="https://unstags.com">Unstags.com</a> 
                                dan semua subdomains, aplikasi, fungsi, konten, materi, fitur dan layanan online yang kami sediakan, baik sebagai pengguna tak terdaftar maupun terdaftar, 
                                anda setuju untuk mengikuti syarat penggunaan di Unstags.com. Jadi, mohon untuk membaca dengan teliti syarat penggunaan yang ada pada layanan 
                                Unstags.com dibawah ini.
                            </p>
                            <h2>Pengenalan</h2>
                            <p>
                                Dengan mengakses layanan Unstags, anda sadar dan setuju untuk terikat dengan syarat penggunaan ini, baik sebagai pengguna terdaftar maupun tidak terdaftar. 
                                JIKA ANDA TIDAK SETUJU DENGAN SYARAT PENGGUNAAN INI, MAKA ANDA TIDAK DAPAT MENGAKSES ATAU MENGGUNAKAN SELURUH FITUR ATAU KONTEN YANG ADA PADA WEBSITE UNSTAGS.
                            </p>
                            <h2>Kebijakan Privasi</h2>
                            <p>
                                Privasi anda sangat kami jaga dan kami akan selalu berkomitmen untuk menjaga privasi anda baik itu akun secara personal maupun organisasi. 
                                Informasi detail terkait dengan kebijakan privasi bisa anda kunjungi di halaman <a href="/privacy">Kebijakan Privasi</a>.
                            </p>
                            <h2>Situs dan Registrasi Akun</h2>
                            <p>
                                Sebagian besar website Unstags diwajibkan untuk mendaftar akun terlebih dahulu, 
                                anda mungkin akan ditanyakan untuk memasukkan beberapa informasi yang berguna bagi kami untuk melakukan pendekatan terkait 
                                konten yang tepat untuk anda. Anda juga setuju bahwa segala macam informasi yang anda berikan adalah benar adanya.
                            </p>
                            <p>
                                Pastikan bahwa informasi yang anda berikan pada unstags, khususnya informasi sensitif seperti password, tidak anda berikan kepada orang lain.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection