@extends('layouts.app')
@section('title','Apa yang bisa anda dan organisasi anda lakukan dengan Unstags')
@section('ogtitle','Apa yang bisa anda dan organisasi anda lakukan dengan Unstags')
@section('desc','Ketahui lebih dalam tentang fitur yang ada pada Unstags. Kelola acara organisasi anda seperti seminar, workshop dan lain-lain')
@section('ogdesc','Ketahui lebih dalam tentang fitur yang ada pada Unstags. Kelola acara organisasi anda seperti seminar, workshop dan lain-lain')
@section('keywords','Kelola acara, Kelola acara dengan cara baru, Overview Unstags, Unstags Feature')
@section('content')
    @section('content')
    <section class="uk-section uk-section-xsmall">
        <div class="uk-container">
            <div class="uk-child-width-1-2@m uk-grid-small uk-flex-middle" uk-grid>
                <div>
                    <div class="uk-text-left@m uk-text-center">
                        <h1>The new way to make an event</h1>
                        <p>
                            The Revolution will be clicked and touched.
                        </p>
                    </div>
                </div>
                <div>
                   <div class="uk-text-right@m uk-text-center">
                       <img src="/images/icons/overview.svg" alt="Build the new shape of Student Event" width="500">
                   </div>
                </div>
            </div>
        </div>
    </section>
    <section class="uk-section bg-grey-lighter">
        <div class="uk-margin-medium-bottom uk-text-center uk-container uk-container-small">
            <p>
                Platform kami menyediakan kesempatan bagi setiap organisasi maupun individu untuk mengeksplor acara, membuat acara, mengumpulkan
                uang atau belajar.
            </p>
        </div>
        <div class="uk-container">
            <div class="uk-text-center uk-margin-medium-top">
                <a href="/help" class="uk-button uk-button-primary border-80">Baca Panduan</a>
            </div>
        </div>
    </section>
    <section class="uk-section">
        <div class="uk-container">
            <div class="uk-child-width-1-2@m uk-grid uk-flex-middle">
                <div>
                    <h2>Atur Organisasi Anda</h2>
                    <p>
                        Kelola Organisasi anda untuk membuat acara.
                    </p>
                    <a href="{{route('organizer.create')}}" class="uk-button uk-button-primary border-80">Buat Organisasi</a>
                </div>
                <div>
                    <img src="/images/icons/org.svg" alt="Manage Organizer">
                </div>
            </div>
        </div>
    </section>
    <section class="uk-section bg-blue-lightest">
        <div class="uk-container">
            <div class="uk-child-width-1-2@m uk-grid uk-flex-middle">
                <div>
                    <img src="/images/icons/c_event.svg" alt="Create Event">
                </div>
                <div>
                    <h2>Buat Acara</h2>
                    <p>
                        Buat acara organisasi anda dengan bantuan fitur di Unstags.
                    </p>
                    <a href="{{route('event.create')}}" class="uk-button uk-button-primary border-80">Buat Acara</a>
                </div>
            </div>
        </div>
    </section>
    <section class="uk-section">
        <div class="uk-container">
            <div class="uk-child-width-1-2@m uk-grid uk-flex-middle">
                <div>
                    <h2>Tumbuhkan Organisasi anda</h2>
                    <p>
                        Buat acara yang menarik dan tumbukan organisasi anda.
                    </p>
                </div>
                <div>
                    <img src="/images/icons/grow.svg" alt="Manage Organizer">
                </div>
            </div>
        </div>
    </section>
@endsection