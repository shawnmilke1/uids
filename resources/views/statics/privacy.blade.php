@extends('layouts.app') 
@section('title','Kebijakan Privasi') 
@section('description','Official Unstags Privacy Policy') 
@section('keywords','Unstags privacy policy, kebijakan privasi') 
@section('content')
<section class="uk-section bg-grey-lighter">
    <div class="uk-container">
        <div uk-grid>
            <div class="uk-width-1-4@m">
                <div class="uk-card uk-card-default uk-card-body">
                    <ul class="uk-nav uk-nav-default">
                        <li><a href="/terms">Terms</a></li>
                        <li><a href="/privacy">Privacy</a></li>
                    </ul>
                </div>
            </div>
            <div class="uk-width-3-4@m">
                <div class="uk-card uk-card-body uk-card-default">
                    <h1>Kebijakan Privasi Unstags</h1>
                    <b>Diperbaharui Tanggal 15 Januari, 2019</b>
                    <div class="content">
                        <p>
                            Anda sedang memasuki kawasan "Rajin Membaca", kami tahu bahwa membaca kebijakan ini sangatlah membosankan, bahkan developer kami pun malas untuk mengeceknya kembali. 
                            Namun perlu diingat bahwa kebijakan ini ditetapkan untuk mempelajari tentang kebijakan yang ada pada Unstags.
                        </p>
                        <h2>Siapa Kami.</h2>
                        <p>
                            Selamat datang di Unstags! Unstags adalah platform tiket dan acara yang didedikasikan untuk menyatukan mahasiswa melalui pengalaman langsung. 
                            Unstags memungkinkan mahasiswa untuk membuat, mencari, berbagi, dan mendaftar acara. Produk, fitur, dan penawaran lainnya tersedia secara online.
                        </p>
                        <h2>Tentang Kebijakan.</h2>
                        <p>
                            Kebijakan ini menguraikan detail terkait dengan data pribadi anda dengan Unstags. Kebijakan ini berlaku untuk semua fitur 
                            yang ada pada website Unstags.com. Dari waktu ke waktu, kami dapat mengembangkan fitur/layanan baru dan menawarkan fitur/layanan tambahan. 
                            Apabila ada layanan baru atau tambahan ini menyebabkan perubahan pada cara kami memproses data pribadi anda, kami akan memberikan informasi itu melalui Email.
                        </p>
                        <p>
                            Tujuan dari kebijakan ini adalah untuk: 
                            <ul>
                                <li>Memastikan bahwa anda memahami data pribadi apa saja yang kami kumpulkan, alasan kami menggunakannya, dan kepada siapa kami membagikannya.</li>
                                <li>
                                    Menjelaskan cara kami menggunakan data pribadi yang anda bagikan agar dapat memberikan pengalaman terbaik pada saat anda mengakses Unstags.
                                </li>
                            </ul>
                        </p>
                        <h2>Pernyataan Privasi Kami.</h2>
                        <h3>Aplikasi</h3>
                        <p>
                            Kebijakan Privasi ini menetapkan kebijakan kami sehubungan dengan informasi yang dapat dikaitkan dengan atau yang terkait
                            dengan seseorang dan / atau dapat digunakan untuk mengidentifikasi seseorang ("Data Pribadi") yang dikumpulkan dari Pengguna
                            pada atau melalui Layanan. Kami menjaga privasi Data Pribadi Anda dengan serius. Karena itu, kami telah membuat Kebijakan
                            Privasi ini. Silakan baca Kebijakan Privasi ini karena termasuk informasi penting mengenai Data Pribadi Anda dan informasi
                            lainnya. "Data Non-Pribadi" yang digunakan dalam Kebijakan Privasi ini adalah informasi apa pun yang tidak terkait dengan
                            seseorang dan / atau tidak dapat digunakan untuk mengidentifikasi seseorang. Saat Anda berinteraksi dengan Layanan, kami
                            dapat mengumpulkan Data Non-Pribadi. Batasan dan persyaratan dalam Kebijakan Privasi ini tentang pengumpulan, penggunaan,
                            pengungkapan, transfer dan penyimpanan / penyimpanan Data Pribadi kami tidak berlaku untuk Data Non-Pribadi.
                        </p>
                        <h3>Data Pribadi yang Kami Kumpulkan</h3>
                        <p>
                            Saat Anda menggunakan atau berinteraksi dengan kami melalui Layanan, kami dapat mengumpulkan Data Pribadi. 
                            Data ini dikumpulkan melalui kami langsung atau terkadang melalui penyelenggara acara (event organizer) untuk keperluan menjalankan suatu acara.
                        </p>
                        <h3>Semua Pengguna</h3>
                        <p>
                            Untuk semua Pengguna yang kami kumpulkan Data Pribadi ketika Anda secara sukarela memberikan informasi tersebut ke Layanan,
                            seperti ketika Anda mendaftar untuk akses ke Layanan, hubungi kami dengan pertanyaan, menanggapi salah satu survei kami atau
                            menelusuri atau menggunakan bagian dari Layanan. Data Pribadi yang kami kumpulkan termasuk tanpa membatasi nama,
                            alamat, alamat email, dan informasi lain yang memungkinkan Pengguna untuk diidentifikasi secara pribadi.
                        </p>
                        <p>
                            Kami juga secara otomatis mengumpulkan data teknis tertentu yang dikirimkan
                            kepada kami dari komputer, perangkat seluler dan / atau browser tempat Anda mengakses Layanan ("Data Otomatis"). Data Otomatis,
                            termasuk tanpa batasan, pengidentifikasi unik yang terkait dengan perangkat akses Anda dan / atau browser (termasuk, misalnya,
                            alamat Protokol Internet (IP)) tentang perangkat akses dan / atau browser Anda, statistik aktivitas Anda di Layanan , informasi
                            tentang bagaimana Anda datang ke Layanan dan data yang dikumpulkan melalui Cookie, Tag Piksel, Penyimpanan
                            Web dan teknologi serupa lainnya. 
                        </p>
                        <p>
                            Ketika Anda mendaftar untuk Layanan atau mengirimkan Data Pribadi kepada kami, kami dapat mengaitkan Data Non-Pribadi lainnya
                            (termasuk Data Non-Pribadi yang kami kumpulkan dari pihak ketiga) dengan Data Pribadi Anda. Pada keadaan seperti itu, kami
                            akan memperlakukan data gabungan seperti itu sebagai Data Pribadi Anda sampai saat itu tidak dapat lagi dikaitkan dengan
                            Anda atau digunakan untuk mengidentifikasi Anda.
                        </p>
                        <h3>Event Organizer</h3>
                        <p>
                            Sebagai penyelenggara (event organizer), kami mengumpulkan data ekstra yang terdapat pada saat anda membuat <i>event organizer</i>, 
                            kami mengumpulkan informasi data itu untuk keperluan pengguna yang hendak mengikuti acara yang anda selenggarakan.
                        </p>
                        <p>
                            Kami juga dapat mengumpulkan atau menerima Data Pribadi dari sumber pihak ketiga, seperti situs web pihak ketiga, bank Anda,
                            mitra pemrosesan pembayaran kami dan agen pelaporan kredit.
                        </p>
                        <h3>Pengguna</h3>
                        <p>
                            Informasi yang Anda berikan melalui Website atau Aplikasi Unstags: Jika Anda mendaftar untuk acara berbayar, Anda akan
                            memberikan informasi keuangan (misalnya, nomor kartu kredit Anda dan tanggal kedaluwarsa, alamat penagihan, dll.) Yang beberapa
                            di antaranya mungkin merupakan Data Pribadi. Selain itu, Penyelenggara dapat mengatur halaman pendaftaran acara untuk mengumpulkan
                            hampir semua informasi dari Konsumen sehubungan dengan pendaftaran untuk acara Penyelenggara yang tercantum pada Layanan.
                            Unstags tidak mengontrol proses pendaftaran Penyelenggara atau Data Pribadi yang mereka kumpulkan. Data Pribadi yang dikumpulkan
                            atas nama Penyelenggara diberikan kepada Penyelenggara acara yang berlaku sesuai dengan "Bagaimana Kami Mengungkapkan dan
                            Mentransfer Data Pribadi Anda: Penyelenggara" di bawah ini.
                        </p>
                        <h2>Bagaimana Kami Menggunakan Data Pribadi Anda.</h2>
                        <p>
                            Kami mengumpulkan dan menggunakan Data Pribadi yang kami kumpulkan dengan cara yang konsisten dengan Kebijakan Privasi ini.
                            Kami dapat menggunakan Data Pribadi sebagai berikut:
                        </p>
                        <h3>Alasan Khusus</h3>
                        <p>
                            Jika Anda memberikan Data Pribadi untuk tujuan tertentu, kami dapat menggunakan Data Pribadi sehubungan dengan tujuan yang
                            disediakan. Misalnya, jika Anda menghubungi kami melalui email, kami akan menggunakan Data Pribadi yang Anda berikan untuk
                            menjawab pertanyaan Anda atau menyelesaikan masalah Anda dan akan menanggapi alamat email dari mana kontak itu berasal.
                        </p>
                        <p>
                            Jika Anda memberikan Data Pribadi untuk mendapatkan akses ke atau penggunaan Layanan atau fungsi apa pun darinya, kami akan
                            menggunakan Data Pribadi Anda untuk memberi Anda akses ke atau penggunaan Layanan atau fungsionalitas dan untuk menganalisis
                            penggunaan Layanan atau fungsionalitas tersebut. Misalnya, jika Anda memberikan Data Pribadi yang berkaitan dengan identitas
                            atau kualifikasi Anda untuk menggunakan bagian tertentu dari Layanan, kami akan menggunakan informasi itu untuk membuat keputusan
                            untuk memberi Anda akses untuk menggunakan Layanan tersebut dan untuk menilai kualifikasi Anda yang sedang berlangsung untuk
                            menggunakan Layanan tersebut
                        </p>
                        <h3>Tujuan Lain</h3>
                        <p>
                            Jika kami bermaksud untuk menggunakan Data Pribadi apa pun dengan cara apa pun yang tidak konsisten dengan Kebijakan Privasi
                            ini, Anda akan diberitahu tentang penggunaan yang diantisipasi tersebut sebelum atau pada saat Data Pribadi dikumpulkan atau
                            kami akan mendapatkan persetujuan Anda setelah pengumpulan tersebut tetapi sebelum penggunaan tersebut.
                        </p>
                        <h3>Data Pribadi Teragregasi</h3>
                        <p>
                            Dalam upaya berkelanjutan untuk memahami dan melayani Pengguna kami dengan lebih baik, kami sering melakukan penelitian
                            tentang demografi, minat, dan perilaku pelanggan kami berdasarkan Data Pribadi dan informasi lain yang telah kami kumpulkan.
                            Penelitian ini biasanya dilakukan hanya berdasarkan agregat yang tidak mengidentifikasi Anda. Setelah Data Pribadi dalam
                            bentuk agregat, untuk tujuan Kebijakan Privasi ini, itu menjadi Data Non-Pribadi.
                        </p>
                        <h2>
                            Bagaimana Kami Mengungkapkan Data Pribadi Anda.
                        </h2>
                        <h3>Tidak Ada Penjualan Data Pribadi Anda.</h3>
                        <p>
                            Kami tidak dalam bisnis menjual Data Pribadi Anda. Kami menganggap informasi ini sebagai bagian penting dari hubungan kami
                            dengan Anda. Karenanya, kami tidak akan menjual Data Pribadi Anda kepada pihak ketiga, termasuk pengiklan pihak ketiga. Namun
                            demikian, ada keadaan tertentu di mana kami dapat mengungkapkan, mentransfer, atau membagikan Data Pribadi Anda dengan pihak
                            ketiga tertentu tanpa pemberitahuan lebih lanjut kepada Anda, sebagaimana dinyatakan di bawah ini.
                        </p>
                        <h3>Pemindahan, Merger, Pembuatan Ulang Unstags</h3>
                        <p>
                            Ketika kami mengembangkan Unstags, dalam hal tertentu kami (Unstags) mungkin dapat melakukan pembubaran, merger, reorganisasi atau lainnya. 
                            Dalam hal ini, data pribadi anda dapat menjadi bagian dari aset yang ditransfer.
                            Anda mengakui dan menyetujui bahwa setiap penerus atau pengakuisisi Unstags (atau asetnya) akan terus memiliki hak untuk
                            menggunakan Data Pribadi Anda dan informasi lainnya sesuai dengan ketentuan Kebijakan Privasi ini.
                        </p>
                        <h3>Afiliasi / Partner</h3>
                        <p>
                            Kami juga dapat membagikan Data Pribadi anda dengan afiliasi atau partner kami. Afiliasi / Partner kami akan terikat untuk menjaga 
                            Data Pribadi anda tersebut sesuai dengan kebijakan privasi ini.
                        </p>
                        <h3>Event Organizer atau Penyelenggara Acara</h3>
                        <p>
                            Ketika anda membeli tiket, mendaftar atau menyumbang ke suatu acara baik gratis maupun berbayar, 
                            kami memberikan Data Pribadi yang dimasukkan pada acara yang berlaku atau halaman penggalangan dana terkait kepada Penyelenggara
                            acara tersebut atau terkait dengan halaman penggalangan dana.
                        </p>
                        <p>
                            Penting bagi Anda untuk meninjau kebijakan yang berlaku dari Penyelenggara, dan jika berlaku dan tersedia, Penyelenggara
                            Pihak Ketiga yang ditunjuk, dari suatu acara (dan halaman penggalangan dana terkait, jika berlaku) sebelum memberikan
                            Data Pribadi atau informasi lain sehubungan dengan acara itu atau halaman penggalangan dana terkait
                        </p>
                        <p>
                            Demikian pula, jika Anda adalah anggota organisasi Penyelenggara , Data Pribadi Anda akan tersedia untuk
                            Penyelenggara dan dibagikan dengan Penyelenggara Pihak Ketiga yang diberi izin oleh Penyelenggara untuk melihat semua anggota
                            organisasi Penyelenggara.
                        </p>
                        <h2>Perubahan Terhadap Kebijakan Privasi</h2>
                        <p>
                            Layanan kami dapat berubah dari waktu ke waktu. Akibatnya, kadang-kadang mungkin perlu bagi kami untuk membuat
                            perubahan pada Kebijakan Privasi ini. Kami berhak, atas kebijakan kami sendiri, untuk memperbarui atau memodifikasi Kebijakan
                            Privasi ini kapan saja (secara bersama-sama disebut, "Modifikasi"). Modifikasi terhadap Kebijakan Privasi ini akan diposting
                            ke Situs dengan perubahan ke tanggal "Diperbarui" di bagian atas Kebijakan Privasi ini. Dalam keadaan tertentu Unstags
                            dapat, tetapi tidak perlu, memberi Anda pemberitahuan tambahan tentang Modifikasi tersebut, seperti melalui email atau dengan
                            pemberitahuan dalam-Layanan. Modifikasi akan efektif empat belas (14) hari setelah tanggal "Diperbarui" atau tanggal lainnya
                            yang dikomunikasikan dalam pemberitahuan lain kepada Anda. Harap tinjau kebijakan ini secara berkala, dan terutama sebelum
                            Anda memberikan Data Pribadi apa pun. Kebijakan Privasi ini diperbarui pada tanggal yang ditunjukkan di atas. Anda terus
                            menggunakan Layanan setelah efektivitas Modifikasi apa pun terhadap Kebijakan Privasi ini merupakan penerimaan atas Modifikasi
                            tersebut. Jika ada Modifikasi terhadap Kebijakan Privasi ini tidak dapat Anda terima, Anda harus berhenti mengakses, menjelajah,
                            dan menggunakan Layanan.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="uk-section uk-section-xsmall">
    <div class="uk-container uk-text-center">
        <h3>Masih Ada Pertanyaan? <a href="/support/contact-us">Hubungi Kami</a></h3>
    </div>
</section>
@endsection