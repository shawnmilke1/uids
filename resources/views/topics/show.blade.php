@extends('layouts.app')
@section('title',"Acara {$topic->name}")
@section('desc',"Temukan acara tentang {$topic->name} di Unstags")
@section('keywords',"Acara {$topic->name}")
@section('ogtitle',"Acara {$topic->name}")
@section('ogdesc',"Temukan acara tentang {$topic->name} di Unstags")
@section('content')
    <section class="uk-section uk-section-xsmall">
        <div class="uk-container">
            <div class="uk-child-width-1-2@m uk-flex-middle" uk-grid>
                <div>
                    <div class="uk-text-bold uk-text-large uk-text-left@m uk-text-center">Topik {{$topic->name}}</div>
                </div>
                <div class="uk-text-right@m uk-text-center">
                    <a href="{{route('event.create')}}" class="uk-button uk-button-primary border-80">Buat Acara</a>
                </div>
            </div>
        </div>
    </section>
    @include('events.card')
@endsection