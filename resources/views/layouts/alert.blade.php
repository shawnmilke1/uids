@if(Session::has('errors'))
<script>
    UIkit.notification({
        message: 'Something went Wrong',
        status: 'warning',
        pos: 'top-center',
        timeout: 5000
    });

</script>
@endif @if(Session::has('success'))
<script>
    UIkit.notification({
        message: '{{Session::get('success')}}',
        status:'primary',
        pos: 'top-center',
        timeout:5000
    });

</script>
@endif