<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Unstags') }} - @yield('title')</title>
    <meta name="description" content="@yield('desc','Unstags')"/>
    <meta name="keywords" content="@yield('keywords','Unstags')" />
    {{-- Icon --}}
    <meta name="theme-color" content="#ee4a5a">
    <meta name="apple-mobile-web-app-status-bar-style" content="white-translucent">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg">
    {{-- Open Graph --}}
    <meta property="og:description" content="@yield('ogdesc')" />
    <meta property="og:title" content="@yield('ogtitle')" />
    <meta property="og:image" content="@yield('ogimage',asset('/favicon/unstags_logo.png'))" />
    <meta property="og:url" content="{{url()->current()}}" />
    <link rel="canonical" href="{{url()->current()}}">

    @env('production')
    {{-- GTAG --}}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131377299-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-131377299-1');
    
    </script>
    {{-- Facebook Analytic + Oauth --}}
    <script>
        window.fbAsyncInit = function() {
        FB.init({
          appId      : '2184112911838313',
          xfbml      : true,
          version    : 'v3.2'
        });
        FB.AppEvents.logPageView();
      };
    
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "https://connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    
    </script>
    @else
    <script>
        window.fbAsyncInit = function() {
        FB.init({
        appId      : '155652838576433',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.2'
        });
        
        FB.AppEvents.logPageView();   
        
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

</script>
    @endenv

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" defer>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800,800i" rel="stylesheet" defer>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700" rel="stylesheet" defer>
    @yield('fonts')
    <!-- Styles -->
    <link rel="stylesheet" href="{{mix('css/app.css')}}" defer>
    @yield('css')
</head>
<body>
    <div id="app">
        @include('layouts.navbar')

        @yield('content')
        @include('layouts.footer')
        @include('layouts.offcanvas')
    </div>
  
    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="{{mix('js/manifest.js')}}" defer ></script>
    <script src="{{mix('js/vendor.js')}}" defer></script>
    @env('production')
    <!-- UIkit JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/js/uikit.min.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.2/js/uikit-icons.min.js" defer></script>
    @else
    <script src="{{asset('js/unstags.js')}}"></script>
    <script src="{{asset('js/unstags-icons.js')}}"></script>
    @endenv
    <script src="{{mix('js/stylePlus.js')}}" defer></script>
    @include('layouts.alert')
    @yield('js')
</body>
</html>
