{{-- This will control if a page has a absolute bottom navigation --}}
@php
    $absolute = "p-100";
    if(!Request::is('events/*')){
        $absolute = '';
    }
@endphp
<hr class="uk-margin-remove">
<footer class="un-footer bg-grey-lighter uk-text-left@m uk-text-center {{$absolute}}">
    <div class="uk-container uk-container-small">
        <div class="footer-list">
            <div class="uk-child-width-1-5@m" uk-grid>
                <div>
                    <img src="{{asset('/favicon/unstags_logo.png')}}" width="50" alt="unstags logo">
                </div>
                <div>
                    <ul class="uk-nav uk-nav-default">
                        <li class="uk-nav-header uk-text-bold">Unstags</li>
                        <li>
                            <a href="/events">Events</a>
                        </li>
                        <li>
                            <a href="#">About Us</a>
                        </li>
                        <li>
                            <a href="/privacy">Privacy</a>
                        </li>
                        <li>
                            <a href="/terms">Terms</a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="uk-nav uk-nav-default">
                        <li class="uk-nav-header uk-text-bold">RESOURCES</li>
                        <li>
                            <a href="#">Partners</a>
                        </li>
                        <li>
                            <a href="/help">Knowledge Base</a>
                        </li>
                        <li>
                            <a href="#">Content Standards</a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="uk-nav uk-nav-default">
                        <li class="uk-nav-header uk-text-bold">SUPPORT</li>
                        <li>
                            <a href="{{route('contact.index')}}">Contact Us</a>
                        </li>
                        <li>
                            <a href="#">FAQ</a>
                        </li>
                    </ul>
                </div>
                <div>
                    <a href="#" class="uk-icon-button uk-button-primary uk-margin-small-right" uk-icon="facebook"></a>
                    <a href="https://instagram.com/unstags4" class="uk-icon-button uk-button-primary" uk-icon="instagram"></a>
                </div>
            </div>
            <hr>
            <div class="un-copy">
                <span class="uk-text-muted uk-text-small">Copywrong © {{now()->format('Y')}} Unstags. All rights reserved </span>
            </div>
        </div>
    </div>
</footer>