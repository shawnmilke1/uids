@extends('layouts.app')

@section('content')
    <section class="uk-section uk-section-small">
        <div class="uk-container">
            <a href="/help"> <span uk-icon="arrow-left"></span> Back To Help Center Dashboard</a>
            @yield('help-header')
        </div>
    </section>
    <hr class="uk-margin-remove">
    <div uk-grid>
        <div class="uk-width-1-4@m bg-grey-lighter">
            <div class="uk-tile uk-padding-small">
                @include('help_types.nav',['types' => \App\HelpType::get()])
            </div>
        </div>
        <div class="uk-width-3-4@m">
            <div class="uk-tile uk-padding-small">
                @yield('help-content')
            </div>
        </div>
    </div>
    <hr class="uk-margin-remove">
@endsection