<nav class="uk-navbar-container ui-border bottom main-navbar uk-navbar">
    <div class="uk-navbar-left">
        <a class="uk-navbar-item uk-logo uk-visible@m" href="/">
            <img alt="Unstags Logo" src="/favicon/unstags_logo.png" width="50">
        </a>
       
        <a href="#offcanvas-slide" uk-toggle class="uk-navbar-toggle uk-hidden@m uk-navbar-toggle-icon uk-icon" uk-icon="icon: menu"></a>
        <a class="uk-navbar-toggle uk-hidden@m uk-margin-small-left" href="#search-modal-full" uk-search-icon uk-toggle></a>
    </div>
    <div class="uk-navbar-center">
        <a class="uk-navbar-item uk-hidden@m uk-logo" href="/">
            <img alt="Unstags Logo" src="/favicon/unstags_logo.png" width="50" alt="Unstags Logo">
        </a>
    </div>
    <div class="uk-navbar-right">

        <div class="uk-navbar-item ui-border left">
            <ul class="uk-navbar-nav uk-visible@m">
                <li {{is_active( 'events')}}>
                    <a href="/events" class="uk-button uk-button-text">Events</a>
                </li>
            </ul>
            <a class="uk-navbar-toggle uk-visible@m uk-margin-small-right" href="#search-modal-full" uk-search-icon uk-toggle></a>
            @auth
                <div>
                    <button class="uk-icon-button user-icon" type="button" uk-icon="icon:user;ratio:1"></button>
                </div>
                <div class="uk-dropdown" id="user-navbar">
                    <ul class="uk-nav uk-dropdown-nav">
                        <li {{is_active( 'choose')}}>
                            <a href="{{route('organizer.choose')}}">Event Dashboard</a>
                        </li>
                        <li>
                            <a href="/@/organizers">Organizers</a>
                        </li>
                        <li>
                            <a href="/@/purchase">My Tickets</a>
                        </li>
                        <li class="uk-nav-divider"></li>
                        <li>
                            <a href="{{route('usetting.index')}}">Account Setting</a>
                        </li>
                        <li>
                            <a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('form-logout').submit();">
                                Log Out
                            </a>
                        </li>
                    </ul>
                </div>
                <form method="POST" id="form-logout" style="display: none;" action="{{route('logout')}}">
                    @csrf
                </form>
            @else
            <a href="/register" class="uk-button uk-visible@m uk-button-primary uk-margin-small-right uk-border-rounded uk-button-small">Sign Up</a>
            <a href="/login" class="uk-button uk-visible@m uk-button-text uk-margin-small-left uk-button-small">Log In</a>
            @endauth
        </div>
    </div>
</nav>
<div id="search-modal-full" class="uk-modal-full uk-modal" uk-modal>
    <div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle" uk-height-viewport>
        <button class="uk-modal-close-full" type="button" uk-close></button>
        <form class="uk-search uk-search-large" action="/events">
            <input class="uk-search-input uk-text-center" name="search" type="search" placeholder="Cari Event" autofocus>
        </form>
    </div>
</div>