<div id="offcanvas-slide" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" uk-close></button>
        <div class="uk-grid-small uk-flex-middle uk-margin-medium-bottom" uk-grid>
            <div class="uk-width-auto">
                <img class="uk-border-circle" width="40" height="40" src="/favicon/unstags_logo.png">
            </div>
            <div class="uk-width-expand">
                <h3 class="uk-card-title uk-margin-remove-bottom">Unstags</h3>
            </div>
        </div>
        <ul class="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical">
            <li {{is_active('events')}}>
                    <a href="/events">Events</a>
                </li>
            @guest
            <li class="uk-nav-divider"></li>
            <li>
                <a href="/login">Login / Register</a>
            </li>
            @endguest
        </ul>
        
    </div>
</div>