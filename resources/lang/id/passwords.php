<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Password setidaknya berisi 6 karakter dan cocok dengan konfirmasi password.',
    'reset' => 'Password Anda telah di reset!',
    'sent' => 'Kami telah mengirimkan email untuk mengganti password anda!',
    'token' => 'This password reset token is invalid.',
    'user' => "Kami tidak dapat mencari user dengan email ini.",

];
