<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function all_user_can_open_index_page()
    {
        //Given Any Role of user or guest or auth

        //when hit the /events to see index page of event
        $response = $this->get('/events');
        //then should be view all event 
        $response->assertOk();

    }

    /** @test */
    public function auth_user_can_create_event_but_redirect_if_dont_have_organizer()
    {
        //Given Any Role except Guest
        $user = factory('App\User')->make(['role_id'=> 2]);
        //when hit the /events/create, and the user didnt have organizer
        // then should be redirect to organizers->create
        $this->be($user)->get('/events/create')->assertRedirect('/organizers/create');
    }

    /** @test */
    public function auth_user_can_create_event_and_have_organizer()
    {
        // Give Any Role exept Guest
        $user = $this->createNormalUser();
        //then create it organizer to go to event/create page
        $user->organizers()->create([
            'name' => 'haha',
            'slug' => 'hahaasfasda',
            'user_id' => $user->id,
            'image' => 'hehen',
            'description' => 'haha'
        ]);
        //then should be access to /events/create
        $this->be($user)->get('/events/create')->assertOk();
    }

    function createNormalUser()
    {
        return factory('App\User')->make(['role_id' => 2,'id' => 1]);
    }
}
