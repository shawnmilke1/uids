<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['auth:api']], function () {
    Route::apiResource('events', 'Api\EventController');
    Route::get('/events/{id}/stats','Api\EventViewController@show');
    Route::get('/myevents','Api\UserController@events');
    Route::get('/o/{slug}/events', 'Api\OrganizerController@orgEvents');
    Route::get('/o/{slug}/collection','Api\OrganizerController@collection');
    //user organizers member
    Route::get('/user/organizers','Api\UserController@organizers');
    Route::get('/organizers/{id}/members','Api\MemberController@members');
    Route::post('/organizers/{id}/members','Api\MemberController@store');
    Route::delete('/members/{id}','Api\MemberController@destroy');
    Route::post('/members/{id}','Api\MemberController@update');

    Route::get('/logout','Api\UserController@logout');
    Route::get('/events/{id}/get-total-auds','Api\EventController@totalAuds');

    Route::get('/events/{id}/tickets','Api\TicketController@index');
    Route::post('/events/{id}/tickets','Api\TicketController@store');
    Route::delete('/tickets/{id}','Api\TicketController@destroy');
    Route::get('/tickets/{id}','Api\TicketController@show');
    Route::post('/tickets/{id}','Api\TicketController@update');
    //audiences
    Route::get('/tickets/{id}/audiences/','Api\AudienceController@index');
    Route::get('/{id}/allauds','Api\AudienceController@allAuds');
    Route::post('/audiences/{id}/check','Api\AudienceController@check');
    Route::post('/audiences/{id}/setAudStatus','Api\AudienceController@setAudStatus');

    //roles of member
    Route::get('/organizer-roles','Api\OrganizerRoleController@index');
});
