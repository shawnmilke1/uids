<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

//oauth
Route::get('/provider/{provider}','SocialController@redirectToProvider')->name('social.login');
Route::get('/provider/{provider}/callback','SocialController@handleProviderCallback')->name('social.callback');
Route::delete('/provider/{id}','SocialController@destroy')->name('social.destroy');

Route::get('/', 'HomeController@index')->name('home');
Route::resource('service','ServiceController')->only(['index','store']);

Route::group(['middleware' => ['verified','auth'], 'prefix' => '@'],function(){
    Route::get('/basic','UserSettingController@index')->name('usetting.index');
    Route::post('/basic','UserSettingController@storeBasic')->name('usetting.store');
    Route::get('/password','UserSettingController@password')->name('usetting.password');
    Route::post('/password','UserSettingController@postPassword')->name('usetting.post_password');
    Route::resource('organizers', 'OrganizerController')->except('show')->names('organizer');
    Route::get('/purchase','AudienceController@index')->name('audience.index');
    Route::get('/purchase/{id}','AudienceController@show')->name('audience.show');
    Route::get('/connections','SocialController@index')->name('social.index');
});

//show organizer
Route::get('/organizers/{slug}','OrganizerController@show')->name('organizer.show');
Route::get('/organizers/{slug}/members','OrganizerController@members')->name('organizer.member');
Route::get('/choose','OrganizerController@choose')->name('organizer.choose');

Route::get('/o/{slug}{any}','OrganizerController@dashboard')->where('any','.*');

Route::resource('events','EventController')->names('event');
Route::get('/events/preview/{slug}','EventController@preview')->name('event.preview');

Route::get('/topic/{slug}','TopicController@show')->name('topic.show');
Route::get('/type/{slug}','TypeController@show')->name('type.show');
Route::get('/city/{slug}','CityController@show')->name('city.show');
Route::get('/events/{uuid}/ticket','AudienceController@create')->name('aud.create');
Route::post('/events/{uuid}/ticket','AudienceController@store')->name('aud.store');
Route::get('/check_in/{uuid}','AudienceController@checkIn')->name('aud.check');


Route::group(['prefix' => 'undiadmin'], function () {
    Voyager::routes();
});

//Help Center
Route::get('/help','HelpController@index')->name('help.index');
Route::get('/help/categories/{slug}','HelpTypeController@show')->name('ht.show');
Route::get('/help/{categories}/{slug}','HelpController@show')->name('help.show');
//Support
Route::get('/support/contact-us','ContactController@index')->name('contact.index');
Route::post('/support/contact-us','ContactController@store')->name('contact.store');
//all static routes here
Route::view('/terms','statics.terms');
Route::view('/privacy','statics.privacy');
Route::view('/overview','statics.overview');

//Json Return
Route::get('/api/cities','CityController@index');