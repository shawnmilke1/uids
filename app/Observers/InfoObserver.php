<?php

namespace App\Observers;

use App\Info;
use App\User;
use Mail;
use App\Mail\Info as InfoMail;

class InfoObserver
{
    /**
     * Handle the info "created" event.
     *
     * @param  \App\Info  $info
     * @return void
     */
    public function saved(Info $info)
    {
        $users = User::where('email_verified_at','!=',null)->get();

        foreach($users as $user){
            Mail::to($user->email)
                ->queue(new InfoMail($info,$user));
        }

    }
}
