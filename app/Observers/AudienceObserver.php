<?php

namespace App\Observers;

use App\Audience;
use App\Payment;
use App\Jobs\SendOrderTicket;
use Mail;
use App\Mail\SendReceiptTicket;

class AudienceObserver
{
    /**
     * Handle the audience "created" event.
     *
     * @param  \App\Audience  $audience
     * @return void
     */
    public function created(Audience $audience)
    {
        $price = $audience->ticket->price == "free" ? 0 : $audience->ticket->price;
        $date = $price == 0 ? date(now()) : null;
        $method = $price == 0 ? 'instant' : 'direct';
        $payment = $audience->payment()->create([
            'method' => $method,
            'paid_at' => $date,
            'price' => $price
        ]);
        if ($price == 0) {
            dispatch(new SendOrderTicket($audience));
            $ticket= $audience->ticket;
            $ticket->ticket_sell += 1;
            $ticket->save();
        } else {
            Mail::to($audience->email)
                ->queue(new SendReceiptTicket($audience));
        }
    }

    /**
     * Handle the audience "updating" event
     * @param \App\Audience $audience
     * @return void
     */
    public function updating(Audience $audience)
    {
        $status = $audience->getOriginal('status');
        $expired_at = $audience->getOriginal('expired_at');
        if($status == 'unpaid' && $audience->status == "paid" && $expired_at != null){
            $audience->expired_at = null;
            $audience->payment()->update([
                'paid_at' => now()
            ]);
            dispatch(new SendOrderTicket($audience));
            $ticket = $audience->ticket;
            $ticket->ticket_sell += 1;
            $ticket->save();
        }
    }

}
