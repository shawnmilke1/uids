<?php

namespace App\Observers;

use App\Organizer;
use App\Member;
use DB;

class OrganizerObserver
{
    /**
     * Handle the organizer "saved" event.
     *
     * @param  \App\Organizer  $organizer
     * @return void
     */
    public function created(Organizer $organizer)
    {
        $member = new Member();
        $member->user_id = auth()->id();
        $member->organizer_id = $organizer->id;
        $member->organizer_role_id = 1;
        $member->save();
    }

}
