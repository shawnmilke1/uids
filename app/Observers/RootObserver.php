<?php

namespace App\Observers;

class RootObserver
{
    public static function inject()
    {
        \App\Event::observe(\App\Observers\EventObserver::class);
        \App\Organizer::observe(\App\Observers\OrganizerObserver::class);
        \App\Info::observe(\App\Observers\InfoObserver::class);
        \App\Audience::observe(\App\Observers\AudienceObserver::class);
    }
}
