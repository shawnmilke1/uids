<?php

namespace App\Observers;

use App\Event;

class EventObserver
{
    
    /**
     * Handle the event "saving" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function saving(Event $event)
    {
        $event->slug = str_slug($event->title).'-'.str_random(8);
        $event->user_id = auth()->id();
       
        // $event->description = preg_replace("/<p>[\s*|&nbsp;]<\/p>/",'');
    }

    /**
     * Handle the event "deleted" event.
     *
     * @param  \App\Event  $event
     * @return void
     */
    public function deleted(Event $event)
    {
        //
    }
}
