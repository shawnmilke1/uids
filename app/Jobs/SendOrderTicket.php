<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\TicketMail;
use Mail;

class SendOrderTicket implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $audience;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($audience)
    {
        $this->audience = $audience;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // if (config('app.env') == "production") {
        //     $transport = (new \Swift_SmtpTransport(config('mail.host'), config('mail.port'), config('mail.encryption')))
        //         ->setUsername(config('mail.username2'))
        //         ->setPassword(config('mail.password2'));

        //     $mailer = new \Swift_Mailer($transport);
        //     Mail::setSwiftMailer($mailer);
        // }

        $email = new TicketMail($this->audience);
        Mail::to($this->audience->email)->send($email);
    }
}
