<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\NotifyMember;
use Illuminate\Support\Facades\Mail;

class SendNotifyMember implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $organizer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user,$organizer)
    {
        $this->user = $user;
        $this->organizer = $organizer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new NotifyMember($this->user,$this->organizer);
        Mail::to($this->user->email)->send($email);
    }
}
