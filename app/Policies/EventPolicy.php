<?php

namespace App\Policies;

use App\User;
use App\Event;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;
    public function before(User $user ,$ability)
    {
        if($user->isSuperAdmin()){
            return true;
        }
    }
    /**
     * handle View, Update and Delete Section
     *
     * @param  \App\User  $user
     * @param  \App\Event  $event
     * @return mixed
     */
    public function view(User $user, Event $event)
    {
        $members = $event->organizer->members();
        return $members->where('user_id',$user->id)->exists();
    }

    public function browse(User $user, Event $event)
    {
        return $user->role_id === 1;
    }
}
