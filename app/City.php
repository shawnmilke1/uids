<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [];

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function getRouteKeyName()
    {
        return str_slug('name');
    }
}
