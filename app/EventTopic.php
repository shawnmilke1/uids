<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTopic extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::saving(function($topic){
            $topic->slug = str_slug($topic->name); 
        });
    }
}
