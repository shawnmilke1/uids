<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $guarded = [];

    /**
     * Member belongsTo Organizer
     * @param organizer_id
     */
    public function organizer()
    {
        return $this->belongsTo('App\Organizer');
    }

    /**
     * Member belonsgTo User,
     * the user assign as a member of organizer
     * @param user_id
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Member belongsTo Role Id
     */
    public function role()
    {
        return $this->belongsTo('App\OrganizerRole','organizer_role_id');
    }

}
