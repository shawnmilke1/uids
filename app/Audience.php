<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;
use App\Filters\AudienceFilters;

class Audience extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::creating(function($aud) {
            $aud->uuid = Uuid::uuid4();
        });
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

    /**
     * Has One Payment
     */
    public function payment()
    {
        return $this->morphOne('App\Payment','paymentable');
    }

    /**
     * Scope Filter
     */
    public function scopeFilter($query,AudienceFilters $filter)
    {
        return $filter->apply($query);
    }

    /**
     * Audience belongsTo User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
