<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpType extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::saving(function($helpType){
            $helpType->slug = str_slug($helpType->name);
        });
    }

    public function helps()
    {
        return $this->hasMany('App\Help');
    }
}
