<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];

    public function service()
    {
        return $this->belongsTo('App\Service');
    }
}
