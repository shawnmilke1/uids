<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = [];

    /**
     * Service Has Many Tasks
     */
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    protected static function boot()
    {
        parent::boot();
        static::saving(function($service){
            $service->slug = str_slug($service->company);
        });
    }

    public function setSlugAttritube($value)
    {
        if(static::whereSlug($slug = str_slug($value))->exists()){
            $slug = "{$slug}-{$this->id}";
        }
        $this->attributes['slug'] =$slug;
    }

}
