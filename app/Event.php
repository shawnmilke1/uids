<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Filters\EventFilters;

class Event extends Model
{
    protected $guarded = [];
    protected $with = ['tickets','views'];
    /**
     * this belongsTo User
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * This belongs To Event Type
     */
    public function type()
    {
        return $this->belongsTo('App\EventType','event_type_id');
    }
    /**
     * belongsTo Organizer
     */
    public function organizer()
    {
        return $this->belongsTo('App\Organizer');
    }

    /**
     * Event HasMany Ticket
     * Limit 2 for Free User
     */
    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    /**
     * Event has many audiences
     */
    public function auds()
    {
        return $this->hasMany('App\Audience');
    }

    /**
     * Event belongsTo City
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }
    /**
     * Evnt has many view
     */
    public function views()
    {
        return $this->hasMany('App\EventView','event_id');
    }
    
    /**
     * Scope filter for events
     */
    public function scopeFilter($query,EventFilters $filter)
    {
        return $filter->apply($query);
    }
}
