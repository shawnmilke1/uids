<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventView extends Model
{
    protected $guarded = [];

    public function event()
    {
        return $this->belongsTo('App\Event','event_id');
    }
}
