<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizerRole extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::saved(function($role){
            $role->slug = str_slug($role->name);
        });
    }

    /**
     * Role has many member
     */
    public function members()
    {
        return $this->hasMany('App\Members');
    }
}
