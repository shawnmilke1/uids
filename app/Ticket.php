<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = [];
    /**
     * Ticket belongsTo Event
     */
    public function event()
    {
        return $this->belongsTo('App\Event');
    }

    public function auds()
    {
        return $this->hasMany('App\Audience');
    }

    public function getTotalAuds()
    {
        return $this->hasMany('App\Audience')->where(['ticket_id' => $this->id])->count();
    }
}
