<?php
namespace App\Filters;

use App\User;
use App\EventTopic;
use App\EventType;

class EventFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['topic', 'type','status','search'];
    /**
     * Filter the query by a given topic slug.
     *
     * @param  string $slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function topic($slug)
    {
        if($slug != "all"){
            $topic = EventTopic::where('slug', $slug)->firstOrFail();
            return $this->builder->where('event_topic_id', $topic->id);
        }
        return $this->builder;
    }

    /**
     * Filter the query by a given type slug
     * @param string $slug
     * @return Builder
     */
    protected function type($slug)
    {
        if($slug != "all"){
            $type = EventType::where('slug', $slug)->firstOrFail();
            return $this->builder->where('event_type_id', $type->id);
        }
        return $this->builder;
    }

    protected function status($stat)
    {
        return $this->builder->where('status',$stat);
    }

    protected function search($search)
    {
        return $this->builder->where('title','like', "%$search%");
    }

}