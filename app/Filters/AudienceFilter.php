<?php
namespace App\Filters;

use App\User;
use App\Audience;

class AudienceFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['tanggal'];

    /**
     * Filter Ticket by Date
     */
    protected function tanggal($date)
    {
        return $this->builder->whereDate('created_at',$date)
            ->orWhereMonth('created_at', $date);
    }

}