<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReceiptTicket extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $aud;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($aud)
    {
        $this->aud = $aud;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Pembayaran Tiket";
        return $this->view('emails.tickets.invoice')
                    ->replyTo($this->aud->event->organizer->email)
                    ->subject($subject)
                    ->with([
                        'aud' => $this->aud
                    ]);
    }
}
