<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Info extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $user;
    protected $info;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($info,$user)
    {
        $this->info = $info;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "{$this->info->title}";
        return $this->markdown('emails.info')
                    ->subject($subject)
                    ->with([
                        'info' => $this->info,
                        'user' => $this->user,
                    ]);
    }
}
