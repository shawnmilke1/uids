<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $audience;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($audience)
    {
        $this->audience = $audience;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Hello {$this->audience->full_name}, Tiket Kamu Telah Datang";
        return $this
            ->replyTo($this->audience->ticket->event->user->email)
            ->subject($subject)->view('emails.tickets.ticket')
            ->with(['audience' => $this->audience]);
    }
}
