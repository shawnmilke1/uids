<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyMember extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $organizer;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$organizer)
    {
        $this->user = $user;
        $this->organizer = $organizer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Hello {$this->user->username}";
        return $this
            ->replyTo($this->user->email)
            ->subject($subject)
            ->view('emails.organizers.member')
            ->with([
                    'user' => $this->user,
                    'organizer' => $this->organizer
                ]);
    }
}
