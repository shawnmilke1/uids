<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($type) {
            $type->slug = str_slug($type->name);
        });
    }

    /**
     * this hasMany events
     */
    public function events()
    {
        return $this->hasMany('App\Event');
    }
}
