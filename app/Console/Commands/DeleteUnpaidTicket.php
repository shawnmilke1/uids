<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Audience;

class DeleteUnpaidTicket extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:unpaid_ticket';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Unpaid Ticket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $auds = \App\Audience::where('expired_at', '<=', now())->get();
        foreach ($auds as $aud) {
            $aud->payment->delete();
            $aud->delete();
        }
        $this->info('Delete UnPaid Tiket');
    }
}
