<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class EventStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uids:event:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Event Change to Passed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $events = DB::table('events')
                    ->whereStatus('PUBLISHED')
                    ->where('starts_date', '<=', \Carbon\Carbon::now()->format('Y-m-d'));
        $events->update(['status' => 'PASSED']);
        $this->info('Change Event To Passed');
    }
}
