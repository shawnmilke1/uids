<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Organizer has Many Events
     */
    public function events()
    {
        return $this->hasMany('App\Event');
    }

    /**
     * Organizer hasmany members
     */
    public function members()
    {
        return $this->hasMany('App\Member');
    }
}
