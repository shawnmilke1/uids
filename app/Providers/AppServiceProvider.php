<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use App\Observers\RootObserver;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        RootObserver::inject();
        Blade::if('env',function($env){
            return app()->environment($env);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
