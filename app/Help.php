<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Help extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::saving(function($help){
            $help->uuid = Uuid::uuid4();
            $help->slug  = str_slug($help->title);
        });
    }

    public function type()
    {
        return $this->belongsTo('App\HelpType','help_type_id');
    }
}
