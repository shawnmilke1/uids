<?php

namespace App\Http\Middleware;

use Closure;

class HaveOrganizer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->user()->organizers()->exists()){
            return redirect()->route('organizer.create')->withErrors('Create Event Organizer First!')->with('need_eo','Buat event organizer terlebih dahulu untuk membuat acara');
        }
        return $next($request);
    }
}
