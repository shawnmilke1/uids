<?php
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\HtmlString;
use Parsedown as Parse;

/**
 * Alias of user first name & Last name
 */
function initial_user()
{
    $first_name = substr(auth()->user()->first_name, 0, 1);
    $last_name = substr(auth()->user()->last_name, 0, 1);

    return "$first_name$last_name";
}

/**
 * Markdown Parse
 * @param data
 */
function parsemd($data)
{
    $parsedown = new Parse;
    $parsedown->setSafeMode(true);
    return $parsedown->text($data);
}

function if_error($errors, $field, $class = null)
{
    if ($errors->has($field)) {
        $say = sprintf('<span %s>%s</span>', $class != null ? "class=$class" : '', $errors->first($field));
        return $say;
    }
}

function is_active($path)
{
    return Request::is($path . '*') ? 'class=uk-active' : '';
}

function is_active_full($path)
{
    return url()->full() == url($path) ? 'class=uk-active' : '';
}

function isActive($path)
{
    return Request::is($path) ? 'class=uk-active' : '';
}

function fullName($one, $two)
{
    return "{$one} {$two}";
}

function user_full_name()
{
    $first_name = auth()->user()->first_name;
    $last_name = auth()->user()->last_name;
    return "$first_name $last_name";
}

function parseDate($data)
{
    return Carbon::parse($data);
}

function dayNow($data)
{
    return parseDate($data)->format('Y-m-d') <= now()->format('Y-m-d');
}

function beforeDayNow($data)
{
    return parseDate($data)->subDays(1)->format('Y-m-d') <= now()->format('Y-m-d');
}

/**
 * Calculating Estimating Reading of Content Body
 * @param content
 */
function estimate_reading_time($content)
{
    $word_count = str_word_count(strip_tags($content));

    $minutes = floor($word_count / 200);
    $seconds = floor($word_count % 200 / (200 / 60));

    $str_minutes = ($minutes == 1) ? "min" : "min";
    $str_seconds = ($seconds == 1) ? "sec" : "sec";

    if ($minutes == 0) {
        return "{$seconds} {$str_seconds}";
    } else {
        return "{$minutes} {$str_minutes}";
    }
}

if(!function_exists('user')){
    function user($attr)
    {
        $user = \Auth::user();
        return $user[$attr];
    }
}

if (!function_exists('mixUrl')) {
    /**
     * Get the path to a versioned Mix file.
     *
     * @param  string  $path
     * @param  string  $manifestDirectory
     * @param  string  $baseUrl
     * @return \Illuminate\Support\HtmlString
     *
     * @throws \Exception
     */
    function mixUrl($path, $manifest = false, $shouldHotReload = false)
    {
        if (!$manifest) static $manifest;
        if (!$shouldHotReload) static $shouldHotReload;
        if (!$manifest) {
            $manifestPath = public_path('/dist/mix-manifest.json');
            $shouldHotReload = file_exists(public_path('hot'));
            if (!file_exists($manifestPath)) {
                throw new Exception(
                    'The Laravel Mix manifest file does not exist. ' .
                        'Please run "npm run webpack" and try again.'
                );
            }
            $manifest = json_decode(file_get_contents($manifestPath), true);
        }
        if (!starts_with($path, '/')) $path = "/{$path}";
        if (!array_key_exists($path, $manifest)) {
            throw new Exception(
                "Unknown Laravel Mix file path: {$path}. Please check your requested " .
                    "webpack.mix.js output path, and try again."
            );
        }
        return $shouldHotReload
            ? "http://localhost:8080{$manifest[$path]}"
            : url($manifest[$path]);
    }
}