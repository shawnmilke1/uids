<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        return [
            'uuid' => 'required',//change later,
            'event_id' => $id == NULL ? 'required' : 'nullable',
            'name' => 'required',
            'qty' => 'required',
            'price' => 'required',
            'description' => 'nullable',
            'channel' => 'nullable',
            'starts_date' => 'nullable',
            'ends_date' => 'nullable',
            'starts_time' => 'nullable',
            'ends_time' => 'nullable',
            'per_order' => 'nullable',
            'cancel_policy' => 'nullable',
            'refundable' => 'nullable',
            'ticket_sell' => 'nullable',
            'ticket_limit' => 'nullable|min:1|max:5'
        ];
    }
}
