<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrganizerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $uid = $this->route('organizer');
        $name = $uid != null ? 'required|unique:organizers,name,'.$uid : 'required|unique:organizers';

        return [
            'name' => $name,
            'description' => 'required',
            'website' => 'nullable|url',
            'slug' => $uid != null ? 'required|min:3|alpha_dash|unique:organizers,slug,'.$uid : 'required|alpha_dash|min:3|unique:organizers',
            'phone' => 'required',
            'facebook' => 'nullable',
            'instagram' => 'nullable',
            'twitter' => 'nullable',
            'email' => 'required|unique:organizers,email,'.$uid
        ];
    }

    public function attributes()
    {
        return [
            'slug' => 'Organizer Username',
        ];
    }
}
