<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('event');
        return [
            'organizer_id' => 'required',
            'title' => 'required|max:160|unique:events,title,'.$id,
            'event_type_id' => 'required',
            'starts_date' => 'required|after_or_equal:today',
            'ends_date' => 'required|after_or_equal:starts_date',
            'starts_time' => 'required',
            'ends_time' => 'required',
            'city_id' => 'required',
            'address' => 'required',
            'gmap_url' => 'nullable|url',
            'description' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'organizer_id' => 'organizer',
            'event_type_id' => 'tipe acara',
            'starts_date' => 'tanggal mulai',
            'ends_date' => 'tanggal berakhir',
            'starts_time' => 'jam mulai',
            'ends_time' => 'jam selesai',
            'gmap_url' => 'google map url',
            'city_id' => "kota"
        ];
    }
}
