<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HelpType;

class HelpTypeController extends Controller
{
    public function show($slug)
    {
        $type = HelpType::whereSlug($slug)->firstOrFail();
        return view('help_types.show',compact('type'));
    }
}
