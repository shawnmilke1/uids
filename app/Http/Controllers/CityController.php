<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class CityController extends Controller
{
    public function index()
    {
        $cities = City::latest();
        $s = request('s');
        if($s){
            $cities->where('name','LIKE',"%$s%")->get();
        }
        $cities = $cities->get();
        return response()->json($cities);
    }

    public function show($slug)
    {
        $city = City::whereSlug($slug)->firstOrFail();
        $events = $city->events()->whereStatus('PUBLISHED')->paginate(20);
        return view('events.city',compact('city','events'));
    }
}
