<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;

class UserSettingController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        return view('auth.settings.index',compact('user'));
    }

    public function storeBasic(Request $request)
    {
        $user = Auth::user();
        $validated = $request->validate([
            'username' => 'required|alpha_dash|unique:users,username,'.$user->id,
            'first_name' => 'required|max:35',
            'last_name' => 'required|max:35'
        ]);
        $user->fill($validated);
        $user->save();
        return back()->with('success','Ter Update!');
    }

    /**
     * Password Change Page
     */
    public function password()
    {
        $user = Auth::user();
        return view('auth.settings.password',compact('user'));
    }

    /**
     * Save Password
     */
    public function postPassword(Request $request)
    {
        $user = Auth::user();
        $validated = $request->validate([
            'password' => $user->password == null ? 'nullable' : 'required',
            'new_password' => 'required|string|min:6|confirmed'
        ]);
        if (!is_null($user->password)) {
            if (!(Hash::check($request->get('password'), $user->password))) {
                return redirect()->back()->withErrors('Password yang anda masukan Salah!')->with('error','Wrong Password');
            }
            if (strcmp($request->get('password'), $request->get('new_password')) == 0) {
                return redirect()->back()->withErrors('Password tidak boleh sama dengan password lama!')->with('error','Password Baru Tidak Boleh sama dengan password Lama');
            }
            if (strcmp($request->get('new_password'), $request->get('new_password_confirmation')) != 0) {
                return redirect()->back()->withErrors('Password konfirmasi tidak cocok');
            }
        }
        $user->password = bcrypt($request->new_password);
        $user->save();
        return back()->with('success','Password Updated');
        
    }
}
