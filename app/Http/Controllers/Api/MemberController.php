<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Organizer;
use App\User;
use App\Jobs\SendNotifyMember;
use App\Member;
use App\OrganizerRole;
use Illuminate\Validation\Rule;

class MemberController extends Controller
{
    public function members($id)
    {
        $organizer = Organizer::findOrFail($id);
        $members = $organizer->members()->with('user','role')->get();
        return response($members,200);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $organizer = Organizer::findOrFail($id);
        $members = $organizer->members->map(function($member){
            return $member->user->email;
        });
        $request->validate([
            'email' => [
                'required',
                'exists:users,email',
                Rule::notIn($members),
            
            ]
        ]);
        $user = User::where('email',$request->email)->firstOrFail(); 
        $member = new Member();
        $member->user_id = $user->id;
        $member->organizer_id = $organizer->id;
        $member->organizer_role_id = 3;
        $member->save();
        $role = OrganizerRole::where('id', 3)->firstOrFail();
        $member->role = $role;
        $member->user = $member->user;
        dispatch(new SendNotifyMember($user,$organizer));
        return response($member,201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::with('user')->findOrFail($id);
        $member->organizer_role_id = $request->organizer_role_id;
        $member->save();
        $roleId = $request->organizer_role_id;
        $role = OrganizerRole::where('id',$roleId)->firstOrFail();
        $member->role = $role;
        return response()->json($member,201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::findOrFail($id);
        $member->delete();
        return response()->json($member,201);
    }
}
