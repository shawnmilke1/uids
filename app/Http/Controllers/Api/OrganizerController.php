<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Filters\EventFilters;
use App\Organizer;
use App\Event;

class OrganizerController extends Controller
{
    /**
     * Display a listing of organizer Event
     * 
     */
    public function orgEvents(EventFilters $filters,$slug)
    {
        $events = $this->getEvents($filters, $slug)->get();
        return response()->json($events, 200);
    }

    /**
     * Collection of statistic organizer events
     */
    public function collection($slug)
    {
        $org = Organizer::whereSlug($slug)->firstOrFail();
        $events = $org->events;
        $totalTickets = $events->map(function ($event) {
            return $event->tickets->count();
        });
        $ticketSell = $events->map(function($event){
            return $event->tickets->sum('ticket_sell');
        });
        $qtys = $events->map(function($event){
            return $event->tickets->sum('qty');
        });
        $sellRate = $ticketSell->sum() == 0 ? 0 : floor(($ticketSell->sum() / $qtys->sum()) * 100);
        $incomes = $events->map(function ($event) {
            return $event->auds->where('ticket.price', '!=', 'free')->sum('ticket.price');
        });
        $visitors = $events->map(function ($event) {
            return $event->views->count();
        });

        return response()->json([
            'tickets' => $totalTickets->sum(),
            'sell' => $ticketSell->sum(),
            'incomes' => $incomes->sum(),
            'visitors' => $visitors->sum(),
            'sellRate' => $sellRate,
        ],200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getEvents(EventFilters $filters, $slug)
    {
        // $events = \Auth::user()->events()->latest()->filter($filters);
        $org = Organizer::whereSlug($slug)->firstOrFail();
        $events = $org->events()->latest()->filter($filters);
        return $events;
    }
}
