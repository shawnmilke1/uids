<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ticket;
use App\Event;
use App\Audience;

class AudienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $ticket = Ticket::find($id);
        $auds = $ticket->auds()->get();
        return response()->json($auds,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Check In Method for Audiences
     */
    public function check(Request $request, $id)
    {
        $audience = Audience::find($id);
        $audience->check_in = !$audience->check_in;
        $audience->save();
        return response()->json($audience, 201);

    }

    /**
     * list all auds by its event
     */
    public function allAuds($id)
    {
        $event = Event::find($id);
        $auds = $event->auds()->with('ticket')->get();
        return response()->json($auds,200);
    }


    public function setAudStatus(Request $request, $uuid)
    {
        $aud = Audience::whereUuid($uuid)->firstOrFail();
        $aud->status = $request->status;
        $aud->save();
        return response($aud, 201);
    }
}
