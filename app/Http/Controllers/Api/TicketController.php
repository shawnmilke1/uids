<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Http\Requests\TicketRequest;
use App\Ticket;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $tickets = Ticket::where('event_id',$id)->get();
        return response()->json($tickets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketRequest $request,$id)
    {
        $validated = $request->validated();
        $ticket = new Ticket();
        $ticket->fill($validated);
        $ticket->save();
        $event = $ticket->event;
        $event->ticket_limit = $event->ticket_limit - 1;
        $event->save();
        return response()->json($ticket,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::with('event')->find($id);
        return response()->json($ticket,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TicketRequest $request, $id)
    {
        $validated = $request->validated();
        $ticket = Ticket::find($id);
        $ticket->fill($validated);
        $ticket->save();
        return response()->json($ticket,201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = Ticket::find($id);
        $event = $ticket->event;
        $event->ticket_limit = $event->ticket_limit + 1;
        $event->save();
        $ticket->delete();
    }
}
