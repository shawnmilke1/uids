<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::get();
        return response()->json($services,200);
    }

    public function show($id)
    {
        $service = Service::find($id);
        return response()->json($service,200);
    }

    public function store(Request $request)
    {
        $service = new Service();
        $validated = $request->validate([
            'email' => 'required|email|unique:services',
            'name' => 'required|min:5',
            'company' => 'required',
            'description' => 'required'
        ]);
        $service->fill($validated);
        $service->save();
        return response()->json($service,201);
    }

    public function destroy($id)
    {
        $service = Service::find($id);
        $service->delete();
        return response()->json($service,200);
    }
}
