<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Filters\EventFilters;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class UserController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function events(EventFilters $filters)
    {
        $events = $this->getEvents($filters)->get();
        
        return response()->json($events,200);
    }

    /**
     * return user members of organizers
     */
    public function organizers()
    {
        $members = auth()->user()->members->map(function ($member) {
            return $member->organizer;
        });
        return response()->json($members,200);
    }

    public function getEvents(EventFilters $filters)
    {
        $events = \Auth::user()->events()->latest()->filter($filters);
        return $events;
    }

    /**
     * 
     */
    public function logout()
    {
        $value = request()->user()->tokens;
        return response()->json($value,200);
    }
}
