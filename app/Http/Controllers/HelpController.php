<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Help;
use App\HelpType;

class HelpController extends Controller
{
    public function index()
    {
        $helps = Help::paginate(12);
        $types = HelpType::get();
        return view('helps.index',compact('helps','types'));
    }

    public function show($type,$slug)
    {
        $help = Help::whereSlug($slug)->firstOrFail();
        $type = HelpType::whereSlug($help->type->slug)->firstOrFail();
        return view('helps.show',compact('help'));
    }
}
