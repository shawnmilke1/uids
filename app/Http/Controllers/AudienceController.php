<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Audience;
use App\Http\Requests\AudienceRequest;
use App\Filters\AudienceFilters;
use Auth;

class AudienceController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * @/tickets
     */
    public function index(AudienceFilters $filters)
    {
        $auds = $this->getAuds($filters);
        return view('audiences.index',compact('auds'));
    }

    /**
     * Detail for Tickets audiences
     */
    public function show($id)
    {
        $aud = Audience::findOrFail($id);
        return view('audiences.show',compact('aud'));
    }

    public function create($uuid)
    {
        $ticket = Ticket::whereUuid($uuid)->firstOrFail();
        if($ticket->event->status != "PUBLISHED"){
            abort(404);
        }
        $left = $ticket->qty - $ticket->ticket_sell;
        $search = request('qty');
        if($search === null || $search > $left || $search > $ticket->ticket_limit){
            return redirect()->route('event.show', $ticket->event->slug);
        }
        $qty = (int)$search;
        return view('audiences.create',compact('ticket','qty'));
    }

    public function store(Request $request, $uuid)
    {
        $ticket = Ticket::whereUuid($uuid)->firstOrFail();
        if ($ticket->event->status != "PUBLISHED") {
            abort(404);
        }
        if($ticket->qty - $ticket->ticket_sell == 0){
            return back()->withErrors('tiket habis');
        }
        $auds =  $request->auds;
        $qty = (int)request('qty');
        $data = [];
        foreach($auds as $key => $value){
            $this->validate(request(),[
                "auds.{$key}.full_name" => 'required',
                "auds.{$key}.email" => 'required',
                "auds.{$key}.phone" => 'required',
            ]);
            $aud = new Audience();
            $aud->full_name = $auds[$key]['full_name'];
            $aud->email = $auds[$key]['email'];
            $aud->phone = $auds[$key]['phone'];
            $aud->event_id = $ticket->event->id;
            $aud->ticket_id = $ticket->id;
            if (auth()->check()) {
                $aud->user_id = auth()->id();
            }
            if ($ticket->price == "free") {
                $aud->status = 'paid';
            } else {
                $aud->expired_at = now()->addDays(2);
            }
            $aud->save();
        }
        return redirect()->route('event.show',$ticket->event->slug)->with('success','Successfuly Creating Ticket');
    }

    public function getAuds(AudienceFilters $filters)
    {
        $auds = Auth::user()->auds()->filter($filters);
        return $auds->paginate(20);
    }

    public function checkIn($uuid)
    {
        $aud = Audience::whereUuid($uuid)->firstOrFail();
        if($aud->event->user_id != auth()->id()){
            return redirect('/');
        }
        $aud->check_in = true;
        $aud->save();
        return redirect("/myevents/event/{$aud->event->id}/audiences");
    }
}
