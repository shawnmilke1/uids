<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    public function index()
    {
        return view('contacts.index');    
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'subject' => 'required|max:191',
            'message' => 'required',
        ]);
        $contact = new Contact();
        $contact->fill($validated);
        $contact->save();
        return back()->with('success','Terima kasih, kami akan merespon pesan anda');
    }
}
