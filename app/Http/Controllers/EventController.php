<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\EventType;
use App\Http\Requests\EventRequest;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;
use App\Filters\EventFilters;
use Indonesia;
use App\EventView;
use Auth;
use Session;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
        $this->middleware('has.org')->except(['index','show','preview']);
        $this->middleware('verified')->except(['index','show']);
    }

    public function index(EventFilters $filters)
    {
        $events = $this->getEvents($filters);
        return view('events.index',compact('events'));
    }

    public function create()
    {
        return view('events.create',[
            'event' => new Event(),
            'user' => Auth::user(),
            'types' => EventType::get(),
        ]);
    }

    public function store(EventRequest $request)
    {
        $validated = $request->validated();
        $event = new Event();
        $event->fill($validated);
        $this->validate(request(),[
            'image' => 'required'
        ]);
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $resize = Image::load($image)
                ->crop(Manipulations::CROP_CENTER, 960, 480)->optimize()->save();
            $image->store('public/events/' . $request->user()->id);
            $event->image = $request->file('image')->hashName('events/' . $request->user()->id . '/');
        }
        $event->save();
        return redirect('/o/'.$event->organizer->slug.'/event/'.$event->id.'/dashboard');
    }

    public function show($slug)
    {
        $event = Event::whereSlug($slug)->where('status','PUBLISHED')->firstOrFail();
        $key = 'events_'.$event->id;
        if(!Session::has($key)){
            $view = new EventView();
            $view->event_id = $event->id;
            Session::put($key,1);
            $view->save();
        }
        $tickets = $event->tickets()->get();
        return view('events.show',compact('event','tickets'));
    }

    public function preview($slug)
    {
        $event = Event::whereSlug($slug)->whereStatus('DRAFT')->firstOrFail();
        $members =  $event->organizer->members;
        $user = Auth::user();
        if(!in_array($user->id,$members->pluck('user_id')->toArray())){
            abort(403);
        }
        $tickets = $event->tickets()->get();
        return view('events.preview',compact('event','tickets'));
    }

    /**
     * Edit Event 
     */
    public function edit($id)
    {
        $event = Event::where('status','DRAFT')->findOrFail($id);
        $user = Auth::user();
        $types = EventType::get();
        $this->authorize('view',$event);
        return view('events.edit',compact('event','user','types'));
    }

    /**
     * Edit Event
     */
    public function update(EventRequest $request ,$id)
    {
        $event = Event::where('status','DRAFT')->findOrFail($id);
        $this->authorize('view',$event);
        $validated = $request->validated();
        $event->fill($validated);
        if ($request->hasFile('image')) {
            unlink(public_path('storage/'. $event->image));
            $image = $request->file('image');
            Image::load($image)
                ->crop(Manipulations::CROP_CENTER, 960, 480)->optimize()->save();
            $image->store('public/events/' . $request->user()->id);
            $event->image = $request->file('image')->hashName('events/' . $request->user()->id . '/');
        }
        $event->save();
        return redirect('/o/' . $event->organizer->slug . '/event/' . $event->id . '/dashboard');
    }

    public function getEvents(EventFilters $filters)
    {
        $events = Event::whereStatus('PUBLISHED')->filter($filters);
        return $events->paginate(20);
    }
}
