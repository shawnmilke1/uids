<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organizer;
use App\Http\Requests\OrganizerRequest;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

class OrganizerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['show','members']);
    }

    public function choose()
    {
        $members = auth()->user()->members;
        $organizers = $members->map(function($member){
            return $member->organizer;
        });
        return view('organizers.choose',compact('organizers'));

    }

    public function dashboard($slug)
    {
        $org = Organizer::whereSlug($slug)->firstOrFail();
        if ($org->members()->where('user_id', auth()->id())->doesntExist()) {
            abort(403);
        }
        return view('organizers.dashboard',compact('org'));
    }

    public function index()
    {
        $user = \Auth::user();
        $orgs = $user->members->map(function($member){
            return $member->organizer;
        });
        return view('organizers.index',compact('orgs','user'));
    }

    public function show($slug)
    {
        $org = Organizer::whereSlug($slug)->firstOrFail();
        $events = $org->events()->whereStatus('PUBLISHED')->paginate(20);
        return view('organizers.show',compact('org','events'));
    }

    public function members($slug)
    {
        $org = Organizer::whereSlug($slug)->firstOrFail();
        $members  = $org->members;
        return view('organizers.members.index', compact('org','members'));
    }

    public function create()
    {
        return view('organizers.create',['organizer' => new Organizer()]);
    }

    public function store(OrganizerRequest $request)
    {
        $validated = $request->validated();
        $organizer = new Organizer();
        $organizer->fill($validated);
        $organizer->user_id = auth()->id();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $resize = Image::load($image)
                ->crop(Manipulations::CROP_CENTER, 480, 480)->optimize()->save();
            $image->store('public/organizers/' . $request->user()->id);
            $organizer->image = $request->file('image')->hashName('organizers/' . $request->user()->id . '/');
        }
        $organizer->save();
        return back()->with('success','Successfully create organizer');
        
    }

    public function edit($id)
    {
        $organizer = Organizer::findOrFail($id);
        if($organizer->user_id != auth()->id()){
            abort(403);
        }
        return view('organizers.edit',compact('organizer'));
    }

    public function update(OrganizerRequest $request,$id)
    {
        $organizer = Organizer::findOrFail($id);
        $validated = $request->validated();
        $organizer->fill($validated);
        $oldImage = public_path() . '/storage/' . $organizer->image;
        if ($request->hasFile('image')) {
            if($organizer->image != null){
                unlink($oldImage);
            }
            $image = $request->file('image');
            $resize = Image::load($image)
                ->crop(Manipulations::CROP_CENTER, 480, 480)->optimize()->save();
            $image->store('public/organizers/' . $request->user()->id);
            $organizer->image = $request->file('image')->hashName('organizers/' . $request->user()->id . '/');
        }
        $organizer->save();
        return back()->with('success','Organizer Updated');
        
    }
}
