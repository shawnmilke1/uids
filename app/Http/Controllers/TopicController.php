<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventTopic;

class TopicController extends Controller
{
    public function show($slug)
    {
        $topic = EventTopic::whereSlug($slug)->firstOrFail();
        $events = $topic->events()->whereStatus('PUBLISHED')->paginate(20);
        return view('topics.show', compact('topic', 'events'));
    }
}
