<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    /**
     * Create Page
     */
    public function index()
    {
        if(request()->wantsJson()){
            $services = Service::get();
            return response()->json($services,200);
        }
        return view('services.index');
    }

    public function store(Request $request)
    {
        $service = new Service();
        $validated = $request->validate([
            'email' => 'required|unique:services',
            'name' => 'required|min:5',
            'company' => 'required',
            'description' => 'required'
        ]);
        $service->fill($validated);
        $service->save();
        return response()->json($service,201);
    }
}
