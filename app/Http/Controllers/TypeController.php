<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventType;

class TypeController extends Controller
{
    public function show($slug)
    {
        $type = EventType::whereSlug($slug)->firstOrFail();
        $events = $type->events()->whereStatus('PUBLISHED')->paginate(20);
        return view('types.show',compact('type','events'));
    }
}
