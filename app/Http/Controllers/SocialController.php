<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\User;
use App\Social;
use Auth;

class SocialController extends Controller
{
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = $this->createOrGetUser(Socialite::driver($provider)->user(),$provider);
        Auth::login($user);
        if(session()->has('from_conn')){
            return redirect(session('from_conn'));
            session()->forget('from_conn');
        }
        return redirect('/')->with('success',"Sukses Login Melalui {$provider}");
    }

    public function destroy($id)
    {
        $provider = Social::findOrFail($id);
        $provider->delete();
        return back()->with('success',"{$provider->provider} Deleted");
    }

    /**
     * buat jika akun user dari provider belum terdaftar
     */
    private function createOrGetUser($providerUser, $provider)
    {
        $account = Social::where('provider', $provider)
            ->where('provider_user_id', $providerUser->getId())
            ->first();
        if ($account) {
            //return account jika ditemukan
            return $account->user;
        } else {
            //Check jika terdapat email yang sama 
            $user;
            //buat user jika tidak ada email yang terdaftar
            if (!Auth::user() && !User::where('email', $providerUser->getEmail())->first() ) {
                $user = User::create([
                    'username' => str_random(10),
                    'first_name' => str_random(7),
                    'last_name' => str_random(7),
                    'email' => $providerUser->getEmail(),
                    'email_verified_at' => date(now()),
                    'password' => null
                ]);
            }
            $user = User::where('email', $providerUser->getEmail())->first();
            if(auth()->check()){
                $user = Auth::user();
                session()->put('from_conn','/@/connections');
            }
            $user->socials()->create([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);
            
            return $user;
        }
    }

    public function index()
    {
        $user = Auth::user();
        $socials = $user->socials;
        return view('socials.index',compact('user','socials'));
    }
}
