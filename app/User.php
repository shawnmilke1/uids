<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','last_name','first_name', 'email', 'password','role_id','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * This hasMany Events
     */
    public function events()
    {
        return $this->hasMany('App\Event');
    }

    /**
     * This HasMany Organizers
     */
    public function organizers()
    {
        return $this->hasMany('App\Organizer');
    }

    /**
     * This HasMany Social Account
     */
    public function socials()
    {
        return $this->hasMany('App\Social');
    }

    /**
     * overwrite verification using queued
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new \App\Notifications\VerifyEmailQueued);
    }

    /**
     * User have Audiences tickets
     */
    public function auds()
    {
        return $this->hasMany('App\Audience');
    }

    /**
     * User has Many Member
     */
    public function members()
    {
        return $this->hasMany('App\Member');
    }

    /**
     * super admin
     */
    public function isSuperAdmin()
    {
        return $this->role_id === 1;
    }

    public function fullName()
    {
        return $this->first_name .' '.$this->last_name;
    }
}
