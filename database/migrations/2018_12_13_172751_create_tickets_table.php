<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->unsignedInteger('event_id');
            $table->string('name');
            $table->integer('qty');
            $table->string('price');
            $table->text('description')->nullable();
            $table->enum('channel',['everywhere','online','offline'])->default('everywhere');
            $table->date('starts_date')->nullable();
            $table->date('ends_date')->nullable();
            $table->time('starts_time')->nullable();
            $table->time('ends_time')->nullable();
            $table->integer('per_order')->default(1);
            $table->unsignedInteger('coupon_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
