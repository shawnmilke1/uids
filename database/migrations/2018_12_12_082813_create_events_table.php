<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('organizer_id');
            $table->string('title');
            $table->string('slug');
            $table->unsignedInteger('event_type_id');
            $table->unsignedInteger('event_topic_id');
            $table->enum('status',['DRAFT','PUBLISHED']);
            $table->string('image');
            $table->text('address');
            $table->text('gmap_url')->nullable();
            $table->date('starts_date');
            $table->date('ends_date');
            $table->time('starts_time');
            $table->time('ends_time');
            $table->string('city');
            $table->integer('ticket_limit')->default(0);
            $table->text('description');
            $table->timestamp('review_at');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
